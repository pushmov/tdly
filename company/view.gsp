<%@page import="test2.Person" %>
<%@page import="test2.MyVendor" %>
<%@page import="test2.Company" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="pz" />
        <title>Show Company</title>
    </head>
    <body>
	<g:render template="viewCompanyDialog" var="company" bean="${company}" />
</body>
</html>
