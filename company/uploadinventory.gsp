<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="layout" content="saas-nologin" />
<title> SaaS WP Theme II</title> 
<!--[if IE]>
<script src="../js/html5.js"></script>
<![endif]-->
<meta name='robots' content='noindex,nofollow' />

<script type='text/javascript' src='../js/jquery/jquery.js?ver=1.4.2'></script> 
<script type='text/javascript' src='../js/easySlider1.5.js?ver=3.0'></script>
<script type='text/javascript' src='../js/saas.js?ver=3.0'></script>
<script type='text/javascript' src='../js/jquery.cookie.min.js?ver=3.0'></script>
<script type='text/javascript' src='../js/saas.twitter.js?ver=3.0'></script>

</head>
<body>
<div class="header">
	<div>
		<div class="center">
			<div class="topLinks">
				<div class="menu-top-links-container">
					<ul id="menu-top-links" class="menu">
						<li class="menu-item current-menu-item"><a href="about.html">About</a></li>
						<li class="menu-item"><a href="blog.html">Blog</a></li>
						<li class="menu-item"><a href="help.html">Help</a></li>
						<li class="menu-item"><a href="contact-us.html">Contact Us</a></li>
					</ul>
				</div>
			</div>
			<h1 id="logo"><a href="home.html"><img src="../images/logo.png" alt="SaaS WP Theme II" /></a></h1>
			<div class="nav">
				<div class="menu-main-nav-container">
					<ul id="menu-main-nav" class="menu">
						<li class="menu-item"><a href="tour.html"><span>Tour</span></a></li>
						<li class="menu-item"><a href="plans-pricing.html"><span>Plans &#038; Pricing</span></a></li>
						<li class="menu-item"><a href="sign-up.html"><span>Sign Up!</span></a></li>
					</ul>
				</div>
			</div>
			<div class="page-title">
				<div>
					<h2>Sign Up</h2>					
				</div>
			</div>
		</div>
	</div>
</div>
<!--div.header end -->
<!--div.container start -->
<div class="container">
	<div class="center">

	    <g:form controller="inventory" method="post" action="fileupload" enctype="multipart/form-data" class="signupform" >

            <g:if test="${flash.message}">
            <div class="message">gg ${flash.message}</div>
            </g:if>

            <g:hasErrors bean="${person}">
            <div class="errors">
                <g:renderErrors bean="${person}" as="list" />
            </div>
            </g:hasErrors>
									
		<h5>Upload Inventory ( We will take care of any formatting, column names etc. )</em></h5>
		<ul>




			<li>
				<label>Inventory File:</label>
    				<input class="file" name="file" type="file" />
			</li>

	    </g:form>                        
	

	</div>
</div>

<!--div.container end -->
<!--div.footer start -->
<div class="footer">
	<div class="prom">
		<div class="center">
			<h2>
				<strong>Start doing stuff now! </strong>Try us out free for 30 days.			
			</h2>
			<div class="right"><a href="sign-up.html"><img src="../images/btn-sign-up.png" alt="Sign Up!"></a></div>
		</div>
	</div>
	<div class="links">
		<div class="center">	
			<div>
				<h3>Company</h3>
				<p>
					<a href="#">Leadership</a><br />
					<a href="#">Partners</a><br />
					<a href="#">Investors</a>
				</p>
			</div>
			<div>
				<h3>Keep in Touch</h3>
				<ul class="social-links">
					<li><a href="http://twitter.com/themeteam"><img src="../images/i_socialTwitter.png" alt="" /></a></li>
					<li><a href="http://www.facebook.com/facebook-name"><img src="../images/i_socialFacebook.png" alt="" /></a></li>
					<li><a href="http://www.flickr.com/photos/flickr-name/"><img src="../images/i_socialFlickr.png" alt="" /></a></li>
					<li><a href="http://saas-wp-ii.worryfreelabs.com/feed/"><img src="../images/i_socialRss.png" alt="" /></a></li>
					<li><a href="http://linkedin.com/in/linkedin-name"><img src="../images/i_socialLinkedin.png" alt="" /></a></li>
					<li><a href="http://www.youtube.com/user/youtube-name"><img src="../images/i_socialYoutube.png" alt="" /></a></li>
				</ul>
			</div>
			<div>
				<h3>Support</h3>
				<p>
					<a href="#">Create a Support Ticket</a><br />
					<a href="#">FAQs</a><br />
					<a href="#">Forum</a><br />
					<a href="#">Live Chat</a>
				</p>
			</div>
			<div>
				<h3>SaaS Corp, Inc.</h3>
				<p>544 Oenoke Ridge<br />
				New Canaan, CT 06480</p>
				<p><a href="http://gothemeteam.com/">http://gothemeteam.com/</a><br />
				Toll Free: (800) 555-1212</p>
			</div>		
		</div>
	</div>
</div>
<!--div.footer end -->

<script type='text/javascript' src='../js/jquery/jquery.form.js?ver=2.02m'></script>
</body>
</html>
