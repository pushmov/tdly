<div class="row">
	<div class="col-sm-12">
		<h2 class="etext">Company Details</h2>
	</div>
	<input type="hidden" name="id" value="${company?.id}" />
	<div class="col-sm-12 col-md-6 col-lg-6">
		<div class="form-group clearfix">
			<label class="col-sm-3 col-md-3 col-lg-3 control-label gtext">Name</label>
			<div class="col-sm-7 col-md-7 col-lg-7"> : ${company?.name}</div>
		</div>
		<div class="form-group clearfix">
			<label class="col-sm-3 col-md-3 col-lg-3 control-label gtext">Category</label>
			<div class="col-sm-7 col-md-7 col-lg-7"> : ${company?.category}</div>
		</div>
		<div class="form-group clearfix">
			<label class="col-sm-3 col-md-3 col-lg-3 control-label gtext">Address</label>
			<div class="col-sm-7 col-md-7 col-lg-7"> : ${company?.address?.street1}</div>
		</div>
		<div class="form-group clearfix">
			<label class="col-sm-3 col-md-3 col-lg-3 control-label gtext">Phone</label>
			<div class="col-sm-7 col-md-7 col-lg-7"> : ${company?.address?.phone}</div>
		</div>
		<div class="form-group clearfix">
			<label class="col-sm-3 col-md-3 col-lg-3 control-label gtext">Toll Free</label>
			<div class="col-sm-7 col-md-7 col-lg-7"> : ${company?.address?.tollfreephone}</div>
		</div>
		<div class="form-group clearfix">
			<label class="col-sm-3 col-md-3 col-lg-3 control-label gtext">Fax</label>
			<div class="col-sm-7 col-md-7 col-lg-7"> : ${company?.address?.fax}</div>
		</div>
		<div class="form-group clearfix">
			<label class="col-sm-3 col-md-3 col-lg-3 control-label gtext">Year Founder</label>
			<div class="col-sm-7 col-md-7 col-lg-7"> : ${company?.founded}</div>
		</div>
		<div class="form-group clearfix">
			<label class="col-sm-3 col-md-3 col-lg-3 control-label gtext">Employee Count</label>
			<div class="col-sm-7 col-md-7 col-lg-7"> : ${company?.noofemployees}</div>
		</div>
		<div class="form-group clearfix">
			<label class="col-sm-3 col-md-3 col-lg-3 control-label gtext">Company Information</label>
			<div class="col-sm-7 col-md-7 col-lg-7"> : <a href="javascript:void(0);" id="moremsg" onClick="moreMsg(${company.id},'company','bio')" >View</a></div>
		</div>
		<div class="form-group clearfix">
			<label class="col-sm-3 col-md-3 col-lg-3 control-label gtext">Terms of Service</label>
			<div class="col-sm-7 col-md-7 col-lg-7"> : <a href="javascript:void(0);" id="moremsg" onClick="moreMsg(${company.id},'company','tos')" >View</a></div>
		</div>
	</div>
	<div class="col-sm-12 col-md-6 col-lg-6">
		<div class="form-group clearfix">
			<label class="col-sm-3 col-md-3 col-lg-3 control-label gtext">Membership Level</label>
			<div class="col-sm-7 col-md-7 col-lg-7"> : ${company?.membershiplevel?.description}</div>
		</div>
		<div class="form-group clearfix">
			<label class="col-sm-3 col-md-3 col-lg-3 control-label gtext">Country</label>
			<div class="col-sm-7 col-md-7 col-lg-7"> : ${company?.address?.country?.name}</div>
		</div>
		<div class="form-group clearfix">
			<label class="col-sm-3 col-md-3 col-lg-3 control-label gtext">State</label>
			<div class="col-sm-7 col-md-7 col-lg-7"> : ${company?.address?.state}</div>
		</div>
		<div class="form-group clearfix">
			<label class="col-sm-3 col-md-3 col-lg-3 control-label gtext">City</label>
			<div class="col-sm-7 col-md-7 col-lg-7"> : ${company?.address?.city}</div>
		</div>
		<div class="form-group clearfix">
			<label class="col-sm-3 col-md-3 col-lg-3 control-label gtext">Zip</label>
			<div class="col-sm-7 col-md-7 col-lg-7"> : ${company?.address?.postalcode}</div>
		</div>
		<div class="form-group clearfix">
			<label class="col-sm-3 col-md-3 col-lg-3 control-label gtext">Timezone</label>
			<div class="col-sm-7 col-md-7 col-lg-7"> : ${application.timezones.get(company.address.timezone)}</div>
		</div>
	</div>
	<div class="col-sm-12">
		<g:isAdminEmployee  company="${company}" >				
		<div class="form-group">
			<button type="button" class="btn btn-broadcast" onClick="javascript:updateCompany();" title="Home" class="active">Update</button>
		</div>
		</g:isAdminEmployee>
		<g:ifNotVendor company="${company}" >
		<div class="form-group">
			<button type="submit" name="addvendor" id="addvendor" class="btn btn-broadcast">Add To MyVendors</button>
		</div>
		</g:ifNotVendor>
	</div>
</div>