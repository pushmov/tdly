<%@page import="test2.Membershiplevel" %>


						
				            <g:form action="update" method="post" class="search_form" >



							<!--[if !IE]>start fieldset<![endif]-->
							<fieldset>
								<!--[if !IE]>start forms<![endif]-->
								<div class="forms">
								<h4>Edit Company</h4>

								<!--[if !IE]>start row<![endif]-->
								<div class="row">
								</div>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>

                <input type="hidden" name="id" value="${company?.id}" />
                        
                        
                            <div class="row">

                                    <label for="name">Name:</label>
						<div class="inputs">
                                    	<span class="input_wrapper">
		                                    <input type="text" class="text" id="name" name="name" value="${fieldValue(bean:company,field:'name')}"/>
							</span>
		  		                  <g:renderErrorsCap bean="${company}" as="list" field="name"  />
						</div>

                            </div> 

                            <div class="row">

                                    <label for="category">Category:</label>
						<div class="inputs">
                                    	<span class="input_wrapper">
            		                        <input type="text" class="text" id="category" name="category" value="${fieldValue(bean:company,field:'category')}"/>
							</span>
				                  <g:renderErrorsCap bean="${company}" as="list" field="category"  />
						</div>

                            </div> 

				<div class="row">
                                    <label for="membershiplevel">Membership Level</label>
						<div class="inputs">
							<span class="input_wrapper select_wrapper">
		                                    <g:select optionKey="id" from="${Membershiplevel.list()}" optionValue="memlevel" name="membershiplevel" value="${fieldValue(bean:company,field:'membershiplevel')}" ></g:select>
							</span>
						</div>
				</div>

                            
                            <g:render template="/companyaddress" var="address" bean="${company.address}" />

                            <div class="row">

                                    <label for="membershiplevel">Timezone :</label>
						<span class="input_wrapper select_wrapper">
							<g:select name="timezone"
						          from="${application.timezones}"
						          value="${company.address.timezone}"
						          optionKey="key" 
							    optionValue="value"
								 />
						</span>
                            </div> 

                            <div class="row">

                                    <label for="membershiplevel">Founding Year</label>
						<div class="inputs">
                                    	<span class="input_wrapper">
		                                    <input type="text" class="text"  id="founded" name="founded" value="${company.founded}"/>
							</span>
						</div>
                            </div> 
                            <div class="row">

                                    <label for="membershiplevel">Employee Count</label>
						<div class="inputs">
                                    	<span class="input_wrapper">
		                                    <input type="text" class="text"  id="noofemployees" name="noofemployees" value="${company.noofemployees}"/>
							</span>
						</div>
                            </div> 
                            <div class="row">

                                    <label for="membershiplevel">Company Information</label>
						<div class="inputs">
                                    	<span class="input_wrapper">
		                                    <textarea class="text"  id="bio" name="bio" >${fieldValue(bean:company,field:'bio')}</textarea>
							</span>
						</div>
                            </div> 
                            <div class="row">

                                    <label for="membershiplevel">Terms Of Service</label>
						<div class="inputs">
                                    	<span class="input_wrapper">
		                                    <textarea class="text"  id="tos" name="tos" >${fieldValue(bean:company,field:'tos')}</textarea>
							</span>
						</div>
                            </div> 



								<div class="row">
									<div class="inputs">
										<span class="button blue_button search_button"><span><span><em>Update</em></span></span><input name="" type="submit" /></span>
									</div>
								</div>
            </g:form>
                </div>
        </fieldset>
