<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta name="layout" content="pz" />
		
		<link href="${createLinkTo(dir:'assets',file:'partszap/css/app.css')}" rel="stylesheet">

<title>Admin Panel</title>
<!--[if lte IE 6]><link media="screen" rel="stylesheet" type="text/css" href="css/ie.css" /><![endif]-->
<r:script>

$(document).ready(function() {

  $('#dialog-company').dialog({
        autoOpen: false,
        height: 600,
        width: 650,
        modal: true,
        buttons: {
            SaveFilter: function() {

    		$("#company").submit();
                $(this).dialog('close');

            },

            Cancel: function() {
                $(this).dialog('close');
            }

        }        
    });

	      $('#company').submit( function() {
    
			    $.ajax({
    			    type: "POST",
    			    url: "../company/update",
    			    data: $(this).serialize(),
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
				    alert('success');
				   //$('#dialog-responseSave').dialog("open");
    			    }
			    
                            });

        	            return false;

              });



});
/*
function moreMsg(respid, attr) {
$.get("moreResponse",{respid : respid},
                function(data,textStatus,jqXHR) {
                    $("#moreMessage").html(data.responsetext);
                    $('#dialog-moreMessage').dialog('open');
                }
);
*/
function editCompany(id) {


	$.get("../company/edit",{id: id},

		function(data,textStatus,jqXHR) {
			$('#dialog-company').html(data);		        
			$("#dialog-company").dialog("open");
		}

        );

}

function updateCompany() {

	$('#saveCompany').submit();

}

</r:script>

    </head>
<g:render template="/moreMessage" />
<div id="dialog-company" title="Company"> 
</div>
	<main class="content" role="main">
		<div class="main-content">
			<div class="container">
				<g:form name="saveCompany" action="edit" method="post" class="search_form" >
				<g:render template="viewCompany" var="company" bean="${company}" />
				</g:form>
			</div>
		</div>
	</main>
</html>
