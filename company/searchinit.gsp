

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Create Company</title>         
    </head>
    <body>
        <div class="body">
            <h1>Search Company</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${company}">
            <div class="errors">
                <g:renderErrors bean="${company}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="searchlist" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="name">Name:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:company,field:'name','errors')}">
                                    <input type="text" id="name" name="name" value="${fieldValue(bean:company,field:'name')}"/>
                                </td>
                            </tr> 

                            <tr class="prop"> 
                                <td valign="top" class="name">
                                    <label for="name">Membership Level</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:company,field:'membershiplevel','errors')}">
                                    <input type="text" id="membershiplevel" name="membershiplevel" value="${fieldValue(bean:company,field:'membershiplevel')}"/>
                                </td>
                            </tr> 

                            <tr class="prop"> 
                                <td valign="top" class="name">
                                    <label for="name">Country</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:company,field:'country','errors')}">
                                    <input type="text" id="country" name="country" value="${fieldValue(bean:company,field:'country')}"/>
                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><input class="save" type="submit" value="Search" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
