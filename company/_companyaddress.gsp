<%@page import="test2.Country" %>
<%@page import="test2.Region" %>


			<li>
				<label for="street">Street Address:</label>
				<input type="text" name='street1' value="${address?.street1?.encodeAsHTML()}" class='text' />
				<em>*</em>
			</li>
			
			
			<li>
				<label for="country">Country:</label>
                                <g:select id="countrySelect" optionKey="id" from="${Country.list(sort:'name', order:'asc')}" optionValue="name" name="country" value="${address?.country?.id?.encodeAsHTML()}"  noSelection="['':'-Please select your country-']" ></g:select>
				<em>*</em>
			</li>
			<li  id="stateSelect" >
				
				<label for="state">State:</label>
                                <g:select  optionKey="name" from="${flash.stateSelected}" optionValue="name" name="state" value="${address?.state?.encodeAsHTML()}"  noSelection="['':'-Please select your State/Region-']" ></g:select>
				<em>*</em>
			</li>
			<li>
				<label for="city">City:</label>
				<input type="text" name='city' value="${address?.city?.encodeAsHTML()}" class='text' />
				<em>*</em>

			</li>
			<li>
				<label for="zip">ZIP:</label>
				<input type="text" name='postalcode' value="${address?.postalcode?.encodeAsHTML()}" class='text' />
				<em>*</em>
			</li>
			
			<li>
				
				<label for="phone">Phone:</label>
				<input type="text" name='phone' value="${address?.phone?.encodeAsHTML()}" class='text' />
				<em>*</em>
			</li>
