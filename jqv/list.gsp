
<%@ page import="tdly.Jqv" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'jqv.label', default: 'Jqv')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-jqv" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-jqv" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="hello" title="${message(code: 'jqv.hello.label', default: 'Hello')}" />
					
						<g:sortableColumn property="world" title="${message(code: 'jqv.world.label', default: 'World')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${jqvInstanceList}" status="i" var="jqvInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${jqvInstance.id}">${fieldValue(bean: jqvInstance, field: "hello")}</g:link></td>
					
						<td>${fieldValue(bean: jqvInstance, field: "world")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${jqvInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
