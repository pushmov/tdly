<%@ page import="tdly.Jqv" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="pz">
		<g:set var="entityName" value="${message(code: 'jqv.label', default: 'Jqv')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
		<r:require modules="jquery-validation-ui" />
	</head>
	<body>
		<jqvalui:renderValidationScript 
	for="tdly.Jqv"
	form="personForm" 
	errorClass="invalid" 
	validClass="success" 
	onsubmit="true" 
	renderErrorsOnTop="true"
	/>
		<a href="#create-jqv" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="create-jqv" class="content scaffold-create" role="main">
			<h1><g:message code="default.create.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${jqvInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${jqvInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form name="personForm" action="save" >
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
