<script>
$(document).ready(function()
        {
  $('#dialog-company').dialog({
        autoOpen: false,
        height: 600,
        width: 950,
        modal: true,
        buttons: {

            Cancel: function() {
                $(this).dialog('close');
            }

        }
    });
});

function getCompany(cid) {

            $.ajax({
                    type: "GET",
                    url: "../company/showajax",
                    cache: false,
                    data: { id : cid },
                    error: function(data, textStatus, jqXHR) {
                        alert("Error: " + textStatus);
                    },
                    success: function(data, textStatus, jqXHR) {
                        $("#dialog-company").html(data)
                        $("#dialog-company").dialog('open');
                    }
            });


}

</script>
<div id="dialog-company" title="Company">

</div>
