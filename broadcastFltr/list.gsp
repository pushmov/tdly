<%@ page import="test2.BroadcastFilter" %>
<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.Continent" %>
<%@page import="test2.Manufacturer" %>
<%@page import="test2.Company" %>
<%@page import="test2.Country" %>
   
<html>
    <head> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="pz" />
		<link href="${createLinkTo(dir:'assets',file:'partszap/css/app.css')}" rel="stylesheet">
<main class="content" role="main">
	<div class="main-content">
	
		<div style="margin:15px 0;">
			<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal_create_filter" type="button">Create Filter</button>
		</div>
		<div class="table-responsive">
			<table class="table table-bordered gray-header responsive">
				<thead>
					<tr>
						<g:sortableColumn property="name" title="${message(code: 'broadcastFilter.name.label', default: 'Name')}" />
						<g:sortableColumn property="broadcasttype" title="${message(code: 'broadcastFilter.broadcasttype.label', default: 'Broadcast Type')}" />
						<g:sortableColumn property="keyword" title="${message(code: 'broadcastFilter.keyword.label', default: 'Keywords')}" />
						<g:sortableColumn property="keyword" title="${message(code: 'broadcastFilter.partlist.label', default: 'Partno / Partlist')}" />
						<g:sortableColumn property="keyword" title="${message(code: 'broadcastFilter.condition.label', default: 'Condition')}" />
						<th>Manufacturers</th>
						<th>Companies</th>
						<th>Countries</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<g:each in="${broadcastFilterList}" status="i" var="broadcastFilterInstance">
					<tr>
						<td>${fieldValue(bean: broadcastFilterInstance, field: "name")}</td>
						<td>${broadcastFilterInstance.broadcasttype}</td>
						<td>${fieldValue(bean: broadcastFilterInstance, field: "keyword")}</td>
						<td>${broadcastFilterInstance.partlist}</td>
						<td>${broadcastFilterInstance.conditions*.name}</td>
						<td>${manufacturers}</td>
						
		<%
			def manufacturers = new StringBuffer()
			def manufacturer = (broadcastFilterInstance.manufacturers.size() == 0)?"All":"list"
			if (manufacturer == "list") {
				
				if (!(broadcastFilterInstance.manufacturers instanceof String)) {

					broadcastFilterInstance.manufacturers.each {
						manufacturers << it.mfgname + ", "
					}

				}
				else {
					manufacturers = broadcastFilterInstance.manufacturers
				}
			}
				
		%>
						<td>${companies}</td>
		<%
			def countries = new StringBuffer()
			def country = broadcastFilterInstance.myCountries?"My Countries":((broadcastFilterInstance.countries.size() == 0)?"All":"list")
			if (country == "list") {
				
				if (!(broadcastFilterInstance.countries instanceof String)) {

					broadcastFilterInstance.countries.each {
						countries << it.name + ", "
					}

				}
				else {
					countries = broadcastFilterInstance.countries
				}
			}
			else {
				countries = country
			}
				
		%>
						<td>${countries}</td>
						<td>
							<div class="action-control">
								<a href="#" data-toggle="modal" data-target="#modal_edit_filter" id="respond" onClick="getFilter(${broadcastFilterInstance.id});">
									<i class="fa fa-edit fa-lg"></i>Edit
								</a>
								<!--<g:remoteLink class="delete" action="delete" id="${broadcastFilterInstance?.id}" before="return confirm('Are you sure?');" ><i class="fa fa-times fa-lg"></i>Delete</g:remoteLink>-->
                                                                <a href="#" data-id="${broadcastFilterInstance?.id}" data-toggle="modal" class="delete-filter" data-target="#confirm-delete"><i class="fa fa-trash"></i> Delete</a>
							</div>
						</td>
					</tr>
					</g:each>
				</tbody>
			</table>
		</div>
		<div class="pagination_n">
			<ul>
				<g:paginate total="${filterCount}" />
			</ul>
		</div>
	</div>
</main>

<button data-toggle="modal" style="display: none" data-target="#confirm-delete"></button>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Delete this filter?</div>
			<div class="modal-body">
				Delete this filter?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
				<a data-id="" class="btn btn-danger btn-delete">Delete</a>
			</div>
		</div>
  </div>
</div>

<button data-toggle="modal" style="display: none" data-target="#modal_delete_success"></button>
<div class="modal fade broadcasts-popup" id="modal_delete_success" tabindex="-1" role="dialog" aria-labelledby="modal_single_part_broadcastsLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_single_part_broadcastsLabel">Success</h4>
            </div>
            <!--modal-header-->
            <div class="modal-body">
                <div id="response" class="form-content">
                    Filter deleted successfully.
                </div>
            </div>
        </div>
        <!--modal-content-->
    </div>
    <!--modal-dialog-->
</div>

<g:render template="/dialog-fltr" />

<script>
    
    $('.delete-filter').click(function(){
        var id = $(this).attr('data-id');
        
        $('#confirm-delete .btn-delete').attr('data-id', id);
    });
    
    $('.btn-delete').click(function(){
        window.location = '/tdly/broadcastFltr/delete/' + $(this).attr('data-id');
    });
</script>
