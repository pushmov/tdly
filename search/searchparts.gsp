

<meta name="layout" content="pz" />
<link href="${createLinkTo(dir:'assets',file:'partszap/css/app.css')}" rel="stylesheet">

<title>Part List</title>



<main class="content" role="main">
	<div class="main-content">
		<div class="tab-content-block">
			<g:render template="broadcastGridSinglePart" model="${[broadcastList:broadcastList,countryMap:countryMap,companyMap:companyMap,manufacturerMap:manufacturerMap,broadcastTotal:broadcastTotal, filterParams:filterParams ]}" />
		</div>
	</div>
</main>

<div id="dialog-form" title="Create new Reminder">
	<table>
		<tbody>
			<g:hiddenField name="id" value="${broadcastFilterInstance?.id}" />
			<tr class="prop">
				<td valign="top" class="name">
					<g:message code="broadcastFilter.name.label" default="Filter Name" /></label>
				</td>
				<td valign="top" >
					<input type=text id="filterNameText" name="filterNameText" value="" />
				</td>
			</tr>
		</tbody>
	</table>
</div>

<r:require module="filter" />
<g:render template="/dialog-broadcast" />
<g:render template="/dialog-watchlist" />