
<%@ page import="test2.AutoUpload" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Show AutoUpload</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">AutoUpload List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New AutoUpload</g:link></span>
        </div>
        <div class="body">
            <h1>Show AutoUpload</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>

                    
                        <tr class="prop">
                            <td valign="top" class="name">Id:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:autoUploadInstance, field:'id')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Ftpurl:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:autoUploadInstance, field:'ftpurl')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Httpurl:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:autoUploadInstance, field:'httpurl')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Company:</td>
                            
                            <td valign="top" class="value"><g:link controller="company" action="show" id="${autoUploadInstance?.company?.id}">${autoUploadInstance?.company?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Disableauto:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:autoUploadInstance, field:'disableauto')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Lastupdated:</td>
                            
                            <td valign="top" class="value"><g:link controller="person" action="show" id="${autoUploadInstance?.lastupdated?.id}">${autoUploadInstance?.lastupdated?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <input type="hidden" name="id" value="${autoUploadInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" value="Edit" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
