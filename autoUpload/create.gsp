
<%@ page import="test2.AutoUpload" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Create AutoUpload</title>         
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">AutoUpload List</g:link></span>
        </div>
        <div class="body">
            <h1>Create AutoUpload</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${autoUploadInstance}">
            <div class="errors">
                <g:renderErrors bean="${autoUploadInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="ftpurl">Ftpurl:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:autoUploadInstance,field:'ftpurl','errors')}">
                                    <input type="text" id="ftpurl" name="ftpurl" value="${fieldValue(bean:autoUploadInstance,field:'ftpurl')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="httpurl">Httpurl:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:autoUploadInstance,field:'httpurl','errors')}">
                                    <input type="text" id="httpurl" name="httpurl" value="${fieldValue(bean:autoUploadInstance,field:'httpurl')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="company">Company:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:autoUploadInstance,field:'company','errors')}">
                                    <g:select optionKey="id" from="${test2.Company.list()}" name="company.id" value="${autoUploadInstance?.company?.id}" ></g:select>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="disableauto">Disableauto:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:autoUploadInstance,field:'disableauto','errors')}">
                                    <g:checkBox name="disableauto" value="${autoUploadInstance?.disableauto}" ></g:checkBox>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastupdated">Lastupdated:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:autoUploadInstance,field:'lastupdated','errors')}">
                                    <g:select optionKey="id" from="${test2.Person.list()}" name="lastupdated.id" value="${autoUploadInstance?.lastupdated?.id}" ></g:select>
                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><input class="save" type="submit" value="Create" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
