<g:hasErrors bean="${broadcast}">
	<div class="errors">
	<g:renderErrors bean="${broadcast}" as="list" />
	</div>
</g:hasErrors>
<g:hasErrors bean="${broadcast?.unit}">
	<div class="errors">
		<g:renderErrors bean="${broadcast?.unit}" as="list" />
	</div>
</g:hasErrors>
<g:form action="save" method="post" name="fsingle" class="form-horizontal" data-test="test">
	<div class="modal-body">
		<div class="message-response"></div>
		<div class="form-group">
			<label for="broadcasttype" class="col-sm-2 control-label">Type:</label>
			<div class="col-sm-8">
				<g:select from="${['WTB','WTS']}" class="form-control" id="broadcasttype" name="broadcasttype" value="${fieldValue(bean:broadcast,field:'broadcasttype')}" ></g:select>
			</div>
		</div>
		<g:render template="/tradeunit" var="tradeunit" bean="${broadcast?.unit}" />
		<!--form-group-->
		<div class="form-group">
			<label for="description" class="col-sm-2 control-label">Description:</label>
			<div class="col-sm-8">
				<textarea class="form-control" rows="5" name="description" id="fsingle-editor"></textarea>
			</div>
		</div>
		<!--form-group-->
	</div>
	<!--modal-body-->
	<div class="modal-footer">
		<p class="ajax-resp" align="left"></p>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-8">
				<button type="button" id="submit-broadcast-single" class="btn btn-primary">Broadcasts</button>
				<button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
	<!--modal-footer-->
</g:form>
<script src="${createLinkTo(dir:'assets',file:'partszap/js/editor.js')}"></script>
<script>
$(document).ready(function(){
	$('#fsingle-editor').Editor();
});
</script>