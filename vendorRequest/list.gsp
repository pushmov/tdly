

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>VendorRequest List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New VendorRequest</g:link></span>
        </div>
        <div class="body">
            <h1>VendorRequest List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	        <g:sortableColumn property="id" title="Id" />
                        
                   	        <th>Company</th>
                   	    
                   	        <g:sortableColumn property="createdby" title="Createdby" />
                        
                   	        <g:sortableColumn property="createdon" title="Createdon" />
                        
                   	        <th>Vendor</th>
                   	    
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${vendorRequestList}" status="i" var="vendorRequest">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${vendorRequest.id}">${fieldValue(bean:vendorRequest, field:'id')}</g:link></td>
                        
                            <td>${fieldValue(bean:vendorRequest, field:'company')}</td>
                        
                            <td>${fieldValue(bean:vendorRequest, field:'createdby')}</td>
                        
                            <td>${fieldValue(bean:vendorRequest, field:'createdon')}</td>
                        
                            <td>${fieldValue(bean:vendorRequest, field:'vendor')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${VendorRequest.count()}" />
            </div>
        </div>
    </body>
</html>
