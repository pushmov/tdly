<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<!-- 
	Design by Free CSS Templates
	http://www.freecsstemplates.org
	Released for free under a Creative Commons Attribution 2.5 License
-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Emerald by Free CSS Templates</title>
<meta name="keyword" content="" />
<meta name="description" content="" />
        <link rel="stylesheet" href="${createLinkTo(dir:'css',file:'extjs/ext-all.css')}" />
        <link rel="stylesheet" href="${createLinkTo(dir:'css',file:'emerald/default.css')}" /> 
        <g:javascript src="ext-base.js" />				
        <g:javascript src="ext-all-debug.js" />				
        <g:layoutHead />
        <g:javascript library="application" />				

</head>
    <body> 
<div id="outer">
	<div id="header">
		<h1><a href="#">Emerald</a></h1>
		<h2>by Free CSS Templates</h2>
	</div>
	<div id="menu">
		<ul>
								<li>
									<a href="/test2/broadcast/list" class="registrations">
										<span class="inner">Broadcasts</span>
									</a>
								</li>
								<li>
									<a href="/test2/inventory/list" class="enhancements">
										<span class="inner">Inventory</span>
									</a>
								</li>
								<li>
									<a href="/test2/myVendor/list" class="contacts selected">
										<span class="inner">My Vendors</span>
									</a>
								</li>
								<li>
									<a href="/test2/company/show" class="card_account">
										<span class="inner">Company Information</span>
									</a>
								</li>
								<li>
									<a href="/test2/person/list" class="billing">
										<span class="inner">Manage Employees</span>
									</a>
								</li>
								<li>
									<a href="" class="administration">
										<span class="inner">RFQ</span>
									</a>
								</li>
								<li>
									<a href="/test2/myActivity/list" class="reports">
										<span class="inner">My Activity</span>
									</a>
								</li>
								<li>
									<a href="/test2/logout" class="reports">
										<span class="inner">Logout</span>
									</a>
								</li>
		</ul>
	</div>
	<div id="submenu">
		<ul>
					    <g:render template="/${params.controller}submenu"  />
		</ul>
	</div>
	<div id="content">
		<div id="primaryContentContainer">
			<div id="primaryContent" class="yui-skin-sam">
					<g:layoutBody />

			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div id="footer">
		<p>Copyright &copy; 2007 Sitename.com. Designed by <a href="http://www.freecsstemplates.org">Free CSS Templates</a></p>
	</div>
</div>
</body>
</html>
