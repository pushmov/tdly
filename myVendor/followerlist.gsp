<%@page import="test2.MyVendor" %>
<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.Inventory" %>
<%@page import="test2.Broadcast" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta name="layout" content="pz" />

<title>Admin Panel</title>
</head>

<%
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
                if (session.person?.address?.timezone)
                sdf.setTimeZone(TimeZone.getTimeZone(session.person?.address?.timezone));


%>


<div class="tablewrap">
<div class="eachtab" id="tab-1">

<div class="search_message">
    <div class="inbox_top2">
		       Sort by recent activity
		  </div>
		  <div class="clear"></div>
  <g:each in="${myVendorList}" status="i" var="myVendor">
		  <div class="inbox_main">
		    <div class="icon_img"><img src="images/company_icon.jpg" /></div>
			  <div class="con_m">
			       <div class="con_m_heading"><a href="../company/view?id=${myVendor?.company?.id}" >${myVendor?.company?.name} (${myVendor.company.address.country.name})</a></div>
				   <div class="con_m_para11">${myVendor.company.category}</div>
				   <div class="con_m_para1">${myVendor.company.address.city}, ${myVendor.company.address.state}</div>
		           </div>
			  <div class="con_m1">
			       <div class="con_m_para11"><a href="#" >Broadcasts : ${Broadcast.countByCompany(myVendor.company)} </a></div>
				   <div class="con_m_para11"><a href="#" >Inventory : ${Inventory.countByCompany(myVendor.company)}</a></div>
				   <div class="con_m_para11"><a href="#" >Followers : ${MyVendor.countByVendor(myVendor.company)}</a></div>
			  </div>
			  
			  <div class="con_m1">              
		      </div>
			  
			  
			  <div class="date_m">${myVendor.dateCreated}</div>
			  
			  <div class="clear"></div>
		  </div>

	  <div  class="clear"></div>
		</g:each>  
		  <div class="pagination_n">
		       <div class="pagination_pre"><a href="#"></a></div>			    
			     <div class="pagination_num">
                  <ul>
						<li><a href="#" class="active">1 </a> </li>
						<li><a href="#" >2</a></li>						
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>		
						<li><a href="#">5</a></li>
						 
						
		         </ul>             
			     </div>
			   <div class="pagination_next"><a href="#"></a></div>
		  </div>
		  
		  <div  class="clear"></div>
	 </div>
	 
	 <div  class="clear"></div>
</div>
			<!--inner-contrightpan end -->
</div>

		</div>
			<!--inner-contrightpan end -->
</div>
			
</html>
								
								
						
						
