

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Show RFQQuotes</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">RFQQuotes List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New RFQQuotes</g:link></span>
        </div>
        <div class="body">
            <h1>Show RFQQuotes</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>

                    
                        <tr class="prop">
                            <td valign="top" class="name">Id:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:RFQQuotes, field:'id')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Company:</td>
                            
                            <td valign="top" class="value"><g:link controller="company" action="show" id="${RFQQuotes?.company?.id}">${RFQQuotes?.company?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Fromid:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:RFQQuotes, field:'fromid')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Quotes:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:RFQQuotes, field:'quotes')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Respdate:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:RFQQuotes, field:'respdate')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Responsetext:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:RFQQuotes, field:'responsetext')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Rfq:</td>
                            
                            <td valign="top" class="value"><g:link controller="RFQ" action="show" id="${RFQQuotes?.rfq?.id}">${RFQQuotes?.rfq?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <input type="hidden" name="id" value="${RFQQuotes?.id}" />
                    <span class="button"><g:actionSubmit class="edit" value="Edit" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
