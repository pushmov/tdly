<%@page import="test2.Manufacturer" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Manufacturer List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New Manufacturer</g:link></span>
        </div>
        <div class="body">
            <h1>Manufacturer List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	        <g:sortableColumn property="id" title="Id" />
                        
                   	        <g:sortableColumn property="mfgname" title="Manufacturer" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${manufacturerList}" status="i" var="manufacturer">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${manufacturer.id}">${fieldValue(bean:manufacturer, field:'mfgname')}</g:link></td>
                        
                            <td>${fieldValue(bean:manufacturer, field:'mfgname')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${Manufacturer.count()}" />
            </div>
        </div>
    </body>
</html>
