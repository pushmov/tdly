<%@page import="test2.Person" %>

        <meta name="layout" content="pz" />


<script>
	var resetPasswordForm;
	var person;
$(document).ready(function() {

  $('#dialog-employee').dialog({
				dialogClass:'personview',
        autoOpen: false,
        height: 620,
        width: 800,
        modal: true,
        buttons: {
            Save: function() {
			$.ajax({
                            type: "POST",
                            url: "../person/saveemployee",
                            data: $("#person").serialize(),
                            success: function(data, textStatus, jqXHR) {
                                   if(data.indexOf("errors") != -1)
                                        $('#dialog-employee').html(data);
                                   else {
					$('#dialog-employee').dialog('close');
					window.location.reload();
				   }
                            },
                            error: function(data, textStatus, jqXHR) {
                                alert("Error: " + data);
                            }
                        });


            },

            Cancel: function() {
                $(this).dialog('close');
            }

        }        
    });

  $('#dialog-resetPasswordSuccess').dialog({
        autoOpen: false,
        height: 200,
        width: 350,
        modal: true,
        buttons: {
            OK: function() {

                $(this).dialog('close');

            }

        }        
    });

  $('#dialog-resetPassword').dialog({
				dialogClass:'personresetpassword',
        autoOpen: false,
        height: 400,
        width: 500,
        modal: true,
	close: function(ev, ui) { },
        buttons: {
            'Save Password': function() {
		$.ajax({
                            type: "POST",
                            url: "../person/resetPassword",
                            data: $("#resetPasswordForm").serialize(),
                            success: function(data, textStatus, jqXHR) {
                                   //$('#dialog-resetPassword').dialog('close');
                                   //$('#resetp').empty();
                                   //$('#resetp').replaceWith(data);
					//alert(data);
				   if(data.indexOf("errors") != -1) 
				        $('#dialog-resetPassword').html(data);
					//activateModal();
				   else
				   	activateSuccessModal();
				   
                            },
                            error: function(data, textStatus, jqXHR) {
                                alert("Error: " + data);
                            }
                        });
            },
            Cancel: function() {
                $(this).dialog('close');
		resetPasswordForm.resetForm();
            }

        }        
    });

});

function submitPerson() {
                    $.ajax({
                            type: "POST",
                            url: "../person/update",
                            data: $('#person').serialize(),
                            error: function(data, textStatus, jqXHR) {
                                //alert("Error: " + textStatus);
                            },
                            success: function(data, textStatus, jqXHR) {
                                        $('#dialog-employee').dialog('close');
                                        window.location.reload();
                            }
                    });
}

function activateSuccessModal() {
  $('#dialog-resetPasswordSuccess').dialog({
        autoOpen: false,
        height: 200,
        width: 350,
        modal: true,
        buttons: {
            OK: function() {

                $(this).dialog('close');

            }

        }
    });
	$('#dialog-resetPassword').dialog('close');

        $("#dialog-resetPasswordSuccess").dialog("open");


}

function activateModal() {

 $('#dialog-resetPassword').dialog({
        autoOpen: false,
        height: 400,
        width: 500,
        modal: true,
        close: function(ev, ui) { },
        buttons: {
            'Save Password': function() {
                $.ajax({
                            type: "POST",
                            url: "../person/resetPassword",
                            data: $("#resetPasswordForm").serialize(),
                            success: function(data, textStatus, jqXHR) {
                                   $('#dialog-resetPassword').dialog('close');
                                   $('#resetp').html(data);
                                   if(data.indexOf("errors") != -1)
                                        activateModal();
                                   else
                                        activateSuccessModal();

                            },
                            error: function(data, textStatus, jqXHR) {
                                alert("Error: " + data);
                            }
                        });
            },
            Cancel: function() {
                $(this).dialog('close');
            }

        }
    });

        $("#dialog-resetPassword").dialog("open");

}

function resetPassword() {

        $(".ui-tooltip-content errors").val("");
        $("#dialog-resetPassword").dialog("open");

}

function setupFormValidation() {

$(function() {
 person = $('#person');
person = person.validate({
onkeyup: false,
errorClass: 'invalid',
errorElement: 'label',	
validClass: 'success',			
onsubmit: true,
submitHandler: submitPerson,


errorContainer: '#personErr',
errorLabelContainer: 'div.errors ul',
wrapper: 'li',	
			
rules: {
firstname: { required: true },
lastname: { required: true },
aol: { },
yahoo: { },
msn: { },
gtalk: { },
skype: { },
officephone: { },
cellphone: { },
tollfreephone: { },
title: { },
address: { required: true },
accountExpired: { required: true },
accountLocked: { required: true },
auth: { required: true },
authorities: { },
company: { required: true },
dateCreated: { date: true },
description: { required: true },
email: { required: true },
"address.city" : { required: true },
"address.postalcode" : { required: true },
"address.state" :  { required: true },
"address.country.id" :  { required: true },
"address.street1" :  { required: true },
emailShow: { required: true },
enabled: { required: true },
lastUpdated: { date: true },
passwd: { required: true },
passwordExpired: { required: true },
role: { required: true }
},
messages: {
firstname: { required: 'First Name cannot be blank' },
lastname: { required: 'Last Name cannot be blank' },
aol: { },
yahoo: { },
msn: { },
gtalk: { },
skype: { },
officephone: { },
cellphone: { },
tollfreephone: { },
title: { },
address: { required: 'Property [address] of class [class test2.Person] cannot be null' },
accountExpired: { required: 'Property [accountExpired] of class [class test2.Person] cannot be null' },
accountLocked: { required: 'Property [accountLocked] of class [class test2.Person] cannot be null' },
auth: { required: 'Property [auth] of class [class test2.Person] cannot be null' },
authorities: { },
company: { required: 'Property [company] of class [class test2.Person] cannot be null' },
dateCreated: { date: 'Property dateCreated must be a valid Date' },
description: { required: 'Property [description] of class [class test2.Person] cannot be null' },
email: { required: 'Email cannot be blank' },
"address.city" : { required: 'City cannot be blank'},
"address.postalcode" : { required: 'Postalcode cannot be blank'},
"address.state" : { required: 'State cannot be blank'},
"address.country.id" : { required: 'Country cannot be blank'},
"address.street1" : { required: 'Street1 cannot be blank'},
emailShow: { required: 'Property [emailShow] of class [class test2.Person] cannot be null' },
enabled: { required: 'Property [enabled] of class [class test2.Person] cannot be null' },
lastUpdated: { date: 'Property lastUpdated must be a valid Date' },
passwd: { required: 'Property [passwd] of class [class test2.Person] cannot be null' },
passwordExpired: { required: 'Property [passwordExpired] of class [class test2.Person] cannot be null' },
role: { required: 'Property [role] of class [class test2.Person] cannot be null' }
}
});
});

}

function editPerson(id) {

	$("returnTemplate").val("row");

	

			$.ajax({
                            type: "GET",
                            url: "../person/showajax",
                            cache: false,
                            data: {id: id, returnTemplate: 'row'}, 
                            error: function(data, textStatus, jqXHR) {
                                alert("Error: " + textStatus);
                            },
                            success: function(data, textStatus, jqXHR) {
					$("#empDiv").html(data);        
					setupFormValidation();
					//person.resetForm();	
					$("#dialog-employee").dialog("open");        

                            }

                            });




}

function addPerson() {



	$.get("../person/createperson",

		function(data,textStatus,jqXHR) {
			
			$("#dialog-employee").html(data)
				        
			$('#dialog-employee').dialog("open");
		}

        );

}


</script>





<g:render template="countryJs"  bean="${person}"   />

          					    <g:render template="employee" bean="${person}" />

 



<div id="resetp">
<g:render template="/person/resetPassword" var="person" bean="${person}" />
</div>
<div class="tablewrap">
<div class="eachtab" id="tab-1">

<g:isAdmin person="${person}" >
                                          <div class="i-headerwrapper1">
                                                        <ul>
                                                        <li>
																													<!--<a href="javascript:void(0)" id="respond"   onClick="javascript:addPerson();"     title="Home" class="active"><span>Add User</span></a>-->
																													<button type="button" id="respond" onClick="javascript:addPerson();" title="Home" class="btn btn-broadcast">Add User</button>
																												</li>
                                                        </ul>
                                                        <ul><li><br/></li></ul>
                                                        </div>
<br/>
<br/>
</g:isAdmin>
<table width="100%" border="0" cellspacing="1" cellpadding="0"  class="main_table"  >								<tr>
                   	        <g:sortableColumn property="firstname" title="firstname" />
                   	        <g:sortableColumn property="lastname" title="lastname" />
                   	        <g:sortableColumn property="email" title="email" />
                   	        <g:sortableColumn property="officephone" title="officephone" />
                   	        <g:sortableColumn property="cellphone" title="cellphone" />
                   	        <g:sortableColumn property="role" title="role" />
									<th>Actions</th>


								</tr>
</thead>
 <tfoot>
    </tfoot>
<tbody>

                    <g:each in="${personList}" status="i" var="person">
			<g:render template="personRow" var="person" bean="${person}" />
                    </g:each>
								
                    </tbody>
							
								
							</table>
	<div class="pagination">
		<ul>
			                <g:paginate total="${total}" />
		</ul>
	</div>

		</div>
			<!--inner-contrightpan end -->
</div>

