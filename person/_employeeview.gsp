<r:script>
   $(document).ready(function() {

  $('#resetPassword').click( function() {
	 $("#dialog-resetPassword").dialog("open");
  });

  $('#dialog-resetPasswordSuccess').dialog({
        autoOpen: false,
        height: 200,
        width: 350,
        modal: true,
        buttons: {
            OK: function() {

                $(this).dialog('close');

            }

        }        
    });

  $('#dialog-resetPassword').dialog({
        autoOpen: false,
        height: 400,
        width: 650,
        modal: true,
        buttons: {
            SaveFilter: function() {

                $(this).dialog('close');

			$.ajax({
    			    type: "POST",
    			    url: "${request.contextPath}/person/resetPassword",
    			    data: $("#resetPasswordForm").serialize(),
    			    success: function(data, textStatus, jqXHR) {
				   $('#dialog-message').html(data.message);
				   $('#dialog-resetPasswordSuccess').dialog("open");
    			    },
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + data.messsage);
    			    }
			    
                        });


            },

            Cancel: function() {
                $(this).dialog('close');
            }

        }
    });


   });

</r:script>
<div id="dialog-resetPassword" >
        <g:render template="/person/resetPassword" var="person" bean="${person}" />
</div>

<div id="dialog-resetPasswordSuccess" >
<p><div id="dialog-message"></div></p>
</div>

<g:form action="edit" method="post" class="form-horizontal">
	
	<div class="row">
		<div class="col-sm-12 col-md-6 col-lg-6">
			<ul class="unstyled opt-list">
				<li><span>Name</span> : <strong>${person.firstname}&nbsp;${person.lastname}</strong></li>
				<li><span>Title</span> : <strong>${person.title}</strong></li>
				<li><span>Email</span> : <strong>${person?.email}</strong></li>
				<li><span>Cell</span> : <strong>${person?.cellphone}</strong></li>
				<li><span>Skype</span> : <strong>${person?.skype}</strong></li>
				<li><span>Yahoo</span> : <strong>${person?.yahoo}</strong></li>
				<li><span>MSN</span> : <strong>${person?.msn}</strong></li>
				<li><span>Gtalk</span> : <strong>${person?.gtalk}</strong></li>
				<li><span>AOL</span> : <strong>${person?.aol}</strong></li>
			</ul>
		</div>
		<div class="col-sm-12 col-md-6 col-lg-6">
			<ul class="unstyled opt-list">
				<li><span>Company</span> : <strong>${person.company.name}</strong></li>
				<li><span>Address</span> : <strong>${person?.address?.street1}</strong></li>
				<li><span>City</span> : <strong>${person?.address?.city}</strong></li>
				<li><span>State</span> : <strong>${person?.address?.state}</strong></li>
				<li><span>Zip</span> : <strong>${person?.address?.postalcode}</strong></li>
				<li><span>Country</span> : <strong>${person?.address?.country?.name}</strong></li>
				<li><span>Phone</span> : <strong>${person?.address?.phone}</strong></li>
				<li><span>Toll Free</span> : <strong>${person?.address?.tollfreephone}</strong></li>
				<li><span>Fax</span> : <strong>${person?.address?.fax}</strong></li>
			</ul>
		</div>
	</div>
	
	<div class="form-group clearfix">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<g:isSelf person="${person}" >
			<button type="button" onClick="javascript:editPerson('${person.id}');" title="Home" class="btn btn-broadcast">Edit Profile</button>
			</g:isSelf>
			
			<g:ifNotConnection person="${person}" >
			<button type="button" id="respond" onClick="javascript:addConnection('${person.id}');" class="btn btn-broadcast">Add to MyConnections</button>
			</g:ifNotConnection>
		</div>
	</div>

</g:form>

<g:render template="employee" />