<%@page import="test2.Role" %>					
<%@page import="test2.Country" %>


					  <table data-test="test" width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td width="49%" align="left" valign="top">
						  <table width="100%" border="0" cellspacing="0" cellpadding="0">
							  <tr>
								<td align="left" valign="center" width="145" class="gtext">First Name</td>
								<td align="left" valign="center" width="2" class="gtext">:</td>
								<td align="left" valign="top" width="230"><input type="text" name="firstname" id="firstname" class="inptinpt" value="${fieldValue(bean:person,field:'firstname')}" /></td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="center" width="145" class="gtext">Last Name</td>
								<td align="left" valign="center" width="2" class="gtext">:</td>
								<td align="left" valign="top" width="230"><input type="text" name="lastname" id="category" class="inptinpt" value="${fieldValue(bean:person,field:'lastname')}" /></td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="center" width="145" class="gtext">Title</td>
								<td align="left" valign="center" width="2" class="gtext">:</td>
								<td align="left" valign="top" width="230"><input type="text" name="title" id="category" class="inptinpt" value="${fieldValue(bean:person,field:'title')}" /></td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="center" width="145" class="gtext">Email</td>
								<td align="left" valign="center" width="2" class="gtext">:</td>
								<td align="left" valign="top" width="230"><input type="text" name="email"  value="${person?.email?.encodeAsHTML()}"   id="phone" class="inptinpt" /></td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="center" width="145" class="gtext">Cell Phone</td>
								<td align="left" valign="center" width="2" class="gtext">:</td>
								<td align="left" valign="top" width="230"><input type="text" name="cellphone"  value="${person?.cellphone}"   id="toll" class="inptinpt" /></td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="center" width="145" class="gtext">Skype</td>
								<td align="left" valign="center" width="2" class="gtext">:</td>
								<td align="left" valign="top" width="230"><input type="text" name="skype"  value="${person?.skype?.encodeAsHTML()}"   id="fax" class="inptinpt" /></td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="center" width="145" class="gtext">Yahoo</td>
								<td align="left" valign="center" width="2" class="gtext">:</td>
								<td align="left" valign="top" width="230"><input type="text" name="yahoo"  value="${person?.yahoo?.encodeAsHTML()}"   id="yearf" class="inptinpt" /></td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="center" width="145" class="gtext">Msn</td>
								<td align="left" valign="center" width="2" class="gtext">:</td>
								<td align="left" valign="top" width="230"><input type="text" name="msn"  value="${person?.msn?.encodeAsHTML()}"   id="employee" class="inptinpt" /></td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="center" width="145" class="gtext">Gtalk</td>
								<td align="left" valign="center" width="2" class="gtext">:</td>
								<td align="left" valign="top" width="230"><input type="text" name="gtalk"  value="${person?.gtalk?.encodeAsHTML()}"   id="employee" class="inptinpt" /></td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="center" width="145" class="gtext">AOL</td>
								<td align="left" valign="center" width="2" class="gtext">:</td>
								<td align="left" valign="top" width="230"><input type="text" name="aol"  value="${person?.aol?.encodeAsHTML()}"   id="employee" class="inptinpt" /></td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
<g:if test="${referer == 'page'}" >
							  <tr>
								<td align="left" valign="center" width="145" class="gtext">&nbsp;</td>
								<td align="left" valign="center" width="2" class="gtext">&nbsp;</td>
								<td align="left" valign="top" width="230"> <input type="submit" value="Update" name="update" id="update" class="updatebtn" /></td>
							  </tr>
</g:if>
							</table>

						</td>
						<td align="left" valign="top" width="1%">&nbsp;</td>
						<td align="left" valign="top" width="50%">
						   <table width="100%" border="0" cellspacing="0" cellpadding="0">
					<g:ifNotSelf person="${person}" >
							  <tr>
								<td align="left" valign="center" width="125" class="gtext">Role</td>
								<td align="left" valign="center" width="2" class="gtext">:</td>
								<td align="left" valign="top" width="220">
									
										<div class="ddOutOfVision" style="height:0px;overflow:hidden;position:absolute;" id="websites2_msddHolder">
											<div id="filterselect">
												<g:select from="${roles}" optionKey="id" optionValue="description" id="websites2" name="role.id" value="${person?.role?.id}" ></g:select>
											</div>
										</div>
										<div id="websites2_msdd" class="dd2" style="width: 193px;">
											<div id="websites2_title" class="ddTitle">
												<span id="websites2_arrow" class="arrow" style="background-position: 0px 0px;"></span>
												<span class="ddTitleText" id="websites2_titletext">
													<span class="ddTitleText">WTB_My Inventory</span>
												</span>
											</div>
											<div id="websites2_child" class="ddChild" style="width: 191px;">
												<a href="javascript:void(0);" class="selected enabled" style="[object CSSStyleDeclaration]" id="websites2_msa_0">
													<span class="ddTitleText">WTB_My Inventory</span>
												</a>
												<a href="javascript:void(0);" class="enabled" style="[object CSSStyleDeclaration]" id="websites2_msa_1">
													<span class="ddTitleText">Demo 1</span>
												</a>
											</div>
										</div>

								</td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
					</g:ifNotSelf>

							  <tr>
								<td align="left" valign="center" width="125" class="gtext">Address</td>
								<td align="left" valign="center" width="2" class="gtext">:</td>
								<td align="left" valign="top" width="220">
									<input type="text" name="address.street1"  value="${person?.address?.street1?.encodeAsHTML()}"  id="address" class="inptinpt" />
<!--	<textarea name="address.street1" id="address" class="textareainpt">${person?.address?.street1?.encodeAsHTML()}</textarea>-->
								</td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="center" width="125" class="gtext">Country</td>
								<td align="left" valign="center" width="2" class="gtext">:</td>
								<td align="left" valign="top" width="220">
										<div class="ddOutOfVision" style="height:0px;overflow:hidden;position:absolute;" id="countrySelect_msddHolder">
											<div id="filterselect">
												<g:select class="select-general" id="countrySelect" optionKey="id" from="${Country.list(sort:'name', order:'asc')}"  style="width:214px;"   optionValue="name" name="address.country.id"  value="${person?.address?.country?.id}"  noSelection="['':'-Please select your country-']" ></g:select>
											</div>
										</div>
										<div id="countrySelect_msdd" class="dd2" style="width: 193px;">
											<div id="countrySelect_title" class="ddTitle">
												<span id="countrySelect_arrow" class="arrow" style="background-position: 0px 0px;"></span>
												<span class="ddTitleText" id="countrySelect_titletext">
													<span class="ddTitleText">WTB_My Inventory</span>
												</span>
											</div>
											<div id="countrySelect_child" class="ddChild">
												<a href="javascript:void(0);" class="selected enabled" style="[object CSSStyleDeclaration]" id="countrySelect_msa_0">
													<span class="ddTitleText">WTB_My Inventory</span>
												</a>
												<a href="javascript:void(0);" class="enabled" style="[object CSSStyleDeclaration]" id="countrySelect_msa_1">
													<span class="ddTitleText">Demo 1</span>
												</a>
											</div>
										</div>


								</td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="center" width="125" class="gtext">State</td>
								<td align="left" valign="center" width="2" class="gtext">:</td>
								<td align="left" valign="top" width="220">
										<div class="ddOutOfVision" style="height:0px;overflow:hidden;position:absolute;" id="selectstate_msddHolder">
											<div id="filterselect">
												<g:select id="selectstate" optionKey="name" from="${flash.stateSelected}" optionValue="name" class="select-general" name="address.state" value="${person?.address?.state}"  style="width:214px;"  noSelection="['':'-Please select your State/Region-']" ></g:select>
											</div>
										</div>
										<div id="selectstate_msdd" class="dd2" style="width: 193px;">
											<div id="selectstate_title" class="ddTitle">
												<span id="selectstate_arrow" class="arrow" style="background-position: 0px 0px;"></span>
												<span class="ddTitleText" id="selectstate_titletext">
													<span class="ddTitleText">WTB_My Inventory</span>
												</span>
											</div>
											<div id="selectstate_child" class="ddChild">
												<a href="javascript:void(0);" class="selected enabled" style="[object CSSStyleDeclaration]" id="selectstate_msa_0">
													<span class="ddTitleText">WTB_My Inventory</span>
												</a>
												<a href="javascript:void(0);" class="enabled" style="[object CSSStyleDeclaration]" id="selectstate_msa_1">
													<span class="ddTitleText">Demo 1</span>
												</a>
											</div>
										</div>
								</td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="center" width="125" class="gtext">City</td>
								<td align="left" valign="center" width="2" class="gtext">:</td>
								<td align="left" valign="top" width="220"><input type="text" name="address.city" value="${person?.address?.city?.encodeAsHTML()}"  id="city" class="inptinpt" /></td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="center" width="125" class="gtext">Zip</td>
								<td align="left" valign="center" width="2" class="gtext">:</td>
								<td align="left" valign="top" width="220"><input type="text" name="address.postalcode" value="${person?.address?.postalcode}"  id="zip" class="inptinpt" /></td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="center" width="125" class="gtext">Phone</td>
								<td align="left" valign="center" width="2" class="gtext">:</td>
								<td align="left" valign="top" width="220"><input type="text" name="address.phone" value="${person?.address?.phone}"  id="zip" class="inptinpt" /></td>
							  </tr>
<tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="center" width="125" class="gtext">Toll Free Phone</td>
								<td align="left" valign="top" width="2" class="gtext">:</td>
	<td align="left" valign="top" width="230"><input type="text" name="address.tollfreephone"  value="${person?.address?.tollfreephone}"   id="toll" class="inptinpt" /></td>
							  </tr>
<tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="center" width="125" class="gtext">Fax</td>
								<td align="left" valign="center" width="2" class="gtext">:</td>
								<td align="left" valign="top" width="230"><input type="text" name="address.fax"  value="${person?.address?.fax?.encodeAsHTML()}"   id="fax" class="inptinpt" /></td>
							  </tr>
							</table>
						</td>
					  </tr>
					</table>

					
					<script>
					$(document).ready(function() {
						try {
							var oHandler7 = $("#selectstate").msDropDown({mainCSS:'dd2'}).data("dd");
							var oHandler2 = $("#websites2").msDropDown({mainCSS:'dd2'}).data("dd");
							var oHandler3 = $("#countrySelect").msDropDown({mainCSS:'dd3'}).data("dd");
							$("#ver").html($.msDropDown.version);
						} catch(e) {
							alert("Error: "+e.message);
						}
													
					});
					</script>