<%@page import="test2.Person" %>
<%@page import="test2.MyConnection" %>
<%@page import="test2.Inventory" %>
<%@page import="test2.Broadcast" %>
<%@page import="com.ucbl.util.PZUtil" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="pz" />
        <title>Show Company</title>

<r:script>
	$(document).ready(function() {


	    $('#dialog-message').dialog({
        	autoOpen: false,
	        height: 250,
        	width: 350,
	        modal: true,
        	buttons: {
		            Ok: function() {
        		        $(this).dialog('close');
            		    }

	        }        
             });

       });

</r:script>

    </head>
<r:require module="network" />	
<g:render template="/dialog-broadcast-response" />
    <body>
<div id="dialog-message" title="Watchlist"> 
	<p> 
		 <div id="respMessage"></div> 
	</p> 
</div> 
<div class="tablewrap">

<div class="eachtab" id="tab-1">

  <div class="search_message">
    <div class="inbox_top2">
		       Sort by recent activity
		  </div>
		  <div class="clear"></div>
                    <g:each in="${personList}" status="i" var="person">
		  <div class="inbox_main">
		    <div class="icon_img"><img src="images/icon_img.jpg" /></div>
			  <div class="con_m">
			       <div class="con_m_heading"><g:link controller="person" action="profile" id="${person?.id}" >${person.firstname} ${person.lastname} (${person.company.address.country.name})</g:link></div>
				   <div class="con_m_para11">${person.title}</div>
				   <div class="con_m_para1">${person.company.address.city}, ${person.company.address.state}</div>
<!--
				<div class="con_icon"><a href="javascript:void(0)" id="respond" onClick="javascript:showResponse();" ><img src="../images/mailbox.png" alt="Mailbox" title="Mailbox" /></a></div>

		           <div class="buttons1"><a href="#" >Message</a></div> 
  -->
                       <g:ifNotConnection person="${person}"> 
		           <div class="buttons1"><a href="javascript:void(0)" id="respond" onClick="javascript:addConnection('${person?.id}');"   >Connect</a></div>
                       </g:ifNotConnection> 
			  </div>
			  
			  <div class="con_m1">
			       <div class="con_m_para11"><a href="../broadcast/search?createdById=${person?.id}" >Broadcasts : ${Broadcast.countByCreatedby(person)}</a></div>
				   <div class="con_m_para11"><a href="../inventory/getInventory?pid=${person?.id}" >Inventory : ${Inventory.countByCreatedBy(person)}</a></div>
				   <div class="con_m_para11"><a href="../myConnection/list?pid=${person?.id}" >Connection : ${MyConnection.countByPerson(person)}</a></div>
			  </div>
			  
			  <div class="clear"></div>
		  </div>
                    </g:each>
		  
		  
		  <div  class="clear"></div>
			<div class="pagination_n">
			<g:paginate controller="person" action="searchpeople" params="[keyword: flash.keyword, searchtype: flash.searchtype ]" total="${total}" />
			</div>
	 
	 <div  class="clear"></div>
</div>
            </div>
</html>
