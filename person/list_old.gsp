

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="emerald" />
        <title>Person List</title>
	<g:javascript library="prototype" />

    </head>
    <body>
        <div class="body">
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	        <g:sortableColumn property="firstname" title="firstname" />
                   	        <g:sortableColumn property="lastname" title="lastname" />
                   	        <g:sortableColumn property="email" title="email" />
                   	        <g:sortableColumn property="officephone" title="officephone" />
                   	        <g:sortableColumn property="cellphone" title="cellphone" />
                   	        <g:sortableColumn property="tollfreephone" title="tollfreephone" />
                   	        <g:sortableColumn property="emprole" title="emprole" />
                   	        <g:sortableColumn property="aol" title="Aol" />
                        
                   	        <g:sortableColumn property="yahoo" title="Yahoo" />
                        
                   	        <g:sortableColumn property="msn" title="Msn" />
                        
                   	        <g:sortableColumn property="gtalk" title="Gtalk" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${personList}" status="i" var="person">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td>${fieldValue(bean:person, field:'firstname')}</td>
                            <td>${fieldValue(bean:person, field:'lastname')}</td>
                            <td>${fieldValue(bean:person, field:'email')}</td>
                            <td>${fieldValue(bean:person, field:'officephone')}</td>
                            <td>${fieldValue(bean:person, field:'cellphone')}</td>
                            <td>${fieldValue(bean:person, field:'tollfreephone')}</td>
                            <td>${fieldValue(bean:person, field:'emprole')}</td>
                        
                            <td>${fieldValue(bean:person, field:'aol')}</td>
                        
                            <td>${fieldValue(bean:person, field:'yahoo')}</td>
                        
                            <td>${fieldValue(bean:person, field:'msn')}</td>
                        
                            <td>${fieldValue(bean:person, field:'gtalk')}</td>
                            <td>
<g:ifAnyGranted role="ROLE_ADMIN,ROLE_SUPERADMIN,ROLD_SYSADMIN">
                <g:form>
                    <input type="hidden" name="id" value="${person?.id}" />
                    <span class="button"><g:actionSubmit class="edit" value="Edit" /></span>
                </g:form>
                <g:form>
                    <input type="hidden" name="id" value="${person?.id}" />
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </g:form>
               <g:form>
                    <input type="hidden" name="id" value="${person?.id}" />
                    <span class="button"><g:actionSubmit class="resetpassword" value="Reset Password" /></span>
                </g:form>
</g:ifAnyGranted>

</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${Person.count()}" />
            </div>
        </div>
    </body>
</html>
