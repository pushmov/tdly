

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Create Person</title>         
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Person List</g:link></span>
        </div>
        <div class="body">
            <h1>Create Person</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${person}">
            <div class="errors">
                <g:renderErrors bean="${person}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="businesstype">Businesstype:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:person,field:'businesstype','errors')}">
                                    <input type="text" id="businesstype" name="businesstype" value="${fieldValue(bean:person,field:'businesstype')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="aol">Aol:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:person,field:'aol','errors')}">
                                    <input type="text" id="aol" name="aol" value="${fieldValue(bean:person,field:'aol')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="yahoo">Yahoo:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:person,field:'yahoo','errors')}">
                                    <input type="text" id="yahoo" name="yahoo" value="${fieldValue(bean:person,field:'yahoo')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="msn">Msn:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:person,field:'msn','errors')}">
                                    <input type="text" id="msn" name="msn" value="${fieldValue(bean:person,field:'msn')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="gtalk">Gtalk:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:person,field:'gtalk','errors')}">
                                    <input type="text" id="gtalk" name="gtalk" value="${fieldValue(bean:person,field:'gtalk')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="officephone">Officephone:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:person,field:'officephone','errors')}">
                                    <input type="text" id="officephone" name="officephone" value="${fieldValue(bean:person,field:'officephone')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="cellphone">Cellphone:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:person,field:'cellphone','errors')}">
                                    <input type="text" id="cellphone" name="cellphone" value="${fieldValue(bean:person,field:'cellphone')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="tollfreephone">Tollfreephone:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:person,field:'tollfreephone','errors')}">
                                    <input type="text" id="tollfreephone" name="tollfreephone" value="${fieldValue(bean:person,field:'tollfreephone')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="emprole">Emprole:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:person,field:'emprole','errors')}">
                                    <input type="text" id="emprole" name="emprole" value="${fieldValue(bean:person,field:'emprole')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="auth">Auth:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:person,field:'auth','errors')}">
                                    
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="company">Company:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:person,field:'company','errors')}">
                                    <g:select optionKey="id" from="${Company.list()}" name="company.id" value="${person?.company?.id}" ></g:select>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="email">Email:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:person,field:'email','errors')}">
                                    <input type="text" id="email" name="email" value="${fieldValue(bean:person,field:'email')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="firstname">Firstname:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:person,field:'firstname','errors')}">
                                    <input type="text" id="firstname" name="firstname" value="${fieldValue(bean:person,field:'firstname')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastname">Lastname:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:person,field:'lastname','errors')}">
                                    <input type="text" id="lastname" name="lastname" value="${fieldValue(bean:person,field:'lastname')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="password">Password:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:person,field:'password','errors')}">
                                    <input type="text" id="password" name="password" value="${fieldValue(bean:person,field:'password')}"/>
                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><input class="save" type="submit" value="Create" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
