
<%@page import="test2.Person" %>
        <meta name="layout" content="pz" />
		<link href="${createLinkTo(dir:'assets',file:'partszap/css/app.css')}" rel="stylesheet">

<script>
        var person;
	$(document).ready(function() {

	    $('#dialog-message').dialog({
        	autoOpen: false,
	        height: 150,
        	width: 250,
	        modal: true,
        	buttons: {
		            Ok: function() {
        		        $(this).dialog('close');
            		    }

	        }        
             });


	  $('#dialog-employee').dialog({
					dialogClass:'personview',
        	autoOpen: false,
	        height: 600,
        	width: 800,
	        modal: true,
        	buttons: {
	            SaveFilter: function() {

             $.ajax({
                            type: "POST",
                            url: "../person/saveemployee",
                            data: $("#person").serialize(),
                            success: function(data, textStatus, jqXHR) {
                                   if(data.indexOf("errors") != -1)
                                        $('#dialog-employee').html(data);
                                   else {
                                        $('#dialog-employee').dialog('close');
                                        window.location.reload();
                                   }
                            },
                            error: function(data, textStatus, jqXHR) {
                                alert("Error: " + data);
                            }
                        });


	            },
	            Cancel: function() {
        	        $(this).dialog('close');
            	    }

	        }        
	   });

  $('#dialog-resetPassword').dialog({
        autoOpen: false,
        height: 400,
        width: 650,
        modal: true,
        buttons: {
            SaveFilter: function() {

    		$("#resetPasswordForm").submit();
                $(this).dialog('close');

            },

            Cancel: function() {
                $(this).dialog('close');
            }

        }        
    });




       });

function submitPerson() {
                    $.ajax({
                            type: "POST",
                            url: "../person/update",
                            data: $('#person').serialize(),
                            error: function(data, textStatus, jqXHR) {
                                //alert("Error: " + textStatus);
                            },
                            success: function(data, textStatus, jqXHR) {
					$('#dialog-employee').dialog('close');
					window.location.reload();
                            }

                    });


}

function editPerson(id) {

/*
	$.get("../person/showajax",{id: id, returnTemplate: 'view'},

		function(data,textStatus,jqXHR) {
			
		$("#dialog-employee").html(data);
		        
			$("#dialog-employee").dialog("open");
		}

        );
*/
		$('#edit-employee').click();
		
		$.ajax({
                            type: "GET",
                            url: "../person/showajax",
                            cache: false,
                            data: {id: id, returnTemplate: 'row'},
                            error: function(data, textStatus, jqXHR) {
                                alert("Error: " + textStatus);
                            },
                            success: function(data, textStatus, jqXHR) {
                                        $("#response").html(data);
                                        setupFormValidation();
                                        //person.resetForm();
                                        //$("#dialog-employee").dialog("open");
										
										

                            }

                });


}

function openResetPassword() {
		        
	$("#dialog-resetPassword").dialog("open");

}


function addConnection(id) {

			    $.ajax({
    			    type: "POST",
    			    url: "../connectionRequest/save",
			    cache: false,
    			    data: { id : id , message : 'Hi I would like to add you to my connections Thanks' },
			    error: function(data, textStatus, jqXHR) {
				   $('#respMessage').val(data.msg);
				   var foo = $('#dialog-message').dialog("open");
				   foo.html(data.msg);
    			    },
    			    success: function(data, textStatus, jqXHR) {
				   $('#respMessage').val(data.msg);
				   var foo = $('#dialog-message').dialog("open");
				   foo.html(data.msg);
 				   //setTimeout(function(){$('#dialog-message').dialog("close")},2000);
        			
    			    }
			    
                            });

}

function setupFormValidation() {

$(function() {
 person = $('#person');
person = person.validate({
onkeyup: false,
errorClass: 'invalid',
errorElement: 'label',
validClass: 'success',
onsubmit: true,
submitHandler: submitPerson,


errorContainer: '#personErr',
errorLabelContainer: 'div.errors ul',
wrapper: 'li',

rules: {
firstname: { required: true },
lastname: { required: true },
aol: { },
yahoo: { },
msn: { },
gtalk: { },
skype: { },
officephone: { },
cellphone: { },
tollfreephone: { },
title: { },
address: { required: true },
accountExpired: { required: true },
accountLocked: { required: true },
auth: { required: true },
authorities: { },
company: { required: true },
dateCreated: { date: true },
description: { required: true },
email: { required: true },
"address.city" : { required: true },
"address.postalcode" : { required: true },
"address.state" :  { required: true },
"address.country.id" :  { required: true },
"address.street1" :  { required: true },
emailShow: { required: true },
enabled: { required: true },
lastUpdated: { date: true },
passwd: { required: true },
passwordExpired: { required: true },
role: { required: true }
},
messages: {
firstname: { required: 'First Name cannot be blank' },
lastname: { required: 'Last Name cannot be blank' },
aol: { },
yahoo: { },
msn: { },
gtalk: { },
skype: { },
officephone: { },
cellphone: { },
tollfreephone: { },
title: { },
address: { required: 'Property [address] of class [class test2.Person] cannot be null' },
accountExpired: { required: 'Property [accountExpired] of class [class test2.Person] cannot be null' },
accountLocked: { required: 'Property [accountLocked] of class [class test2.Person] cannot be null' },
auth: { required: 'Property [auth] of class [class test2.Person] cannot be null' },
authorities: { },
company: { required: 'Property [company] of class [class test2.Person] cannot be null' },
dateCreated: { date: 'Property dateCreated must be a valid Date' },
description: { required: 'Property [description] of class [class test2.Person] cannot be null' },
email: { required: 'Email cannot be blank' },
"address.city" : { required: 'City cannot be blank'},
"address.postalcode" : { required: 'Postalcode cannot be blank'},
"address.state" : { required: 'State cannot be blank'},
"address.country.id" : { required: 'Country cannot be blank'},
"address.street1" : { required: 'Street1 cannot be blank'},
emailShow: { required: 'Property [emailShow] of class [class test2.Person] cannot be null' },
enabled: { required: 'Property [enabled] of class [class test2.Person] cannot be null' },
lastUpdated: { date: 'Property lastUpdated must be a valid Date' },
passwd: { required: 'Property [passwd] of class [class test2.Person] cannot be null' },
passwordExpired: { required: 'Property [passwordExpired] of class [class test2.Person] cannot be null' },
role: { required: 'Property [role] of class [class test2.Person] cannot be null' }
}
});
});
}

</script>


<main class="content" role="main">
	<div class="main-content person-view">
		<div class="container">
		<g:render template="countryJs"  bean="${person}"   />
		<g:render template="employeeview" />
		</div>
	</div>
</main>
<button type="button" data-toggle="modal" id="edit-employee" data-target="#modal_edit_employee"></button>