<%@page import="test2.Person" %>
<%@page import="com.ucbl.util.PZUtil" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="pz" />
        <title>Show Company</title>

<r:script>
	$(document).ready(function() {

	    $('#dialog-message').dialog({
        	autoOpen: false,
	        height: 250,
        	width: 350,
	        modal: true,
        	buttons: {
		            Ok: function() {
        		        $(this).dialog('close');
            		    }

	        }        
             });


	  $('#dialog-employee').dialog({
        	autoOpen: false,
	        height: 600,
        	width: 650,
	        modal: true,
        	buttons: {
	            SaveFilter: function() {
	
    			$("#person").submit();
                	$(this).dialog('close');

	            },
	            Cancel: function() {
        	        $(this).dialog('close');
            	    }

	        }        
	   });

  $('#dialog-resetPassword').dialog({
        autoOpen: false,
        height: 400,
        width: 650,
        modal: true,
        buttons: {
            SaveFilter: function() {

    		$("#resetPasswordForm").submit();
                $(this).dialog('close');

            },

            Cancel: function() {
                $(this).dialog('close');
            }

        }        
    });


           $('#person').submit( function() {
    
		    $.ajax({
			    type: "POST",
    			    url: "../person/update",
    			    data: $(this).serialize(),
			    error: function(data, textStatus, jqXHR) {
			        //alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
					//alert('success');
				    $('#employeeview').html(data);
				   //$('#dialog-responseSave').dialog("open");
    			    }
			    
                    });

       	            return false;

            });


       });


function editPerson(id) {

	$("returnTemplate").val("dialog");

	$.get("../person/showajax",{id: id},

		function(data,textStatus,jqXHR) {
			
			//$("#dialog-employee").replaceWith(data)
		        
			$("#dialog-employee").dialog("open");
		}

        );

}

function openResetPassword() {
		        
	$("#dialog-resetPassword").dialog("open");

}



</r:script>

<r:require module="network" />

    </head>
<g:render template="/dialog-broadcast-response" />
<g:render template="/moreBroadcast" />

<div id="dialog-message" title="Connection Request">

</div>

<div class="tablewrap">


<br/>
<br/>
<div class="eachtab" id="tab-1">
<div class="box">
		
		
		<!-- Left Menu Start -->
<div class="left_side">
  
	       <div id="basic-accordian" >
  <div id="test-header" class="accordion_headings header_highlight" >Personal</div>
  <div id="test-content">  
    <div class="accordion_child">
    	<div class="accordion_con">${person.firstname} ${person.lastname}<br />
${person.title}<br />
Office: ${person?.address?.phone}<br />
Cell: ${person?.cellphone}<br />
${person?.email} <br />
Yahoo: ${person?.yahoo} <br />
Skype: ${person?.skype} <br />
Aol: ${person?.aol} <br />
gtalk: ${person.gtalk} <br />
Msn : ${person?.msn} <br />
Broadcasts : <a href="${request.contextPath}/broadcast/search?createdById=${person.id}">${broadcastCount}</a> <br />
Inventory: <a href="${request.contextPath}/inventory/getInventory?pid=${person.id}" >${inventoryCount}</a> <br />
		</div>
		
    </div>    
  </div>
  <!--End of each accordion item--> 

  <div id="test1-header" class="accordion_headings" >Company</div>  
  <div id="test1-content">  
    <div class="accordion_child">
    	<div class="accordion_con">${person?.company?.name}<br />
${person?.company?.membershiplevel.memlevel}<br />
${person?.company?.address?.city} ${person?.company?.address?.state}, ${person?.company?.address?.country?.name}<br />
Broadcast: <a href="${request.contextPath}/broadcast/search?createdById=${person.id}">${companyBroadcastCount}</a> <br />
Inventory: <a href="${request.contextPath}/inventory/getInventoryC?cid=${person?.company?.id}" >${companyInventoryCount}</a> <br />
		</div>
    </div>
  </div>
  <!--End of each accordion item--> 

  <div id="test2-header" class="accordion_headings" >Connections</div>   
  <div id="test2-content">   	 
    <div class="accordion_child">
<g:each in="${connections}" status="i" var="con">
        <div class="accordion_con">${con?.contact?.firstname} ${con?.contact?.lastname}<br />
                                                                ${con?.contact?.company?.name} <br />
                                                            <span>${con?.contact?.company?.address?.city} ${con?.contact?.company?.address?.state}, ${con?.contact?.company?.address?.country?.name}</span>
		</div>
                                        </g:each>
    </div>
  </div>
 <!--End of each accordion item-->
</div>
	
	 <div class="clear"></div>
	 </div>
<!-- Left Menu End -->
		
		
			
	 
	 
	 <div class="right_side">
	  
	   <div class="list_menu">
      <ul>
        <li><a href="#">All Updates</a>
          <ul>
            <li><a href="#">Dummy Text</a></li>
            <li><a href="#">Dummy Text</a></li>
			<li><a href="#">Dummy Text</a></li>
			<li><a href="#">Dummy Text</a></li>
			<li><a href="#">Dummy Text</a></li>
			<li><a href="#">Dummy Text</a></li>
			<li><a href="#">Dummy Text</a></li>
          </ul>
        </li>      
      </ul>  	
	</div>
	    <h1>Sort By Recent Activity</h1>
	<g:each in="${feedList}" status="i" var="feed">	
		<g:if test="${feed.type == 'B'}" >
		<div class="profile">
		<div class="profile_left">
			 <div class="profile_text"> 
			     <div class="profile_n">Single Part Broadcast:  ${feed?.broadcast?.broadcasttype} </div>
			     <div class="respond"><a href="#">Respond</a></div>
			 </div>
			 <div class="clear"></div>
		</div>
		<div class="profile_right">
		  <ul>
		     <li>Part : ${feed?.broadcast?.unit?.part}</li>
		     <li>Manufacturer : ${feed?.broadcast?.unit?.manufacturer?.mfgname}</li>
		     <li>Condition : ${feed?.broadcast?.unit?.condition?.name}</li>
		     <li>Price : $${feed?.broadcast?.unit?.price}</li>
		     <li>Quantity : ${feed?.broadcast?.unit?.qty}</li>
		  </ul>
			
			<div class="date_p"><%=PZUtil.format(feed?.broadcast?.dateCreated)%></div>
			<div class="clear"></div>
			<div class="profile_rightcon">
				<%=feed.broadcast.description%>
			</div>
			<div class="more_p"><a href="#">More</a></div>
			</div>
		<div  class="clear"></div>
		</div>
		</g:if>
		<g:if test="${feed.type == 'M'}" >
		<div class="profile">
		<div class="profile_left">
			 <div class="profile_text"> 
			     <div class="profile_n">WTB (Multi Part)</div>
				 <div class="respond"><a href="#">Respond</a></div>
			 </div>
			 <div class="clear"></div>
		</div>
		<div class="profile_right">
		  <ul>
		    <li>Part : IBM888</li>
		     <li>Manufacturer : IBM</li>
			 <li>Condition : Refurnished</li>
			 <li>Price : $ 1000</li>
			 <li>Quantity : 200</li>
			
		    </ul>
			
			<div class="date_p">Mar 12</div>
			
			<div class="clear"></div>
			<div class="profile_rightcon">
			     Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets
			</div>
			<div class="more_p"><a href="#">More</a></div>
			</div>
		<div  class="clear"></div>
		</div>
		</g:if>

		<div class="profile">
		<div class="profile_left">
			 <div class="profile_text"> 
			     <div class="profile_n">WTB (Multi Part)</div>
				 <div class="respond"><a href="#">Respond</a></div>
			 </div>
			 <div class="clear"></div>
		</div>
		<div class="profile_right">
		  <ul>
		    <li>Part : IBM888</li>
		     <li>Manufacturer : IBM</li>
			 <li>Condition : Refurnished</li>
			 <li>Price : $ 1000</li>
			 <li>Quantity : 200</li>
			
		    </ul>
			
			<div class="date_p">Mar 12</div>
			
			<div class="clear"></div>
			<div class="profile_rightcon">
			     Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets
			</div>
			<div class="more_p"><a href="#">More</a></div>
			</div>
		<div  class="clear"></div>
		</div>
		</g:each>
		
	
	      <div class="pagination_n">
		       <div class="pagination_pre"><a href="#"></a></div>			    
			     <div class="pagination_num">
                  <ul>
						<li><a href="#" class="active">1 </a> </li>
						<li><a href="#" >2</a></li>						
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>		
						<li><a href="#">5</a></li>
						 
						
		         </ul>             
			     </div>
			   <div class="pagination_next"><a href="#"></a></div>
		  </div>
	 
	
	<div class="clear"></div>
		</div>
  <div  class="clear"></div>
</div>
</div>
</div>
