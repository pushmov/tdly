<%@page import="test2.Role" %>	
<%
response.setHeader( "Pragma", "no-cache" );
response.setHeader( "Cache-Control", "no-cache" );
response.setDateHeader( "Expires", 0 );
%>

<div class="modal fade broadcasts-popup" id="modal_edit_employee" tabindex="-1" role="dialog" aria-labelledby="modal_edit_employeeLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_edit_employeeLabel">view/Edit Employee</h4>
            </div>
            <!--modal-header-->
            <div class="modal-body">
                <div class="form-content">
					<g:hasErrors bean="${person}">
					<div class="errors">
					<g:renderErrors bean="${person}" as="list" />
					</div>
					</g:hasErrors>
					<g:hasErrors bean="${person?.address}">
					<div class="errors">
					<g:renderErrors bean="${person?.address}" as="list" />
					</div>
					</g:hasErrors>
					
					<g:form action="saveemployee" name="person" id="person" >
					<div id="response">
					<g:render template="employeedetails" bean="${person}" />
					</div>
					</g:form>
                </div>
            </div>
        </div>
        <!--modal-content-->
    </div>
    
</div>