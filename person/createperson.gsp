

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="start" />
        <title>Create Person</title>         
    </head>

				<div id="main_info">
					<div id="main_info_bottom">

				            <g:form action="saveperson" method="post" class="search_form" >



							<!--[if !IE]>start fieldset<![endif]-->
							<fieldset>
								<!--[if !IE]>start forms<![endif]-->
								<div class="forms">
								<h4>Personal Details</h4>

								<!--[if !IE]>start row<![endif]-->
								<div class="row">
								</div>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${person}">
            <div class="errors">
                <g:renderErrors bean="${person}" as="list" />
            </div>
            </g:hasErrors>
                        
                        
                            <div class="row">
                                <td valign="top" class="name">
                                    <label for="email">Email:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:person,field:'email','errors')}">
                                    <input type="text" id="email" name="email" value="${fieldValue(bean:person,field:'email')}"/>
                                </td>
                            </div> 

                            <div class="row">
                                <td valign="top" class="name">
                                    <label for="passwd">Password:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:person,field:'passwd','errors')}">
                                    <input type="text" id="passwd" name="passwd" value="${fieldValue(bean:person,field:'passwd')}"/>
                                </td>
                            </div> 
                        
                            <div class="row">
                                <td valign="top" class="name">
                                    <label for="firstname">Firstname:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:person,field:'firstname','errors')}">
                                    <input type="text" id="firstname" name="firstname" value="${fieldValue(bean:person,field:'firstname')}"/>
                                </td>
                            </div> 
                        
                        
                            <div class="row">
                                <td valign="top" class="name">
                                    <label for="lastname">Lastname:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:person,field:'lastname','errors')}">
                                    <input type="text" id="lastname" name="lastname" value="${fieldValue(bean:person,field:'lastname')}"/>
                                </td>
                            </div> 
                        
								<div class="row">
									<div class="inputs">
										<span class="button blue_button search_button"><span><span><em>Save</em></span></span><input name="" type="submit" /></span>
									</div>
								</div>


								<!--[if !IE]>start forms<![endif]-->
								</div>
							</fieldset>

                        
            </g:form>
        </div>
	</div>
</html>
