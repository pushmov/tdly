<div id="dialog-resetPassword" >
		<h4>Reset Password</h4>
<g:form action="resetPassword" name="resetPasswordForm" method="post"   >
		<input type="hidden" class="text"  id="person.id" name="id" value="${fieldValue(bean:person,field:'id')}" />
		<input type="hidden" name="returnTemplate" value="" id="returnTemplate" />
<g:hasErrors bean="${person}">
            <div class="errors">
                <g:renderErrors bean="${person}" as="list" />
            </div>
            </g:hasErrors>
		<div class="row">
			<label>New Password:</label>
			<div class="inputs">
				<span class="input_wrapper">
					<input type="text" class="text inptinpt"  id="passwd" name="passwd" value=""/>
				</span>
			  <g:renderErrorsCap bean="${person}" as="list" field="passwd"  />
			</div>
		</div>
		<div class="row">
			<label>Re-Enter New Password:</label>
			<div class="inputs">
				<span class="input_wrapper">
					<input type="text" class="text inptinpt"  id="passwd2" name="passwd2" value=""/>
				</span>
				<g:renderErrorsCap bean="${person}" as="list" field="passwd2"  />
			</div>
		</div>
</g:form>
</div>

