<r:require module="broadcast" />
<div id="dialog-broadcast" title="Create Broadcast"> 
<g:render template="/broadcastsingle" />
</div>
<div id="dialog-broadcast-multiple" title="Create Broadcast"> 
<g:render template="/broadcastmultiple" />
</div>
<div id="dialog-bmessage" title="Broadcast"> 
	<p> 
		<span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span> 
		 Broadcast Created.
	</p> 
</div> 
