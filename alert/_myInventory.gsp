<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm z");
//		sdf.setTimeZone(TimeZone.getTimeZone(session.person.address.timezone));
	        
		
%>

<div class="table-responsive">
	<table class="table table-bordered gray-header responsive">
		<thead>
			<tr>
				<util:remoteSortableColumn  action="myInventory"  property="bc.dateCreated" title="Date"  update="tabs-2"   />
				<util:remoteSortableColumn  action="myInventory" property="bc.broadcasttype" title="Broadcast Type"  update="tabs-2" />
				<util:remoteSortableColumn  action="myInventory"  property="bc.unit.part" title="Part"  update="tabs-2"  />
				<util:remoteSortableColumn  action="myInventory"  property="bc.unit.manufacturer" title="Manufacturer"  update="tabs-2"  />
				<util:remoteSortableColumn  action="myInventory"  property="bc.unit.condition" title="Condition"  update="tabs-2"  />
				<util:remoteSortableColumn  action="myInventory"  property="bc.unit.price" title="Price"  update="tabs-2"  />
				<util:remoteSortableColumn  action="myInventory"  property="bc.unit.qty" title="Quantity"  update="tabs-2"  />
				<th>Description</th>
				<util:remoteSortableColumn  action="myInventory"   property="bc.company" title="Company"  update="tabs-2"   />
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			<g:render template="/alert/alertRow" />
		</tbody>
	</table>
</div>
	


<script>
	$(document).ready(function(){
		
		$('.pagination').addClass('pagination_n');
		
	});
</script>
			
<style>
	.pagination a.step{
		background: rgba(0, 0, 0, 0) none repeat scroll 0 0 !important;
		color: #333333 !important;
		display: block !important;
		font-size: 15px !important;
		padding: 6px 12px !important;
		text-align: center !important;
		text-decoration: none !important;
	}
	
	.pagination a.step:hover{
		background: #f0f0f0 !important;
		border-radius: 5px !important;
	}
</style>