<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
//		sdf.setTimeZone(TimeZone.getTimeZone(session.person.address.timezone));
	        
		
%>

<g:each in="${broadcastList}" status="i" var="broadcast">
	<tr class="${(i % 2) == 0 ? 'first' : 'second'}">
		<td>${broadcast.broadcasttype}</td>
		<td>${fieldValue(bean:broadcast.unit, field:'part')}</td>
		<td>${fieldValue(bean:broadcast.unit, field:'manufacturer.mfgname')}</td>
		<td>${fieldValue(bean:broadcast.unit, field:'condition.name')}</td>
		<td>${fieldValue(bean:broadcast.unit, field:'price')}</td>
		<td>${fieldValue(bean:broadcast.unit, field:'qty')}</td>
		<td>
			<div class="showmore">
				<div class="moreblock htmldscr">
					${broadcast.description}
				</div>
			</div>
		</td>
		<td>
			<a href="../company/view?id=${broadcast?.company?.id}">${broadcast.company.name}</a>
		</td>
		<td>${sdf.format(broadcast.dateCreated)}</td>
		<td>
			<g:if test="${broadcast.company.id != session.company.id}">
				<a href="javascript:void(0)" id="respond" onClick="javascript:showResponse('${broadcast.id}','${broadcast.createdby.id}'  );" >
					<i class="fa fa-reply fa-lg"></i>Respond
				</a>
			</g:if>
		</td>
	</tr>
</g:each>
<script>
		$(document).ready(function(){
			
			$('.htmldscr').each(function(){
			
				var html = $(this).text();
				$(this).html(html);
			});
			
		});
</script>