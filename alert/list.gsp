<%@page import="java.text.*" %>
<%@page import="java.util.*" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="pz" />
        <title>Alert List</title>
				<link href="${createLinkTo(dir:'assets',file:'partszap/css/app.css')}" rel="stylesheet">

    </head>

    <body> 
        <div class="body">

<script> 

$(document).ready(function()
        {

	$( "#tabs" ).tabs();

});


</script>
<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm z");
//		sdf.setTimeZone(TimeZone.getTimeZone(session.person.address.timezone));
	        
		
%>

<main class="content" role="main">
	<div class="main-content">
		
		<div id="tabs">
			<div role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" aria-controls="tabs-1" aria-labelledby="ui-id-1" aria-selected="true">
						<a href="../alert/customAlert" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1">Custom Alerts</a>
					</li>
					<li role="presentation" tabindex="-1" aria-controls="tabs-2" aria-labelledby="ui-id-2" aria-selected="false">
						<a href="../alert/myInventory" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-2">My Inventory Alerts</a>
					</li> 
					<li role="presentation" tabindex="-1" aria-controls="tabs-3" aria-labelledby="ui-id-3" aria-selected="false">
						<a href="../alert/myWatchlist" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-2">My Watchlist Alerts</a>
					</li> 
					<li role="presentation" tabindex="-1" aria-controls="tabs-4" aria-labelledby="ui-id-4" aria-selected="false">
						<a href="../alert/myVendor" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-2">My Vendor Alerts</a>
					</li> 
				</ul>
			</div>
				<!--eachtab start -->
			<div class="tab-content">
				<div class="tab-pane" id="tabs-1" aria-labelledby="ui-id-1" role="tabpanel" style="display: block;" aria-expanded="true" aria-hidden="false">
				</div>
				<div class="tab-pane" id="tabs-2" aria-labelledby="ui-id-2" role="tabpanel" style="display: none;" aria-expanded="false" aria-hidden="true">
				</div>
				<div class="tab-pane" id="tabs-3" aria-labelledby="ui-id-2" role="tabpanel" style="display: none;" aria-expanded="false" aria-hidden="true">
				</div>
				<div class="tab-pane" id="tabs-4" aria-labelledby="ui-id-2" role="tabpanel" style="display: none;" aria-expanded="false" aria-hidden="true">
				</div>
			</div>
		</div>
	</div>
</main>

<g:render template="/dialog-company" />
<g:render template="/dialog-broadcast-response" />
<g:render template="/dialog-fltr" />

            <div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all" style="display:none">
                <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
                <li class="ui-state-default ui-corner-top ui-tabs-active ui-state-active" role="tab" tabindex="0" aria-controls="tabs-1" aria-labelledby="ui-id-1" aria-selected="true">
									<a href="../alert/customAlert" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1">Custom Alerts</a></li>
                <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tabs-2" aria-labelledby="ui-id-2" aria-selected="false"><a href="../alert/myInventory" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-2">My Inventory Alerts</a></li>
                <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tabs-3" aria-labelledby="ui-id-3" aria-selected="false"><a href="../alert/myWatchlist" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-3">My Watchlist Alerts</a></li>
                <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tabs-4" aria-labelledby="ui-id-4" aria-selected="false"><a href="../alert/myVendor" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-4">My Vendor Alerts</a></li>
                </ul>
                <!--eachtab start -->
                <div class="eachtab ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-1" aria-labelledby="ui-id-1" role="tabpanel" style="display: block;" aria-expanded="true" aria-hidden="false">
                </div>
                <div class="eachtab ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-2" aria-labelledby="ui-id-2" role="tabpanel" style="display: none;" aria-expanded="false" aria-hidden="true">
                </div>
                <div class="eachtab ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-3" aria-labelledby="ui-id-3" role="tabpanel" style="display: none;" aria-expanded="false" aria-hidden="true">
                </div>
                <div class="eachtab ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-4" aria-labelledby="ui-id-4" role="tabpanel" style="display: none;" aria-expanded="false" aria-hidden="true">
                </div>
           </div>



 
				
				
				
				
        </div>
    </body>
</html>
