<%@page import="java.text.*" %>
<%@page import="java.util.*" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="pz" />
        <title>Alert List</title>
	<g:javascript library="jquery" plugin="jquery" />
	<jqui:resources/>
        <jqDT:resources/>               	                

	<jqval:resources />
        <jqvalui:resources />


    </head>
    <body> 
        <div class="body">

<script> 

$(document).ready(function()
        {

	$( "#tabs" ).tabs();
});

/*
var myWin;
var theform;
var bid;

Ext.onReady(function(){
Ext.BLANK_IMAGE_URL = 'images/s.gif'; 
Ext.QuickTips.init();  

}); 

var genres = new Ext.data.SimpleStore({ 
fields: ['broadcasttype', 'displaytype'], 
data : [['WTB','WTB'],['WTS','WTS']]
});


var proxy = new Ext.data.ScriptTagProxy({
        url: '/test2/manufacturer/ajaxlist'
    });

/*
    var store = new Ext.data.Store({
        proxy: proxy,
        reader: new Ext.data.JsonReader({
            id: 'id',
            root: 'rows'
        }, [{
            name: 'id'
        }, {
            name: 'mfgname'
        }])
    });


var store = new Ext.data.Store({ 
reader: new Ext.data.JsonReader({ 
fields: ['id', 'mfgname'], 
root: 'rows' 
}), 
proxy:  proxy, 
autoLoad: true
}); 
*/

  var store = new Ext.data.JsonStore({
 id:'id'
 ,root:'rows'
 ,fields:[
 {name:'id' }
 ,{name:'mfgname' }
 ]
 ,url:'/test2/manufacturer/ajaxlist'
 })


  var partstore = new Ext.data.JsonStore({
 id:'part'
 ,root:'rows'
 ,fields:[
 {name:'part' }
 ,{name:'part' }
 ]
 ,url:'/test2/inventory/listparts'

 })

 var tradeunit = new Ext.data.JsonStore({
 id:'condition'
 ,root:'rows'
 ,fields:[
 {name:'condition' }
 ,{name:'price' }
 ,{name:'quantity'}
 ]
 ,url:'/test2/inventory/listunit'

 })

var conditionstore = new Ext.data.SimpleStore({ 
fields: ['condition', 'conddisplay'], 
data : [['New','New'],['Refurbished','Refurbished'],['Used','Used']]
});


function createBroadcast() {
	theform=new Ext.FormPanel({

	labelAlign: 'top',

	frame:true,

	title: 'Send Broadcast',

	bodyStyle:'padding:5px 5px 0',

	width: 600,

	items: [

	{

	xtype:'combo',

	fieldLabel: 'Broadcast Type',

	name: 'broadcasttype',

mode: 'local', 
store: genres, 
	displayField:'displaytype', 

	anchor:'95%'

	}
	,{

	xtype:'combo',
      id: 'mfgcombo',
	name: 'mfgcombo',

	fieldLabel: 'Manufacturer',

	store : store,
	displayField: 'mfgname',
            valueField: 'id', 
            typeAhead: true,
            forceSelection: true,
            mode: 'remote',
	      minChars : 0,
            triggerAction: 'all',
            selectOnFocus: true,
		enableKeyEvents:true,
            editable: true,
	anchor:'95%',
    listeners: {
        select: {
            fn:function(combo, value) {
 
 
             	var modelDest = Ext.getCmp('part');
                //set and disable cities
                modelDest.setDisabled(true);
		    modelDest.setValue('');

                modelDest.store.removeAll();

                //reload region store and enable region 
                modelDest.store.reload({
                    params: { 'mfgname': combo.getValue() }
                });
		
                modelDest.setDisabled(false);

		}
        }
	}

	}
	,{

      id : 'part',
	xtype:'combo',
	store : partstore,
	displayField: 'part',
            valueField: 'part', 
            typeAhead: true,
            forceSelection: true,
            mode: 'local',
	      minChars : 0,
            triggerAction: 'all',
            selectOnFocus: true,
		enableKeyEvents:true,
            editable: true,

	fieldLabel: 'Part',

	name: 'part',

	anchor:'95%',
    listeners: {
        select: {
            fn:function(combo, value) {
 
                tradeunit.removeAll();
                tradeunit.reload({
                    params: { 'part': combo.getValue() },
			  callback : function(success , r){
				if ( this.getTotalCount() == 1 ) {		
		    			Ext.getCmp('condition').setValue(this.getAt(0).get("condition"));
		    			Ext.getCmp('price').setValue(this.getAt(0).get("price"));
		    			Ext.getCmp('quantity').setValue(this.getAt(0).get("quantity"));
						
				}
			  }
                });
		}
        }
	}


	}
	,{

	xtype:'combo',
      id: 'condition',
	name: 'condition',
	fieldLabel: 'Condition',
	store : conditionstore,
	displayField: 'conddisplay',
            valueField: 'condition', 
            typeAhead: true,
            forceSelection: true,
            mode: 'local',
	      minChars : 0,
            triggerAction: 'all',
            selectOnFocus: true,
		enableKeyEvents:true,
            editable: true,
	anchor:'95%'

	}
	,{

	id : 'price',

	xtype:'textfield',

	fieldLabel: 'Price',

	name: 'price',

	anchor:'95%'

	}
	,{

	id : 'quantity',
	xtype:'textfield',

	fieldLabel: 'Quantity',

	name: 'qty',

	anchor:'95%'

	}
	,{

	xtype:'textarea',

	fieldLabel: 'Description',

	name: 'description',

	anchor:'95%'

	}
	,{

	xtype:'checkbox',

	fieldLabel: 'Send to MyVendors Only',

	name: 'myvendor',

	anchor:'95%'

	}

      
	]
,


	buttons: [{ 
text: 'Save', 
handler: function(){ 
theform.getForm().submit({ 
url:'/test2/broadcast/save', 
			waitMsg:'Saving Data...',

success: function(f,a){ 
Ext.Msg.alert('Success', a.result.msg); 
myWin.close();
}, 
failure: function(f,a){ 
Ext.Msg.alert('Warning', 'Error'); 
} 
}); 
}
}, { 
text: 'Reset', 
handler: function(){ 
theform.getForm().reset(); 
}
}]

	});




myWin = new Ext.Window({ // 2 
id : 'myWin', 
modal : true , 
autoScroll : true ,
height : 500, 
width : 700, 
items : [ 
theform 
] 
}); 
myWin.show();
}


function showResponse(prm) {
      bid = prm;
	theform=new Ext.FormPanel({

	labelAlign: 'top',

	frame:true,

	title: 'Multi Column, Nested Layouts and HTML editor',

	bodyStyle:'padding:5px 5px 0',

	width: 600,

	items: [{

	layout:'column',

	items:[{

	columnWidth:.5,

	layout: 'form',

	items: [{

	xtype:'textfield',

	fieldLabel: 'First Name',

	name: 'first',

	anchor:'95%'

	}]

	},{

	columnWidth:.5,

	layout: 'form',

	items: [{

	xtype:'textfield',

	fieldLabel: 'Last Name',

	name: 'last',

	anchor:'95%'

	},{

	xtype:'textfield',

	fieldLabel: 'Email',

	name: 'email',

	vtype:'email',

	anchor:'95%'

	}]

	}]

	},{

	xtype:'htmleditor',

	id:'responsetext',

	name:'responsetext',

	fieldLabel:'Comments',

	height:200,

	anchor:'98%'

	}],


	buttons: [{ 
text: 'Save', 
handler: function(){ 
theform.getForm().submit({ 
url:'/test2/broadcastResponse/save?bid=' + bid, 
			waitMsg:'Saving Data...',

success: function(f,a){ 
Ext.Msg.alert('Success', a.result.msg); 
myWin.close();
}, 
failure: function(f,a){ 
Ext.Msg.alert('Warning', 'Error'); 
} 
}); 
}
}, { 
text: 'Reset', 
handler: function(){ 
theform.getForm().reset(); 
}
}]
	});




myWin = new Ext.Window({ // 2 
id : 'myWin', 
modal : true,
height : 500, 
width : 700, 
items : [ 
theform 
] 
}); 
myWin.show();
}

function getCompany(cid) {

Ext.Ajax.request({
	url : '/test2/company/showajax' , 
	params : { id : cid },
	method: 'POST',
	success: function ( result, request ) { 
		showCompany(result.responseText); 
	},
	failure: function ( result, request) { 
		Ext.MessageBox.alert('Failed', result.responseText); 
	} 
});


}

function doJSON(stringData) {
		try {
			var jsonData = Ext.util.JSON.decode(stringData);
			return jsonData;
		}
		catch (err) {
			Ext.MessageBox.alert('ERROR', 'Could not decode ' + stringData);
		}
	}

 function showCompany(company) {

	var comp = doJSON(company);
	myWin = new Ext.Window({ // 2 
	id : 'myWin', 
      modal : true,
	height : 500, 
	width : 700, 
	title: 'Company Details',
      layout : 'fit',
	items : [ 
		{

			xtype:'panel',

			id:'responsetext',

			name:'responsetext',

			fieldLabel:'Comments',

			height:200,

			anchor:'98%',
                 html : '<b>Name : </b>' + comp.company.name + '<br><br>' + 'Membership Level ' + comp.company.membershiplevel


		}],

		buttons: [{ 
			text: 'Close', 
			handler: function(){ 
				myWin.close();
			} 
		}]
       
	}); 	
	myWin.show();


}

</script>
<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm z");
		sdf.setTimeZone(TimeZone.getTimeZone(session.person.address.timezone));
	        
		
%>

<div id="tabs"> 
	<ul> 
		<li><a href="#tabs-1">All</a></li> 
		<li><a href="#tabs-2">From My Vendors</a></li> 
		<li><a href="#tabs-2">From My Countries</a></li> 
	</ul> 
	<div id="tabs-1"> 

				
				<!--[if !IE]>start main info<![endif]-->
				<div id="main_info">
					<div id="main_info_bottom">
<!--						 
						<div class="title_wrapper">
							
							<ul id="search_tabs" class="search_tabs" >
								<li><g:link   id="${(params.action == 'list')?'selected_search_tab':' '}"  class="${(params.action == 'list')?'selected':' '}" url="[action:'list']"   ><span><span>All</span></span></g:link></li>
								<li><g:link  id="${(params.action == 'wtb')?'selected_search_tab':' '}" class="${(params.action == 'wtb')?'selected':' '}" url="[action:'wtb']"   ><span><span>WTB on Inventory</span></span></g:link></li>
								<li><g:link  id="${(params.action == 'wts')?'selected_search_tab':' '}" class="${(params.action == 'wts')?'selected':' '}" url="[action:'wts']"   ><span><span>WTS on Inventory</span></span></g:link></li>
								<li><g:link  id="${(params.action == 'wtbWatchlist')?'selected_search_tab':' '}" class="${(params.action == 'wtbWatchlist')?'selected':' '}" url="[action:'wtbWatchlist']"   ><span><span>WTB on Watchlist</span></span></g:link></li>
								<li><g:link  id="${(params.action == 'wtsWatchlist')?'selected_search_tab':' '}"  class="${(params.action == 'wtsWatchlist')?'selected':' '}" url="[action:'wtsWatchlist']"   ><span><span>WTS on Watchlist</span></span></g:link></li>
							</ul>
						</div>
 -->
				<!--[if !IE]>start sec info<![endif]-->
				<div id="sec_info">
					
					<div class="row">
						<br><br>
					</div>
					
					
					<!--[if !IE]>start product list<![endif]-->
					<div id="product_list">
						
						
						<!--[if !IE]>start table_wrapper<![endif]-->
						<div class="table_wrapper">
							<div class="table_wrapper_inner">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>
                   	        <g:sortableColumn property="alerttype" title="Alert Type" />
                   	        <g:sortableColumn property="dateCreated" title="Date" />
                   	        <g:sortableColumn property="broadcast.broadcasttype" title="Broadcast Type" />
                   	        <g:sortableColumn property="broadcast.unit.part" title="Part" />
                   	        <g:sortableColumn property="broadcast.unit.manufacturer" title="Manufacturer" />
                   	        <g:sortableColumn property="broadcast.unit.condition" title="Condition" />
                   	        <g:sortableColumn property="broadcast.unit.price" title="Price" />
                   	        <g:sortableColumn property="broadcast.unit.qty" title="Quantity" />
                   	        <g:sortableColumn property="broadcast.description" title="Description" />
                   	        <g:sortableColumn property="company" title="Company" />
								<th>Actions</th>

	
								</tr>
								
                    <g:each in="${alertInstanceList}" status="i" var="alert">
                        <tr class="${(i % 2) == 0 ? 'first' : 'second'}">
                        
                        
                            <td>${fieldValue(bean:alert, field:'alerttype')}</td>
                            <td>${sdf.format(alert.dateCreated)}</td>
                            <td>${alert.broadcast.broadcasttype}</td>
                            <td>${fieldValue(bean:alert.broadcast.unit, field:'part')}</td>
                            <td>${fieldValue(bean:alert.broadcast.unit, field:'manufacturer.mfgname')}</td>
                            <td>${fieldValue(bean:alert.broadcast.unit, field:'condition')}</td>
                            <td>${fieldValue(bean:alert.broadcast.unit, field:'price')}</td>
                            <td>${fieldValue(bean:alert.broadcast.unit, field:'qty')}</td>
                            <td>${fieldValue(bean:alert.broadcast, field:'description')}</td>
				    <td><a href="javascript:void(0)" id="company" onClick="javascript:getCompany('${session.company.id}');" >${alert.broadcast.company.name}</a></td>

				<g:if test="${alert.broadcast.company.id != session.company.id}">

					<td>
						<a href="javascript:void(0)" id="respond" onClick="javascript:showResponse('${fieldValue(bean:alert.broadcast, field:'id')}');" >Respond</a>
					</td>
				</g:if>

				<td>
						<div class="actions_menu">
							<ul>
												<li><g:remoteLink class="delete" controller="alert" action="delete" id="${alert?.id}" before="return confirm('Are you sure?');" params="[targetUri: (request.forwardURI - request.contextPath)]" >Delete</g:remoteLink></li>
											</ul>
										</div>
									</td>

                        </tr>
                    </g:each>

								
								
							</table>
							</div>
						</div>
						<!--[if !IE]>end table_wrapper<![endif]-->
						
						
					</div>
					<!--[if !IE]>end product list<![endif]-->
					
					
					<!--[if !IE]>start pagination<![endif]-->
					<div class="pagination">
						<span class="page_no">Page 1 of 217</span>
						
						<ul class="pag_list">
                	<g:paginate total="${alertInstanceTotal}" />
	
						</ul>
						
					</div>
					<!--[if !IE]>end pagination<![endif]-->
					
					
					
					
					
					
				</div>
				<!--[if !IE]>end sec info<![endif]-->
						
						
					
					
					</div>
				</div>
				<!--[if !IE]>end main info<![endif]-->
</div> <!--  end tabs-1 -->
<div id="tabs-2">
				<!--[if !IE]>start main info<![endif]-->
				<div id="main_info">
					<div id="main_info_bottom">
<!--						 
						<div class="title_wrapper">
							
							<ul id="search_tabs" class="search_tabs" >
								<li><g:link   id="${(params.action == 'list')?'selected_search_tab':' '}"  class="${(params.action == 'list')?'selected':' '}" url="[action:'list']"   ><span><span>All</span></span></g:link></li>
								<li><g:link  id="${(params.action == 'wtb')?'selected_search_tab':' '}" class="${(params.action == 'wtb')?'selected':' '}" url="[action:'wtb']"   ><span><span>WTB on Inventory</span></span></g:link></li>
								<li><g:link  id="${(params.action == 'wts')?'selected_search_tab':' '}" class="${(params.action == 'wts')?'selected':' '}" url="[action:'wts']"   ><span><span>WTS on Inventory</span></span></g:link></li>
								<li><g:link  id="${(params.action == 'wtbWatchlist')?'selected_search_tab':' '}" class="${(params.action == 'wtbWatchlist')?'selected':' '}" url="[action:'wtbWatchlist']"   ><span><span>WTB on Watchlist</span></span></g:link></li>
								<li><g:link  id="${(params.action == 'wtsWatchlist')?'selected_search_tab':' '}"  class="${(params.action == 'wtsWatchlist')?'selected':' '}" url="[action:'wtsWatchlist']"   ><span><span>WTS on Watchlist</span></span></g:link></li>
							</ul>
						</div>
 -->
				<!--[if !IE]>start sec info<![endif]-->
				<div id="sec_info">
					
					<div class="row">
						<br><br>
					</div>
					
					
					<!--[if !IE]>start product list<![endif]-->
					<div id="product_list">
						
						
						<!--[if !IE]>start table_wrapper<![endif]-->
						<div class="table_wrapper">
							<div class="table_wrapper_inner">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>
                   	        <g:sortableColumn property="alerttype" title="Alert Type" />
                   	        <g:sortableColumn property="dateCreated" title="Date" />
                   	        <g:sortableColumn property="broadcast.broadcasttype" title="Broadcast Type" />
                   	        <g:sortableColumn property="broadcast.unit.part" title="Part" />
                   	        <g:sortableColumn property="broadcast.unit.manufacturer" title="Manufacturer" />
                   	        <g:sortableColumn property="broadcast.unit.condition" title="Condition" />
                   	        <g:sortableColumn property="broadcast.unit.price" title="Price" />
                   	        <g:sortableColumn property="broadcast.unit.qty" title="Quantity" />
                   	        <g:sortableColumn property="broadcast.description" title="Description" />
                   	        <g:sortableColumn property="company" title="Company" />
								<th>Actions</th>

	
								</tr>
								
                    <g:each in="${alertInstanceList}" status="i" var="alert">
                        <tr class="${(i % 2) == 0 ? 'first' : 'second'}">
                        
                        
                            <td>${fieldValue(bean:alert, field:'alerttype')}</td>
                            <td>${sdf.format(alert.dateCreated)}</td>
                            <td>${alert.broadcast.broadcasttype}</td>
                            <td>${fieldValue(bean:alert.broadcast.unit, field:'part')}</td>
                            <td>${fieldValue(bean:alert.broadcast.unit, field:'manufacturer.mfgname')}</td>
                            <td>${fieldValue(bean:alert.broadcast.unit, field:'condition')}</td>
                            <td>${fieldValue(bean:alert.broadcast.unit, field:'price')}</td>
                            <td>${fieldValue(bean:alert.broadcast.unit, field:'qty')}</td>
                            <td>${fieldValue(bean:alert.broadcast, field:'description')}</td>
				    <td><a href="javascript:void(0)" id="company" onClick="javascript:getCompany('${session.company.id}');" >${alert.broadcast.company.name}</a></td>

				<g:if test="${alert.broadcast.company.id != session.company.id}">

					<td>
						<a href="javascript:void(0)" id="respond" onClick="javascript:showResponse('${fieldValue(bean:alert.broadcast, field:'id')}');" >Respond</a>
					</td>
				</g:if>

				<td>
						<div class="actions_menu">
							<ul>
												<li><g:remoteLink class="delete" controller="alert" action="delete" id="${alert?.id}" before="return confirm('Are you sure?');" params="[targetUri: (request.forwardURI - request.contextPath)]" >Delete</g:remoteLink></li>
											</ul>
										</div>
									</td>

                        </tr>
                    </g:each>

								
								
							</table>
							</div>
						</div>
						<!--[if !IE]>end table_wrapper<![endif]-->
						
						
					</div>
					<!--[if !IE]>end product list<![endif]-->
					
					
					<!--[if !IE]>start pagination<![endif]-->
					<div class="pagination">
						<span class="page_no">Page 1 of 217</span>
						
						<ul class="pag_list">
                	<g:paginate total="${alertInstanceTotal}" />
	
						</ul>
						
					</div>
					<!--[if !IE]>end pagination<![endif]-->
					
					
					
					
					
					
				</div>
				<!--[if !IE]>end sec info<![endif]-->
						
						
					
					
					</div>
				</div>
				<!--[if !IE]>end main info<![endif]-->

</div>
<div id="tabs-3">
				<!--[if !IE]>start main info<![endif]-->
				<div id="main_info">
					<div id="main_info_bottom">
<!--						 
						<div class="title_wrapper">
							
							<ul id="search_tabs" class="search_tabs" >
								<li><g:link   id="${(params.action == 'list')?'selected_search_tab':' '}"  class="${(params.action == 'list')?'selected':' '}" url="[action:'list']"   ><span><span>All</span></span></g:link></li>
								<li><g:link  id="${(params.action == 'wtb')?'selected_search_tab':' '}" class="${(params.action == 'wtb')?'selected':' '}" url="[action:'wtb']"   ><span><span>WTB on Inventory</span></span></g:link></li>
								<li><g:link  id="${(params.action == 'wts')?'selected_search_tab':' '}" class="${(params.action == 'wts')?'selected':' '}" url="[action:'wts']"   ><span><span>WTS on Inventory</span></span></g:link></li>
								<li><g:link  id="${(params.action == 'wtbWatchlist')?'selected_search_tab':' '}" class="${(params.action == 'wtbWatchlist')?'selected':' '}" url="[action:'wtbWatchlist']"   ><span><span>WTB on Watchlist</span></span></g:link></li>
								<li><g:link  id="${(params.action == 'wtsWatchlist')?'selected_search_tab':' '}"  class="${(params.action == 'wtsWatchlist')?'selected':' '}" url="[action:'wtsWatchlist']"   ><span><span>WTS on Watchlist</span></span></g:link></li>
							</ul>
						</div>
 -->
				<!--[if !IE]>start sec info<![endif]-->
				<div id="sec_info">
					
					<div class="row">
						<br><br>
					</div>
					
					
					<!--[if !IE]>start product list<![endif]-->
					<div id="product_list">
						
						
						<!--[if !IE]>start table_wrapper<![endif]-->
						<div class="table_wrapper">
							<div class="table_wrapper_inner">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>
                   	        <g:sortableColumn property="alerttype" title="Alert Type" />
                   	        <g:sortableColumn property="dateCreated" title="Date" />
                   	        <g:sortableColumn property="broadcast.broadcasttype" title="Broadcast Type" />
                   	        <g:sortableColumn property="broadcast.unit.part" title="Part" />
                   	        <g:sortableColumn property="broadcast.unit.manufacturer" title="Manufacturer" />
                   	        <g:sortableColumn property="broadcast.unit.condition" title="Condition" />
                   	        <g:sortableColumn property="broadcast.unit.price" title="Price" />
                   	        <g:sortableColumn property="broadcast.unit.qty" title="Quantity" />
                   	        <g:sortableColumn property="broadcast.description" title="Description" />
                   	        <g:sortableColumn property="company" title="Company" />
								<th>Actions</th>

	
								</tr>
								
                    <g:each in="${alertInstanceList}" status="i" var="alert">
                        <tr class="${(i % 2) == 0 ? 'first' : 'second'}">
                        
                        
                            <td>${fieldValue(bean:alert, field:'alerttype')}</td>
                            <td>${sdf.format(alert.dateCreated)}</td>
                            <td>${alert.broadcast.broadcasttype}</td>
                            <td>${fieldValue(bean:alert.broadcast.unit, field:'part')}</td>
                            <td>${fieldValue(bean:alert.broadcast.unit, field:'manufacturer.mfgname')}</td>
                            <td>${fieldValue(bean:alert.broadcast.unit, field:'condition')}</td>
                            <td>${fieldValue(bean:alert.broadcast.unit, field:'price')}</td>
                            <td>${fieldValue(bean:alert.broadcast.unit, field:'qty')}</td>
                            <td>${fieldValue(bean:alert.broadcast, field:'description')}</td>
				    <td><a href="javascript:void(0)" id="company" onClick="javascript:getCompany('${session.company.id}');" >${alert.broadcast.company.name}</a></td>

				<g:if test="${alert.broadcast.company.id != session.company.id}">

					<td>
						<a href="javascript:void(0)" id="respond" onClick="javascript:showResponse('${fieldValue(bean:alert.broadcast, field:'id')}');" >Respond</a>
					</td>
				</g:if>

				<td>
						<div class="actions_menu">
							<ul>
												<li><g:remoteLink class="delete" controller="alert" action="delete" id="${alert?.id}" before="return confirm('Are you sure?');" params="[targetUri: (request.forwardURI - request.contextPath)]" >Delete</g:remoteLink></li>
											</ul>
										</div>
									</td>

                        </tr>
                    </g:each>

								
								
							</table>
							</div>
						</div>
						<!--[if !IE]>end table_wrapper<![endif]-->
						
						
					</div>
					<!--[if !IE]>end product list<![endif]-->
					
					
					<!--[if !IE]>start pagination<![endif]-->
					<div class="pagination">
						<span class="page_no">Page 1 of 217</span>
						
						<ul class="pag_list">
                	<g:paginate total="${alertInstanceTotal}" />
	
						</ul>
						
					</div>
					<!--[if !IE]>end pagination<![endif]-->
					
					
					
					
					
					
				</div>
				<!--[if !IE]>end sec info<![endif]-->
						
						
					
					
					</div>
				</div>
				<!--[if !IE]>end main info<![endif]-->

</div>
 
				
				
				
				
        </div>
    </body>
</html>
