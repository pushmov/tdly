

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Create CompanyStatistics</title>         
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">CompanyStatistics List</g:link></span>
        </div>
        <div class="body">
            <h1>Create CompanyStatistics</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${companyStatistics}">
            <div class="errors">
                <g:renderErrors bean="${companyStatistics}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="company">Company:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:companyStatistics,field:'company','errors')}">
                                    <g:select optionKey="id" from="${Company.list()}" name="company.id" value="${companyStatistics?.company?.id}" ></g:select>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="rfqbroadcast">Rfqbroadcast:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:companyStatistics,field:'rfqbroadcast','errors')}">
                                    <input type="text" id="rfqbroadcast" name="rfqbroadcast" value="${fieldValue(bean:companyStatistics,field:'rfqbroadcast')}" />
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="rfqresponse">Rfqresponse:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:companyStatistics,field:'rfqresponse','errors')}">
                                    <input type="text" id="rfqresponse" name="rfqresponse" value="${fieldValue(bean:companyStatistics,field:'rfqresponse')}" />
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="wtbbroadcast">Wtbbroadcast:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:companyStatistics,field:'wtbbroadcast','errors')}">
                                    <input type="text" id="wtbbroadcast" name="wtbbroadcast" value="${fieldValue(bean:companyStatistics,field:'wtbbroadcast')}" />
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="wtbresponse">Wtbresponse:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:companyStatistics,field:'wtbresponse','errors')}">
                                    <input type="text" id="wtbresponse" name="wtbresponse" value="${fieldValue(bean:companyStatistics,field:'wtbresponse')}" />
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="wtsbroadcast">Wtsbroadcast:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:companyStatistics,field:'wtsbroadcast','errors')}">
                                    <input type="text" id="wtsbroadcast" name="wtsbroadcast" value="${fieldValue(bean:companyStatistics,field:'wtsbroadcast')}" />
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="wtsresponse">Wtsresponse:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:companyStatistics,field:'wtsresponse','errors')}">
                                    <input type="text" id="wtsresponse" name="wtsresponse" value="${fieldValue(bean:companyStatistics,field:'wtsresponse')}" />
                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><input class="save" type="submit" value="Create" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
