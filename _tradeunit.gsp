<%@page import="test2.Manufacturer" %>
<%@page import="test2.Cond" %>
									<!--form-group-->
                  <div class="form-group">
                    <label for="part" class="col-sm-2 control-label">Part:</label>
                    <div class="col-sm-8">
											<input type="text" class="form-control" name="part" id="part" value="${fieldValue(bean:tradeunit,field:'part')}"  >
                    </div>
                  </div>
                  <!--form-group-->
                  <div class="form-group">
                    <label for="condition.id" class="col-sm-2 control-label">Condition:</label>
                    <div class="col-sm-8">
											<g:select from="${Cond.list()}" optionKey="id" optionValue="name" value="${tradeunit?.condition?.id}" name="condition.id" class="form-control" id="condition.id"></g:select>
                    </div>
                  </div>
                  <!--form-group-->
                  <div class="form-group">
                    <label for="manufacturer.id" class="col-sm-2 control-label">Manufacturer:</label>
										
                    <div class="col-sm-8">
											<g:select class="form-control" optionKey="id" from="${Manufacturer.list()}" optionValue="mfgname"  id="manufacturer.id" name="manufacturer.id" value="${fieldValue(bean:tradeunit,field:'manufacturer.id')}"></g:select>
                    </div>
                  </div>
                  <!--form-group-->
                  <!--form-group-->
                  <div class="form-group">
                    <label for="price" class="col-sm-2 control-label">Price:</label>
                    <div class="col-sm-8">
											<input type="text" id="price" class="form-control" name="price" value="${tradeunit?.price}" />
                    </div>
                  </div>
                  <!--form-group-->
                  <div class="form-group">
                    <label for="qty" class="col-sm-2 control-label">Quantity:</label>
                    <div class="col-sm-8">
											<input type="text" class="form-control" id="qty" name="qty" value="${tradeunit?.qty}" />
                    </div>
                  </div>
									