<%@page import="java.text.*" %>
<%@page import="java.util.*" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="pz" />
        <title>Inbox - Public Broadcasts</title>


    </head>
				<!--[if !IE]>start main info<![endif]-->
				<div id="main_info">
					<div id="main_info_bottom">
						
						<div class="title_wrapper">
							
							<ul class="search_tabs">
								<li><g:link  url="[controller:'broadcastResponse', action:'bcastresponse']"   ><span><span>Broadcast Inbox</span></span></g:link></li>
							<!-- 	<li><g:link  url="[controller:'privatebroadcastinbox', action:'list']"   ><span><span>Private Broadcast Inbox</span></span></g:link></li> -->
								<li><g:link  id="selected_search_tab"   url="[controller:'consumerinquiry', action:'list']"   ><span><span>Consumer Inquiry</span></span></g:link></li>
								<li><g:link   url="[controller:'myVendor',action:'listrequests']"   ><span><span>Vendor Requests</span></span></g:link></li>
							</ul>
						</div>

				<!--[if !IE]>start sec info<![endif]-->
				<div id="sec_info">
					
					<div class="row">
						<br><br>
					</div>
					
					
					<!--[if !IE]>start product list<![endif]-->
					<div id="product_list">
						
						<div id="product_list_menu">
							<!--[if !IE]>start product list tabs<![endif]-->
							<ul id="product_list_tabs">
								<li><a href="#" class="selected"><span><span>Featured</span></span></a></li>
								<li><a href="#"><span><span>All products</span></span></a></li>
								<li><a href="#"><span><span>Best Sellers</span></span></a></li>
							</ul>
							<!--[if !IE]>end product list tabs<![endif]-->
							<a href="#" class="update"><span><span><em>Add New Record</em><strong></strong></span></a>
						</div>

 			<g:form action="delete" >
						
						
						<!--[if !IE]>start table_wrapper<![endif]-->
						<div class="table_wrapper">
							<div class="table_wrapper_inner">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>



                   	        <g:sortableColumn property="partno" title="Part" />
                   	        <g:sortableColumn property="firstname" title="Firstname" />
                   	        <g:sortableColumn property="lastname" title="Lastname" />
                   	        <g:sortableColumn property="consumeremail" title="Email" />
                   	        <g:sortableColumn property="dateCreated" title="Date" />
                   	        <g:sortableColumn property="phone" title="Phone" />
<!--                   	        <g:sortableColumn property="alternatephone" title="Alternatephone" /> -->
                   	        <g:sortableColumn property="city" title="Location" />
<!--                   	        <g:sortableColumn property="state" title="State" />
                   	        <g:sortableColumn property="country" title="Country" />
 -->
                   	        <g:sortableColumn property="comments" title="Comments" />
<!--                   	        <g:sortableColumn property="postalcode" title="Postalcode" /> -->

				<td>Actions</td>
                        </tr>
<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm z");
		sdf.setTimeZone(TimeZone.getTimeZone(session.company.address.timezone));
	        
		
%>

                    <g:each in="${consumerinquiryInstanceList}" status="i" var="consumerinquiryInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td>${fieldValue(bean:consumerinquiryInstance, field:'partno')}</td>
                            <td>${fieldValue(bean:consumerinquiryInstance, field:'firstname')}</td>
                            <td>${fieldValue(bean:consumerinquiryInstance, field:'lastname')}</td>
                            <td>${fieldValue(bean:consumerinquiryInstance, field:'consumeremail')}</td>
                            <td>${sdf.format(consumerinquiryInstance.dateCreated)}</td>
                            <td>${fieldValue(bean:consumerinquiryInstance, field:'phone')}<br>${fieldValue(bean:consumerinquiryInstance, field:'alternatephone')}</td>
                            <td>${fieldValue(bean:consumerinquiryInstance, field:'city')},&nbsp;&nbsp;${fieldValue(bean:consumerinquiryInstance, field:'state')}<br>${fieldValue(bean:consumerinquiryInstance, field:'postalcode')}&nbsp;&nbsp;${fieldValue(bean:consumerinquiryInstance, field:'country')}</td>
                            <td>${fieldValue(bean:consumerinquiryInstance, field:'comments')}</td>

				<td>
						<div class="actions_menu">
							<ul>
												<li><g:remoteLink class="delete" controller="consumerinquiry" action="delete" id="${consumerinquiryInstance?.id}" before="return confirm('Are you sure?');" params="[targetUri: (request.forwardURI - request.contextPath)]" >Delete</g:remoteLink></li>
											</ul>
										</div>
									</td>

                        
                        </tr>
                    </g:each>



							</table>
							</div>
						</div>
						<!--[if !IE]>end table_wrapper<![endif]-->
						</g:form>
								<div class="row">
									<div class="inputs">
										<span class="button blue_button search_button"><span><span><em>Delete</em></span></span><input name="" onclick="return confirm('Are you sure?');"  type="submit" /></span>
									</div>
								</div>

						
					</div>
					<!--[if !IE]>end product list<![endif]-->
					
					
					<!--[if !IE]>start pagination<![endif]-->
					<div class="pagination">
						<span class="page_no">Page 1 of 217</span>
                                 <g:paginate controller="consumerinquiry" action="list" total="${consumerinquiryInstanceTotal}"  params="[activetab: 4]" /> 
					</div>
					<!--[if !IE]>end pagination<![endif]-->
					
					
					
					
					
					
				</div>
				<!--[if !IE]>end sec info<![endif]-->
						
						
					
					
					</div>
				</div>
				<!--[if !IE]>end main info<![endif]-->
				
				
				
				
			</div>
			<!--[if !IE]>end page<![endif]-->
			
			
			
		</div>
	</div>
<!--[if !IE]>end wrapper<![endif]-->

</html>







