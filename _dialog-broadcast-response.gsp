<script language="Javascript" >

$(document).ready(function()
        {

  $('#dialog-response').dialog({ 
				dialogClass:'alertbroadcastresponse',
        autoOpen: false,
        height: 550,
        width: 905,
        modal: true,
	open:function(event, id) {
$('textarea.tinymce').tinymce({
    selector: "textarea.tiny",
    theme: "modern",
    width: 850,
    height: 500,
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]


		});
	},
        buttons: {
            'Send Response': function() {

    		responseSubmit();
		tinyMCE.execCommand('mceRemoveControl',false,'mcetext');	
                $(this).dialog('close');

            },

            Cancel: function() {
		tinymce.execCommand('mceRemoveControl',false,'textarea');	
                $(this).dialog('close');
            }

        }        
    });

    $('#dialog-responseSave').dialog({
        autoOpen: false,
        height: 250,
        width: 350,
        modal: true,
        buttons: {
            Ok: function() {
                $(this).dialog('close');
            }

        }        
    });




	 });


	function showResponse(bid,rid) {
        	$('#bid').val(bid);
        	$('#rid').val(rid);
		$('#dialog-response').dialog('open');
		//var d = $('#dialog-response').position();
		(window).scrollTop();
 	}

	function responseSubmit() {

			    $.ajax({
    			    type: "POST",
    			    url: "/tdly/broadcastResponse/save",
			    cache: false,
    			    data: $('#formResponse').serialize(),
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
				   $('#dialog-responseSave').dialog("open");
 				   //setTimeout(function(){$('#dialog-message').dialog("close")},2000);
        			
    			    }
			    
                            });

       	 }

</script>



<div id="dialog-response" title="Broadcast Response">
            <g:form url="[action:'save', controller:'broadcastResponse']" method="post" class="search_form"  name="formResponse"  >
			<g:hiddenField name="bid" value="" />
			<g:hiddenField name="rid" value="" />

			<fieldset>
				<!--[if !IE]>start forms<![endif]-->
				<div class="row">
				<label>Response:</label>
					<div class="inputs">
						<span class="input_wrapper">
	 <textarea name="responsetext" class="tinymce" id="mcetext"  rows="10 cols="125"  ></textarea>
	
						 </span>
					</div>
				</div>

			</fieldset>
							
							
	    </g:form>
				
</div> 			

<div id="dialog-responseSave" title="Broadcast Response Sent"> 
	<p> 
		<span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span> 
		 Broadcast Response Sent.
	</p> 
</div>


