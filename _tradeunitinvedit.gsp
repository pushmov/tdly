<%@page import="test2.Manufacturer" %>
<%@page import="test2.Cond" %>

<div class="form-group">
	<label for="part" class="col-sm-2 control-label">Part:</label>
  <div class="col-sm-8">
		<input type="text" class="form-control" name="part" id="part" value="${item?.part}">
	</div>
</div>
<div class="form-group">
	<label for="part" class="col-sm-2 control-label">Condition:</label>
  <div class="col-sm-8">
		<g:select class="form-control" from="${Cond.list()}" optionKey="id" optionValue="name" name="condition.id" value="${item?.condition?.id}" ></g:select>
	</div>
</div>
<div class="form-group">
	<label for="part" class="col-sm-2 control-label">Manufacturer:</label>
  <div class="col-sm-8">
		<g:select class="form-control" optionKey="id" from="${Manufacturer.list()}" optionValue="mfgname" name="manufacturer.id" value="${item?.manufacturer?.id}"></g:select>
	</div>
</div>
<div class="form-group">
	<label for="part" class="col-sm-2 control-label">Price:</label>
  <div class="col-sm-8">
		<input type="text" class="form-control" name="price" id="price" value="${item?.price}">
	</div>
</div>
<div class="form-group">
	<label for="part" class="col-sm-2 control-label">Quantity:</label>
  <div class="col-sm-8">
		<input type="text" class="form-control" id="qty" name="qty" value="${item?.qty}">
	</div>
</div>