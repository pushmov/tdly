<%@page import="test2.Company" %>
<%@page import="test2.MyVendor" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta name="layout" content="pz" />

<title>Admin Panel</title>
<link media="screen" rel="stylesheet" type="text/css" href="css/admin.css"  />
<!--[if lte IE 6]><link media="screen" rel="stylesheet" type="text/css" href="css/ie.css" /><![endif]-->

<script> 
var myWin;
var theform;
var id;

Ext.onReady(function(){
Ext.BLANK_IMAGE_URL = 'images/s.gif'; 
Ext.QuickTips.init(); 

}); 

function showResponse(prm) {
      id = prm;
	theform=new Ext.FormPanel({

	labelAlign: 'top',

	frame:true,

	title: 'Multi Column, Nested Layouts and HTML editor',

	bodyStyle:'padding:5px 5px 0',

	width: 600,

	items: [{

	layout:'column',

	items:[{

	columnWidth:.5,

	layout: 'form',

	items: [{

	xtype:'textfield',

	fieldLabel: 'First Name',

	name: 'first',

      value: '<%=session.person.firstname%>',

	anchor:'95%'

	}]

	},{

	columnWidth:.5,

	layout: 'form',

	items: [{

	xtype:'textfield',

	fieldLabel: 'Last Name',

	name: 'last',

      value: '<%=session.person.lastname%>',

	anchor:'95%'

	},{

	xtype:'textfield',

	fieldLabel: 'Email',

	name: 'email',

      value: '<%=session.person.email%>',

	vtype:'email',

	anchor:'95%'

	}]

	}]

	},{

	xtype:'htmleditor',

	id:'message',

	name:'message',

	fieldLabel:'Message',

	height:200,

	anchor:'98%'

	}],


	buttons: [{ 
text: 'Save', 
handler: function(){ 
theform.getForm().submit({ 
url:'/test2/vendorRequest/save?id=' + id, 
			waitMsg:'Saving Data...',

success: function(f,a){ 
Ext.Msg.alert('Success', a.result.msg); 
myWin.close();
}, 
failure: function(f,a){ 
Ext.Msg.alert('Warning', 'Error'); 
} 
}); 
}
}, { 
text: 'Reset', 
handler: function(){ 
theform.getForm().reset(); 
}
}]
	});




myWin = new Ext.Window({ // 2 
id : 'myWin', 
height : 500, 
width : 700, 
items : [ 
theform 
] 
}); 
myWin.show();
}
</script>


    </head>
				<!--[if !IE]>start main info<![endif]-->
				<div id="main_info">
					<div id="main_info_bottom">
						
						<div class="title_wrapper">
							
							<ul class="search_tabs">
								<li><g:link    action="list"><span><span>Vendor List</span></span></g:link></li>
								<li><g:link  id="selected_search_tab" url="[action:'searchinit']"  ><span><span>Search Company</span></span></g:link></li>
								<li><g:link  url="[action:'listresponse']" ><span><span>Vendor Requests Inbox</span></g:link></li>
								<li><g:link   url="[action:'listrequest']" ><span><span>Vendor Requests Outbox</span></span></g:link></li>
							</ul>
						</div>

				<!--[if !IE]>start sec info<![endif]-->
				<div id="sec_info">
					
					<div class="row">
						<br><br>
					</div>
					
					
					<!--[if !IE]>start product list<![endif]-->
					<div id="product_list">
						
						<div id="product_list_menu">
							<!--[if !IE]>start product list tabs<![endif]-->
							<ul id="product_list_tabs">
								<li><a href="#" class="selected"><span><span>Featured</span></span></a></li>
								<li><a href="#"><span><span>All products</span></span></a></li>
								<li><a href="#"><span><span>Best Sellers</span></span></a></li>
							</ul>
							<!--[if !IE]>end product list tabs<![endif]-->
							<a href="#" class="update"><span><span><em>Add New Record</em><strong></strong></span></a>
						</div>
						
						
						<!--[if !IE]>start table_wrapper<![endif]-->
						<div class="table_wrapper">
							<div class="table_wrapper_inner">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>
                        
                        
                   	        <g:sortableColumn property="category" title="Category" />
                   	        <g:sortableColumn property="name" title="Name" />
                   	        <g:sortableColumn property="name" title="Membershiplevel" />
                   	        <g:sortableColumn property="country" title="Country" />
                   	        <g:sortableColumn property="state" title="State" />
                   	        <g:sortableColumn property="city" title="City" />

                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${companyList}" status="i" var="company">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				<g:if test="${company.id != session.company.id }">
        	                    <td>${fieldValue(bean:company, field:'category')}</td>
	                            <td>${fieldValue(bean:company, field:'name')}</td>
        	                    <td>${fieldValue(bean:company.membershiplevel, field:'description')}</td>
	                            <td>${fieldValue(bean:company.address, field:'country')}</td>
        	                    <td>${fieldValue(bean:company.address, field:'state')}</td>
	                            <td>${fieldValue(bean:company.address, field:'city')}</td>
					<td>
						<a href="javascript:void(0)" id="respond" onClick="javascript:showResponse('${company.id}');" >Add to My Vendors</a>
					</td>

				<td>
	</td>
	<div id="bresp"></div>

				</g:if>

                        
                       
                        
                        
                        </tr>
                    </g:each>
							</table>
							</div>
						</div>
						<!--[if !IE]>end table_wrapper<![endif]-->
						
						
					</div>
					<!--[if !IE]>end product list<![endif]-->
					
					
					<!--[if !IE]>start pagination<![endif]-->
					<div class="pagination">
						<span class="page_no">Page 1 of 217</span>

												                <g:paginate total="${MyVendor.count()}" />

						
					</div>
					<!--[if !IE]>end pagination<![endif]-->
					
					
					
					
					
					
				</div>
				<!--[if !IE]>end sec info<![endif]-->
						
						
					
					
					</div>
				</div>
				<!--[if !IE]>end main info<![endif]-->
				
				
				
				
			</div>
			<!--[if !IE]>end page<![endif]-->
			
			
		</div>
	</div>
<!--[if !IE]>end wrapper<![endif]-->
    </body>
</html>
