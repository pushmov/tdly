<%@ page import="test2.MyConnection" %>
<%@page import="test2.Person" %>
<%@page import="test2.MyConnection" %>
<%@page import="test2.Inventory" %>
<%@page import="test2.Broadcast" %>
<%@page import="com.ucbl.util.PZUtil" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta name="layout" content="pz" />
		<link href="${createLinkTo(dir:'assets',file:'partszap/css/app.css')}" rel="stylesheet">

<title>Admin Panel</title>
</head>
<g:render template="/dialog-broadcast-response" />

<main class="content" role="main">
	<div class="main-content">
		<div class="container">
			<div class="row">
				<g:if test="${myVendorList?.size() > 0}"
					<div class="inbox_top2">Sort by <span class="drop"><span class="drop_txt">recent activity</span> <i class="fa fa-caret-down"></i>
						<ul class="dropmenu">
							<li><a href="#">Drop Menu 1</a></li>
							<li><a href="#">Drop Menu 2</a></li>
							<li><a href="#">Drop Menu 3</a></li>
							<li><a href="#">Drop Menu 4</a></li>
						</ul>
						</span>
					</div>
				</g:if>
				<g:else>
					<p>Add to your Connections and get Updates on their Broadcasts and inventory uploads.</p>
					<div>Sort by <span class="drop"><span class="drop_txt">recent activity</span> <i class="fa fa-caret-down"></i>
						<ul class="dropmenu">
							<li><a href="#">Drop Menu 1</a></li>
							<li><a href="#">Drop Menu 2</a></li>
							<li><a href="#">Drop Menu 3</a></li>
							<li><a href="#">Drop Menu 4</a></li>
						</ul>
						</span>
					</div>
				</g:else>
			</div>
			
			<g:each in="${myConnectionInstanceList}" status="i" var="myConnectionInstance">
			<div class="row row-item clearfix">
				<div class="col-sm-12 col-md-1 col-lg-1">
					<div class="icon-img"><img src="images/icon_img.jpg" /></div>
				</div>
				<div class="col-sm-12 col-md-4 col-lg-4">
					<div class="con_m_heading"><g:link controller="person" action="profile" id="${myConnectionInstance.contact.id}" >${myConnectionInstance.contact.firstname} ${myConnectionInstance.contact.lastname} (${myConnectionInstance.contact.address.country.name})</g:link></div>
					<div class="con_m_para11">${myConnectionInstance.contact.company.name}</div>
					<div class="con_m_para1">${myConnectionInstance.contact.address.city}, ${myConnectionInstance.contact.address.state}</div>
					<div class="con_icon"><a href="javascript:void(0)" id="respond" onClick="javascript:showResponse('','${myConnectionInstance.contact.id}');" ><img src="../images/mailbox.png" alt="Mailbox" title="Mailbox" /></a></div>
				</div>
				<div class="col-sm-12 col-md-4 col-lg-4">
					<div class="con_m_para11"><a href="../broadcast/search?createdById=${myConnectionInstance.contact.id}" >Broadcasts : ${Broadcast.countByCreatedby(myConnectionInstance.contact)}</a></div>
					<div class="con_m_para11"><a href="../inventory/getInventory?pid=${myConnectionInstance.contact.id}" >Inventory : ${Inventory.countByCreatedBy(myConnectionInstance.contact)}</a></div>
					<div class="con_m_para11"><a href="../myConnection/list?pid=${myConnectionInstance.contact.id}" >Connection : ${MyConnection.countByPerson(myConnectionInstance.contact)}</a></div>
				</div>
				<div class="col-sm-12 col-md-3 col-lg-3">
					<div class="date_m">${PZUtil.format(myConnectionInstance.dateCreated)}</div>
					<g:isSelf person="${myConnectionInstance.person}" >
						<div class="date_m"><g:remoteLink class="delete" controller="myConnection" action="delete" id="${myConnectionInstance?.id}" before="return confirm('Are you sure?');" ><img src="../images/deletebutton.png" /></g:remoteLink></div>
					</g:isSelf>
				</div>
			</div>
			</g:each>
			
			<div class="pagination_n">
				<g:paginate total="${myConnectionInstanceTotal}" />
			</div>
		</div>
	</div>
</main>
<script>
	$(document).ready(function () {
		$(".drop").hover(
			function () {
				$('ul.dropmenu').slideDown('medium');
			}, 
			function () {
				$('ul.dropmenu').slideUp('medium');
			}
		);
		$('.dropmenu li a').click(function(){
			$('.drop_txt').html($(this).text());
			$('ul.dropmenu').slideUp('medium');
		});
	});
</script>
</body>
</html>
