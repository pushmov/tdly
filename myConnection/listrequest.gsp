<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.MyConnection" %>
<%@page import="test2.ConnectionRequest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta name="layout" content="pz" />

<title>Admin Panel</title>
<link media="screen" rel="stylesheet" type="text/css" href="css/admin.css"  />
<!--[if lte IE 6]><link media="screen" rel="stylesheet" type="text/css" href="css/ie.css" /><![endif]-->

	<script>

    		var vid;


	function addVendor(id) {
		vid = id;
		Ext.Msg.show({
		   title:'Save Changes?',
		   msg: 'Would you also like to add this company to your vendor list?',
		   buttons: Ext.Msg.YESNO,
		   fn: addToMyList
		  // animEl: 'elId'
		});

	}

	</script>



</head>

<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm z");
		sdf.setTimeZone(TimeZone.getTimeZone(session.company.address.timezone));
	        
		
%>

				<!--[if !IE]>start main info<![endif]-->
				<div id="main_info">
					<div id="main_info_bottom">

				<!--[if !IE]>start sec info<![endif]-->
				<div id="sec_info">
					
					<!--[if !IE]>start product list<![endif]-->
					<div id="product_list">
						
						
						<!--[if !IE]>start table_wrapper<![endif]-->
						<div class="table_wrapper">
							<div class="table_wrapper_inner">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>

                   	        <g:sortableColumn property="name" title="Name" />
                   	        <g:sortableColumn property="name" title="Company" />
                   	        <g:sortableColumn property="country" title="Location" />

									<th><span>Date Created</span></th>
                   	        <g:sortableColumn property="message" title="Message" />
									
									<th>Actions</th>
								</tr>
								
                    <g:each in="${connectionRequestList}" status="i" var="connectionRequest">
                        <tr class="${(i % 2) == 0 ? 'first' : 'second'}">
	                            <td>${connectionRequest.contact.firstname}&nbsp;${connectionRequest.contact.lastname}</td>
	                            <td>${connectionRequest.contact.address.city}, ${connectionRequest.contact.address.state} - ${connectionRequest.contact.company.address.country}</td>
				    <td>${sdf.format(connectionRequest.dateCreated)}</td>
        	                    <td>${fieldValue(bean:connectionRequest, field:'message')}</td>
				<td>
			<div class="actions_menu">
				<ul>
					<li><g:remoteLink class="delete" controller="connectionRequest" action="delete" id="${connectionRequest?.id}" before="return confirm('Are you sure?');" >Delete</g:remoteLink></li>
				</ul>
			</div>

				</td>



                        </tr>
                    </g:each>

								
							</table>
							</div>
						</div>
						<!--[if !IE]>end table_wrapper<![endif]-->
						
						
					</div>
					<!--[if !IE]>end product list<![endif]-->
					
					
					<!--[if !IE]>start pagination<![endif]-->
					<div class="pagination">
						<span class="page_no">Page 1 of 217</span>

												                <g:paginate total="${ConnectionRequest.countByPerson(session.person)}" />

						
					</div>
					<!--[if !IE]>end pagination<![endif]-->
					
					
					
					
					
					
				</div>
				<!--[if !IE]>end sec info<![endif]-->
						
						
					
					
					</div>
				</div>
				<!--[if !IE]>end main info<![endif]-->
				
				
				
				
			</div>
			<!--[if !IE]>end page<![endif]-->
			
			
		</div>
	</div>
<!--[if !IE]>end wrapper<![endif]-->

</body>
</html>
