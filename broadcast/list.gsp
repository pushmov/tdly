<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.Broadcast" %>
<%@page import="test2.BroadcastFilter" %>
<%@page import="test2.Person" %>
<%@page import="test2.Manufacturer" %>
<%@page import="test2.Continent" %>
<%@page import="test2.AppFlags" %>

<meta name="layout" content="pz" />

<link href="${createLinkTo(dir:'assets',file:'partszap/css/app.css')}" rel="stylesheet">

<script language="Javascript" >

	var prevSelect = null;
	var oTable;
	var oSettings;

	var oTableM;
	var oSettingsM;


$(document).ready(function()
        {




	$( "#tabs" ).tabs({

	load: function(event, ui) {

//	 $('input').customInput();

// $('input:checkbox:not([safari])').checkbox();
				$('input[safari]:checkbox').checkbox({cls:'jquery-safari-checkbox'});

     //syntax highlighter
        hljs.tabReplace = '    ';
        hljs.initHighlightingOnLoad();

        //collapsible management
        $('.collapsible').collapsible({
                defaultOpen: 'section1',
                cookieName: 'nav'
        });
        $('.page_collapsible').collapsible({
                defaultOpen: 'body-section1',
                cookieName: 'body'
        });

	if ( $('#body-section1').collapsible('collapsed') ) { 
		$('#body-section1').attr("class","page_collapsible collapse-close");
	}
	else {
		$('#body-section1').attr("class","page_collapsible collapse-open");
        }

	if ( $('#body-section2').collapsible('collapsed') ) { 
		$('#body-section2').attr("class","page_collapsible collapse-close");
	}
	else {
		$('#body-section2').attr("class","page_collapsible collapse-open");
        }

	if ( $('#body-section3').collapsible('collapsed') ) { 
		$('#body-section3').attr("class","page_collapsible collapse-close");
	}
	else {
		$('#body-section3').attr("class","page_collapsible collapse-open");
        }

	if ( $('#body-section4').collapsible('collapsed') ) { 
		$('#body-section4').attr("class","page_collapsible collapse-close");
	}
	else {
		$('#body-section4').attr("class","page_collapsible collapse-open");
        }
	if ( $('#body-section5').collapsible('collapsed') ) { 
		$('#body-section5').attr("class","page_collapsible collapse-close");
	}
	else {
		$('#body-section5').attr("class","page_collapsible collapse-open");
        }

	if ( $('#body-section6').collapsible('collapsed') ) { 
		$('#body-section6').attr("class","page_collapsible collapse-close");
	}
	else {
		$('#body-section6').attr("class","page_collapsible collapse-open");
        }
	if ( $('#body-section7').collapsible('collapsed') ) { 
		$('#body-section7').attr("class","page_collapsible collapse-close");
	}
	else {
		$('#body-section7').attr("class","page_collapsible collapse-open");
        }

	$('.defaultP input').ezMark();
	$('.customP input[type="checkbox"]').ezMark({checkboxCls: 'ez-checkbox-green', checkedCls: 'ez-checked-green'})


	

  $("select#continents").change(function(){
      $("#countrySelect").load("../continent/updateSelectFilter",{continentid: $(this).val(),prevcontinentid : prevSelect ,countryid: $("#countries").val(), domain: 'broadcastFilter' , ajax: 'true'});
	prevSelect = $(this).val();
  });

 
	    	try {
		oHandler = $("#filterdd").msDropDown({mainCSS:'dd2'}).data("dd");
		oHandler = $("#filterddM").msDropDown({mainCSS:'dd2'}).data("dd");
		//oHandler = $("#websites3").msDropDown({mainCSS:'dd2'}).data("dd");
		oHandler = $("#partlist").msDropDown({mainCSS:'dd2'}).data("dd");
		//alert($.msDropDown.version);
		//$.msDropDown.create("body select");
		$("#ver").html($.msDropDown.version);
		} catch(e) {
			alert("Error: "+e.message);
		}
 


              $('#formMyCountries').submit( function() {

			    $.ajax({
    			    type: "POST",
    			    url: "../preferences/saveCountries",
			    cache: false,
    			    data: $(this).serialize(),
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
				   $('#dialog-responseSave').dialog("open");
 				   //setTimeout(function(){$('#dialog-message').dialog("close")},2000);
        			
    			    }
			    
                            });

		    
        	            return false;

              });



              $('#filterResults').live("click", function() {

			$("#filterForm").attr("action","../broadcast/filter");
    			$("#filterForm").submit();
			
              });

              $('#filterResultsM').live("click", function() {

			$("#filterFormM").attr("action","../broadcast/filter");
    			$("#filterFormM").submit();
			
              });

              $('#mycountrieslink').live("click", function() {
			$.get("../preferences/getMyCountries",
				function(data,textStatus,jqXHR) {
					$("#myCountriesList").replaceWith(data);
				}

			);

	      		$('#dialog-mycountries').dialog('open');			
              });



  $('#dialog-form').dialog({
        autoOpen: false,
        height: 600,
        width: 650,
        modal: true,
        buttons: {
            SaveFilter: function() {

		var filtername = $('#filterNameText').val();
		$('#filterForm #filterName').val(filtername);
                //alert($('#filterForm #filterName').val());
		$("#filterForm").attr("action","../broadcastFltr/save");
    		submitFilter();
                $(this).dialog('close');

            },

            Cancel: function() {
                $(this).dialog('close');
            }

        }        
    });

   $('#popup_area').dialog({
        autoOpen: false,
        height: 600,
        width: 650,
        modal: true
    });


  $('#dialog-mycountries').dialog({ 
        autoOpen: false,
        height: 600,
        width: 650,
        modal: true,
        position: [0,0],
        buttons: {
            SaveFilter: function() {

    		$("#formMyCountries").submit();
                $(this).dialog('close');

            },

            Cancel: function() {
                $(this).dialog('close');
            }

        }        
    });



  $('#dialog-company').dialog({ 
        autoOpen: false,
        height: 600,
        width: 950,
        modal: true,
        buttons: {

            Cancel: function() {
                $(this).dialog('close');
            }

        }        
    });



  $('#dialog-form-M').dialog({
        autoOpen: false,
        height: 600,
        width: 650,
        modal: true,
        buttons: {
            SaveFilter: function() {
		var filtername = $('#filterNameTextM').val();
		//alert(filtername);
		$('#filterFormM #filterName').val(filtername);
		$("#filterFormM").attr("action","../broadcastFltr/save");
    		submitFilterM();
                $(this).dialog('close');

            },

            Cancel: function() {
                $(this).dialog('close');
            }

        }        
    });

  $("#saveFilterButton1").live("click", function() {
	$('#dialog-form').dialog('open');
    });

  $("#saveFilterM").live("click", function() {
	$('#dialog-form-M').dialog('open');
    });
 
/*
  $("select#filter").live("change", function(){
      getFilter($(this).val());
  });

  $("select#filterM").live("change", function(){
      getFilterM($(this).val());
  });
*/


  $("#filterForm").live("submit", function(){
    if ( ( $("#filterForm").attr("action") == '' ) || ( $("#filterForm").attr("action") == '../broadcast/filter' ) ) {	
      var url = '../broadcast/filter?'+$(this).serialize();
      $.ajax({
        type: 'GET',
        url: url,
        error: function(data, textStatus, jqXHR) {
          alert("Error: " + textStatus);
        },
        success: function(data, textStatus, jqXHR) {
          
          $("#tabs-1 .tab-content-right").replaceWith(data);
        }
      })
    } else {
			    
			    
			    $.ajax({
    			    type: "POST",
    			    url: "../broadcastFltr/save",
			    cache: false,
    			    data: $(this).serialize(),
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
				if (data.success) {
				//	alert('Filter Saved');
					$('#filterselect').load("../broadcastFilter/broadcastFilterSelect")
				}
				else {
					alert('Error saving');
					//alert(data);
				}
    			    }
			    
                            });

		    }
        	            return false;

  });


  $("#filterFormM").live("submit", function(){


		    if ( ( $("#filterFormM").attr("action") == '' ) || ( $("#filterFormM").attr("action") == '../broadcast/filter' ) ) {			    
        	            var url = '../broadcast/filter?'+$(this).serialize();
			    $.ajax({
            			type: 'GET',
            			url: url,
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
				$("#tabs-2 .tab-content-right").replaceWith(data);
    			    }

        		    })

		    }
		    else {

			    $.ajax({
    			    type: "POST",
    			    url: "../broadcastFltr/save",
			    cache: false,
    			    data: $(this).serialize(),
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
				$('#filterselectM').load("../broadcastFilter/broadcastFilterSelect")
    			    }
			    
                            });

		    }
        	            return false;

  });

	}
  
  }); // tabs load


  }); // onready


	function filterStatus(v) {
		if (v == 0) {
			return "add";	
		} else {
			return "";
		}
		
	}


 

 function submitFilter() {
 

		//alert('Filtered Form Submit');

		    if ( ( $("#filterForm").attr("action") == '' ) || ( $("#filterForm").attr("action") == '../broadcast/filter' ) ) {	
		//alert('Filtered Form Submit');

        	            var url = '../broadcast/filter?'+$(this).serialize();
			    $.ajax({
            			type: 'GET',
            			url: url,
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
				//alert('Filtered');
				//$("#tabs").tabs('load',0(0);
				$("#tabSingle").html(data);

				
    			    }

        		    })


		    }
		    else {
                            //alert('Saving Filter S');
			    
			    
			    $.ajax({
    			    type: "POST",
    			    url: "../broadcastFltr/save",
			    cache: false,
    			    data: $('#filterForm').serialize(),
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
				if (data.success){
					alert('Filter Saved');
					$('#filterselect').load("../broadcastFltr/broadcastFilterSelect");
				}
				else {
					//alert(data.msg);
				}
    			    }
			    
                            });

		    }
        	            return false;
  }


 function submitFilterM() {
 

		//alert('Filtered Form Submit');

		    if ( ( $("#filterFormM").attr("action") == '' ) || ( $("#filterFormM").attr("action") == '../broadcast/filter' ) ) {	
		//alert('Filtered Form Submit');

        	            var url = '../broadcast/filter?'+$(this).serialize();
			    $.ajax({
            			type: 'GET',
            			url: url,
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
				//alert('Filtered');
				//$("#tabs").tabs('load',0(0);
				$("#tabSingle").html(data);

				
    			    }

        		    })


		    }
		    else {
                            //alert('Saving Filter M');
			    
			    
			    $.ajax({
    			    type: "POST",
    			    url: "../broadcastFltr/save",
			    cache: false,
    			    data: $('#filterFormM').serialize(),
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
				if (data.success) {
					//alert('Filter Saved');
					$('#filterselectM').load("../broadcastFltr/broadcastFilterSelectM")
				}
				else {
					//alert(data.msg);
				}
    			    }
			    
                            });

		    }
        	            return false;
  }

 
 function submitFilterM1() {

		    if ( ( $("#filterFormM").attr("action") == '' ) || ( $("#filterFormM").attr("action") == '../broadcast/filter' ) ) {			    
        	            var url = '../broadcast/filter?'+$(this).serialize();
			    $.ajax({
            			type: 'GET',
            			url: url,
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
				$("#tabMultiple").html(data);
    			    }

        		    })

		    }
		    else {

			    $.ajax({
    			    type: "POST",
    			    url: "http://localhost:8080/pz/broadcastFilter/save",
			    cache: false,
    			    data: $(this).serialize(),
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
				$('#filterselectM').load("../broadcastFilter/broadcastFilterSelect")
        		//	alert("Filter Saved");
    			    }
			    
                            });

		    }
        	            return false;



 } 
  
 function getFilter(id) {
	$.get("../broadcastFilter/get",{id: id},
		function(data,textStatus,jqXHR) {
			$('#filterForm')[0].reset();
			$('#filter').val(data.id);
			if(data.myVendors) {
				alert(data.myVendors);
				alert($('#filterForm input:radio[name="companyType"][value="myVendors"]').val());
				$('#filterForm input:radio[name="companyType"][value="myVendors"]').prop('checked', true);
			}
      
			$('#filterForm #keyword').val(data.keyword);

			if (data.broadcasttype != null) {
				var btArr = data.broadcasttype.split(',');

			
				if( $.inArray('WTB',btArr) != -1) {
					$('#filterForm #wtb').attr('checked', true);
				}
				else {
					$('#filterForm #wtb').attr('checked', false);
				}
			
				if( $.inArray('WTS',btArr) != -1) {
					$('#filterForm #wts').attr('checked', true);
				}
				else {
					$('#filterForm #wts').attr('checked', false);
				}

			}
      
      
      $('#filterForm .checkbox-condition input[type="checkbox"]').removeAttr('checked');
      var conditionArr = new Array(data.conditions.length);
      
      for(i=0;i<data.conditions.length;i++) {
        conditionArr[i] = data.conditions[i].id;
        $('#condition-' + data.conditions[i].id).attr('checked', 'checked');
      }
      
      //$("#filterForm #conditionS-filterinput").val(conditionArr);
      $("#filterForm #partlist").val(data.partlist);	

			$('#filterForm input[name="myCountries"]').filter('[value='+ data.myCountries + ']').attr('selected', 'selected');
		
    $('#filterForm .checkbox-manufacturer input[type="checkbox"]').removeAttr('checked');
			var mfgArr = new Array(data.manufacturers.length);	
			for(i=0;i<data.manufacturers.length;i++) {
				mfgArr[i] = data.manufacturers[i].id;
        
        $('#filterForm #manufacturer-' + data.companies[i].id).attr('checked', 'checked');
			}
				$("#filterForm #manufacturer").val(mfgArr);

        $('#filterForm .checkbox-company input[type="checkbox"]').removeAttr('checked');
			var companyArr = new Array(data.companies.length);	
			for(i=0;i<data.companies.length;i++) {
				companyArr[i] = data.companies[i].id;
        
        $('#filterForm #company-' + data.companies[i].id).attr('checked', 'checked');
			}
				$("#filterForm #company").val(companyArr);


        $('#filterForm .checkbox-country input[type="checkbox"]').removeAttr('checked');
			var countryArr = new Array(data.countries.length);	
			for(i=0;i<data.countries.length;i++) {
				countryArr[i] = data.countries[i].id;
        
        $('#filterForm #country-' + data.countries[i].id).attr('checked', 'checked');
			}
      
			$("#filterForm #country").val(countryArr);


			$("#filterForm").attr("action","../broadcast/filter");
    			$("#filterForm").submit();

                }
             );
	
 }

 function getFilterM(id) {
	$.get("../broadcastFilter/get",{id: id},
		function(data,textStatus,jqXHR) {

			$('#filterFormM')[0].reset();
			$('#filterFormM #filter').val(data.id);
		
			//alert($.toJSON(data));
			//alert('filteridM : ' + data.id);


                        if(data.myVendors) {
                                //alert(data.myVendors);
                                $('input[name="companyType"][value="myVendors"]').attr('checked', true);
                        }

			$('#filterFormM #keyword').val(data.keyword);

			if (data.broadcasttype != null) {
				var btArr = data.broadcasttype.split(',');

			
				if( $.inArray('WTB',btArr) != -1) {
					$('#filterFormM #wtb').attr('checked', true);
				}
			
				if( $.inArray('WTS',btArr) != -1) {
					$('#filterFormM #wts').attr('checked', true);
				}

			}


			$('#filterFormM input:radio[name="myCountries"]').filter('[value='+ data.myCountries + ']').attr('checked', true);
		

//			$("#continents option:first").attr('selected','selected');
	//	        $("#filterFormM #countrySelect").load("../continent/updateSelectFilter",{continentid: $('#continents').val(),prevcontinentid : prevSelect ,countryid: null, domain: 'broadcastFilter' , ajax: 'true'});
	//		prevSelect = $(this).val();


                      var companyArr = new Array(data.companies.length);
                        for(i=0;i<data.companies.length;i++) {
                                companyArr[i] = data.companies[i].id;
                        }
                                $("#filterFormM #company").val(companyArr);


                        var countryArr = new Array(data.countries.length);
                        for(i=0;i<data.countries.length;i++) {
                                countryArr[i] = data.countries[i].id;
                        }
                        $("#filterFormM #country").val(countryArr);


			$("#filterFormM").attr("action","../broadcast/filter");
    			$("#filterFormM").submit();

		


			

                }
             );
	

 }

 function saveFilter() {
	$('#dialog-form').dialog('open');
 }

 function saveBB() {
	//alert('here');
	$('#dialog-form-M').dialog('open');
 }


function getCompany(cid) {

	    $.ajax({
		    type: "GET",
		    url: "../company/showajax",
		    cache: false,
		    data: { id : cid },
		    error: function(data, textStatus, jqXHR) {
	    		alert("Error: " + textStatus);
    		    },
	    	    success: function(data, textStatus, jqXHR) {
		    	$("#dialog-company").html(data)
			$("#dialog-company").dialog('open');
	    	    }
	    });


}




</script>

<main class="content" role="main">
	<div class="main-content">
		
		<div id="tabs">
			<div role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" aria-controls="tabs-1" aria-labelledby="ui-id-1" aria-selected="true">
						<a href="../broadcast/listFromTab?parttype=Single&myconnections=<%=flash.myconnections%>&keyword=<%=flash.keyword%>&createdById=<%=flash.createdById%>" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1">Single Part Broadcasts</a>
					</li>
					<li role="presentation" tabindex="-1" aria-controls="tabs-2" aria-labelledby="ui-id-2" aria-selected="false">
						<a href="../broadcast/listFromTab?parttype=Multiple&myconnections=<%=flash.myconnections%>&keyword=<%=flash.keyword%>&createdById=<%=flash.createdById%>" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-2">Multi Part Broadcasts</a>
					</li> 
				</ul>
			</div>
				<!--eachtab start -->
			<div class="tab-content">
				<div class="tab-pane" id="tabs-1" aria-labelledby="ui-id-1" role="tabpanel" style="display: block;" aria-expanded="true" aria-hidden="false">
				</div>
				<div class="tab-pane" id="tabs-2" aria-labelledby="ui-id-2" role="tabpanel" style="display: none;" aria-expanded="false" aria-hidden="true">
				</div>
			</div>
		</div>
	</div>
</main>


<div id="dialog-company" title="Company"></div>

<div id="popup_area">
	<div id="search_area">
		<input type="text" id="search" /><span id="messages"></span>
	</div>
	
	<table id="countries"></table>
	<table id="seleced_filters"></table>
	
	<div id="buttons_area">
		<span id="cancel"> Cancel </span><span id="apply"> Apply filter </span> 
	</div>
	
	<div id="dialog" title="Message" style="display:none">The filters has been added</div>
</div>

<div id="dialog-form" title="Create new Reminder">
	<table>
		<tbody>
			<g:hiddenField name="id" value="${broadcastFilterInstance?.id}" />
			<tr class="prop">
				<td valign="top" class="name">
					<g:message code="broadcastFilter.name.label" default="Filter Name" /></label>
				</td>
				<td valign="top" >
					<input type="text" id="filterNameText" name="filterNameText" value="" />
				</td>
			</tr>
		</tbody>
	</table>
</div>

<div id="dialog-form-M" title="Create new Reminder">
	<table>
		<tbody>
			<g:hiddenField name="id" value="${broadcastFilterInstance?.id}" />
			<tr class="prop">
				<td valign="top" class="name">
					<g:message code="broadcastFilter.name.label" default="Filter Name" /></label>
				</td>
				<td valign="top" >
					<input type="text" id="filterNameTextM" name="filterNameTextM" value="" />
				</td>
			</tr>
		</tbody>
	</table>
</div>

<button type="button" data-target="#modal_broadcast_success_broadcastlist" data-toggle="modal"></button>
<div class="modal fade broadcasts-popup" id="modal_broadcast_success_broadcastlist" tabindex="-1" role="dialog" aria-labelledby="modal_single_part_broadcastsLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="modal_single_part_broadcastsLabel">Success</h4>
			</div>
			<!--modal-header-->
			<div class="modal-body">
				<div id="createsingle" class="form-content">Broadcast Sent.</div>
			</div>
		</div>
		<!--modal-content-->
	</div>
	<!--modal-dialog-->
</div>

<g:render template="/dialog-broadcast-response" />
<g:render template="/dialog-broadcast" />
<script>
$(document).ready(function(){
	$(document).on('click', '#submit-broadcast-single', function(){
		var form = $(this).closest('form');
		$.post(form.attr('action'), form.serialize() + "&description=" + $('#fsingle .Editor-editor').html(), function(response){
			if(response == 'good'){
				//$('#fsingle .ajax-resp').text('Success');
				setTimeout(function(){
					form.find('button[data-dismiss="modal"]').click();
					$('.modal-backdrop').remove();
					$('button[data-target="#modal_broadcast_success_broadcastlist"]').click();
					$('.ajax-resp').text('');
				}, 1000);
				
			} else {
				$('#createsingle').html(response);
			}
		});
	});

        $(document).on('click', '#submit-broadcast-multiple', function(){
            var form = $('#fmultiple');
            $('.ajax-resp').text('');									
            $.ajax({
                type: 'POST',
		url: form.attr('action'),
		data: form.serialize() + "&description=" + $('#fmultiple .Editor-editor').html(),
		success: function(response){
                    if(response == 'good'){
                        //$('#fmultiple .ajax-resp').text('Success');
			setTimeout(function(){
                            form.find('button[data-dismiss="modal"]').click();
                            $('.modal-backdrop').remove();
                            $('button[data-target="#modal_broadcast_success_broadcastlist"]').click();
                            $('.ajax-resp').text('');
			}, 1000)
                    } else {
                        $('#createmultiple').html(response);
                    }
		}
            });
	});
});
</script>
	
	<script src="${createLinkTo(dir:'assets',file:'partszap/js/fontfaceobserver.js')}assets/js/"></script>
  <script src="${createLinkTo(dir:'assets',file:'partszap/js/bowser.min.js')}assets/js/"></script>
  <script src="${createLinkTo(dir:'assets',file:'partszap/js/responsive-tables.js')}"></script>
  <script src="${createLinkTo(dir:'assets',file:'partszap/js/editor.js')}"></script>
  <script src="${createLinkTo(dir:'assets',file:'partszap/js/main.js')}"></script>
