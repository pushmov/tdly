<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="test2.Broadcast" %>
<%@page import="test2.BroadcastFilter" %>
<%@page import="test2.Manufacturer" %>
<%@page import="test2.AppFlags" %>
<%@page import="com.ucbl.util.PZUtil" %>

<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm z");
		if(session.person?.address?.timezone)
		sdf.setTimeZone(TimeZone.getTimeZone(session.person?.address?.timezone));

			
%>

		<!-- box area2 starts -->
		<div class="multiplepart">
		<g:each in="${broadcastList}" status="i" var="broadcast">
			<div class="row">
				<div class="item-feed clearfix">
					<div class="col-sm-12 col-md-1 col-lg-1">
						<img alt="Name of ..." src="/tdly/assets/partszap/img/avatar.jpg">
					</div>
					<div class="col-sm-12 col-md-9 col-lg-9">
						<div class="list-feed-heading">
							<h4>${broadcast?.createdby?.firstname} ${broadcast?.createdby?.lastname} <small>( ${broadcast.company.address.city}&nbsp;,${broadcast.company.address.country.name} )</small></h4>	
						</div>
						<div class="list-feed-sub-heading">
							<strong>Broadcast :</strong> <%=PZUtil.format(new Timestamp(broadcast.dateCreated.getTime()))%>
							<strong>${broadcast.broadcasttype}</strong>
							<strong>${broadcast.company.name}</strong>
						</div>
						<div class="list-feed-text">
							<h3><strong>${broadcast.title}</strong></h3>
							<p class="htmldscr">${broadcast.description}</p>
						</div>
						<g:if test="${broadcast.company.id != session.company.id}">
						<button class="btn btn-small btn-broadcast" id="respond" onClick="javascript:showResponse('${fieldValue(bean:broadcast, field:'id')}','${broadcast?.createdby?.id}');" type="button">Respond</button>
						</g:if>
						
						<!--list-feed-text-->
					</div>
					<div class="col-sm-12 col-md-2 col-lg-2 text-right">
						<time datetime="" class="list-feed-time"><%=PZUtil.format(new Timestamp(broadcast.dateCreated.getTime()))%></time>
					</div>
				</div>
			</div>
		</g:each>
		</div>
		

		<div class="tab-bottom-pagination">
            <util:remotePaginate  params="${flash.filterParams}" action="filter" total="${broadcastTotal}" update="tableRowM" />
		</div>
		<script>
		$(document).ready(function(){
			$('.htmldscr').each(function(){	
				var html = $(this).text().replace(',', '');
				$(this).html(html);
			});
		});
		</script>