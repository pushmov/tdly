<%@page import="test2.Manufacturer" %>
<head>
<meta name="layout" content="pz" />
<link href="${createLinkTo(dir:'assets',file:'partszap/css/app.css')}" rel="stylesheet">
<r:script disposition="defer" language="Javascript" >
	$(document).ready(function() {


	$( "#tabs" ).tabs({
	
	load:function(event, ui){


/*
   $('textarea.tinymce').tinymce({

    selector: "textarea.tiny",
    theme: "modern",
    width: 850,
    height: 500,
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]



		});   
 */
     }
        });
        



    $('#dialog-message').dialog({
        autoOpen: false,
        height: 150,
        width: 250,
        modal: true,
        buttons: {
            Ok: function() {
                $(this).dialog('close');
            }

        }        
    });

              $('#bsingle').live("click", function() {

                        submitSingle();

              });








		    
 


              $('#bmultiple').live( "click",  function() {

                        submitMultiple();
			
              });



	});


        function submitSingle() {

		      	    $('#showCreateButtonS').val(true);
                            $.ajax({
                            type: "POST",
                            url: "../broadcast/save",
                            cache: false,
                            data: $('#fsingle').serialize(),
                            error: function(data, textStatus, jqXHR) {
                                alert("Error: Occured creating broadcast.Please try again" );
                            },
                            success: function(data, textStatus, jqXHR) {
				   if(data.indexOf("errors") != -1) {
                                        $('#createsingle').html(data);
                                   }
                                   else {
                                   	$('#dialog-message').dialog("open");
                                        $('.errors').empty();
				   }
                                   //setTimeout(function(){$('#dialog-message').dialog("close")},2000);

                            }

                            });



        }

	function submitMultiple() {

			    //tinyMCE.triggerSave();

		      	    $('#showCreateButtonM').val(true);
			    $.ajax({
    			    type: "POST",
    			    url: "../broadcast/savemultiple",
			    cache: false,
    			    data: $('#fmultiple').serialize(),
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
				   if(data.indexOf("errors") != -1) {
                                        $('#createmultiple').html(data);
                                   }
                                   else {
                                        $('#dialog-message').dialog("open");
                                        $('.errors').empty();
                                   }
 				   //setTimeout(function(){$('#dialog-message').dialog("close")},2000);
        			
    			    }
			    
                            });
         }

$(document).on('click', '#submit-broadcast-single', function(){
    var form = $(this).closest('form');
    $('#createsingle').find('.alert').remove();
        $.post(form.attr('action'), form.serialize() + "&description=" + $('#fsingle .Editor-editor').html(), function(response){
            if(response == 'good'){
                //$('#fsingle .ajax-resp').text('Success');
                setTimeout(function(){
                    $('#createsingle').prepend('<div class="alert alert-success">Broadcast Sent</div>');
                }, 1000);

            } else {
                $('#createsingle').html(response);
            }
	});
});

$(document).on('click', '#submit-broadcast-multiple', function(){
    var form = $('#fmultiple');
    $('#createmultiple').find('.alert').remove();
    $('.ajax-resp').text('');									
    $.ajax({
    type: 'POST',
        url: form.attr('action'),
        data: form.serialize() + "&description=" + $('#fmultiple .Editor-editor').html(),
        success: function(response){
            if(response == 'good'){
                //$('#fmultiple .ajax-resp').text('Success');
                setTimeout(function(){
                    $('#createmultiple').prepend('<div class="alert alert-success">Broadcast Sent</div>');
                }, 1000)
            } else {
                $('#createmultiple').html(response);
            }
        }
    });
});

</r:script>




</head>

<main class="content" role="main">
	<div class="main-content">
		
		<div id="tabs">
			<div role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" aria-controls="tabs-1" aria-labelledby="ui-id-1" aria-selected="true">
						<a href="../broadcast/createsingle" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1">Single Part Broadcasts</a>
					</li>
					<li role="presentation" tabindex="-1" aria-controls="tabs-2" aria-labelledby="ui-id-2" aria-selected="false">
						<a href="../broadcast/createmultiple" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-2">Multi Part Broadcasts</a>
					</li>
				</ul>
			</div>
				<!--eachtab start -->
			<div class="tab-content">
				<div class="tab-pane" id="tabs-1" aria-labelledby="ui-id-1" role="tabpanel" style="display: block;" aria-expanded="true" aria-hidden="false">
				</div>
				<div class="tab-pane" id="tabs-2" aria-labelledby="ui-id-2" role="tabpanel" style="display: none;" aria-expanded="false" aria-hidden="true">
				</div>
			</div>
		</div>
	</div>
</main>

<div id="dialog-message" title="Broadcast Created">
	<p> 
		<span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span> 
		 Broadcast Created.
	</p> 
</div>
