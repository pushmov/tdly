<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.Broadcast" %>
<%@page import="test2.BroadcastFilter" %>
<%@page import="test2.Manufacturer" %>
<%@page import="test2.Company" %>
<%@page import="test2.Continent" %>
<%@page import="test2.AppFlags" %>


<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm z");
		if(session.person?.address?.timezone)
		sdf.setTimeZone(TimeZone.getTimeZone(session.person?.address?.timezone)); 
			
%>
<!--
<div id="grid">
<div id="pagewidth" >

						<div id="wrapper" class="clearfix">

						<div id="leftcol"  class="rounded" >
 -->

<script language="Javascript" >
$(document).ready(function() {

        $('.page_collapsiblem').collapsible({
                defaultOpen: 'body-section1m',
                cookieName: 'bodym'
        });


        if ( $('#body-section1m').collapsible('collapsed') ) {
                $('#body-section1m').attr("class","page_collapsiblem collapse-close");
        }
        else {
                $('#body-section1m').attr("class","page_collapsiblem collapse-open");
        }

        if ( $('#body-section2m').collapsible('collapsed') ) {
                $('#body-section2m').attr("class","page_collapsiblem collapse-close");
        }
        else {
                $('#body-section2m').attr("class","page_collapsiblem collapse-open");
        }

        if ( $('#body-section3m').collapsible('collapsed') ) {
                $('#body-section3m').attr("class","page_collapsiblem collapse-close");
        }
        else {
                $('#body-section3m').attr("class","page_collapsiblem collapse-open");
        }

        if ( $('#body-section4m').collapsible('collapsed') ) {
                $('#body-section4m').attr("class","page_collapsiblem collapse-close");
        }
        else {
                $('#body-section4m').attr("class","page_collapsiblem collapse-open");
        }
		$('a#ui-id-1').click(function()
		{
			
			$('#multipl_part').css('display','none');
			
		});
		$('a#ui-id-2').click(function()
		{
			$('#signal_parts').css('display','none');
		 
		});
		
		$( document ).on( "click", ".profile_right > .more_p > a", function()
		//$('.profile_right > .more_p > a').bind('click', function() 
		{
			var id = $(this).attr('id');
			var txt = $('#'+id).text();
			var scon  = id.split("_");
			if(txt == '+ More')
			{
				$('#'+id).text('');
				$('#'+id).text('- Less');
				$('.profile_right > .profile_rightcon_'+scon[1]).animate({height:"+=150"},1000);
				
			}
			else if (txt == '- Less')
			{
				$('#'+id).text('');
				$('#'+id).text('+ More');
				$('.profile_right > .profile_rightcon_'+scon[1]).animate({height:"-=150"},1000);
			}
			
		});
		
		

});

</script>

							<div class="tab-content-left">
								<form id="filterFormM" name="signUpForm2"  action="../broadcast/filter" method="post">
									<input type="hidden" id="filterName" name="name" value="" />
									<input type="hidden" id="parttype" name="parttype" value="Multiple" />
									<input type="hidden" id="myconnections" name="myconnections" value="<%=filterParams.myconnections%>" />
									<!--inner-contleftpan start -->
									<div class="btn-group-sidebar">
                    <div class="btn-group btn-group-sm" role="group" aria-label="sidebar button group">
                    <button type="button" class="btn btn-primary" id="filterResultsM">
                      <i class="fa fa-filter" aria-hidden="true"></i> Filter
                    </button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_multiple_part_filter" >
                      <i class="fa fa-floppy-o" aria-hidden="true"></i> Save Filter
                    </button>
                    </div>
                  </div>
                  <!--side-bar-btn-group-->

                  <div class="panel-group">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <a data-toggle="collapse" href="#acc2_collapse_01">
                          Filter Name <span class="icon" aria-hidden="true"></span>
                        </a>
                      </div>
                      <div id="acc2_collapse_01" class="panel-collapse collapse in">
                        <div class="panel-body">
													<g:select class="form-control" optionKey="id" from="${BroadcastFilter.findAllByPersonAndFilterType(session.person,'M')}" optionValue="name" name="filter" onChange="getFilterM(this.value)"  value="${filterid}" noSelection="[null:'Select Filter']"></g:select>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <a data-toggle="collapse" href="#acc2_collapse_02" class="collapsed">
                            Type <span class="icon" aria-hidden="true"></span>
                        </a>
                      </div>
                      <div id="acc2_collapse_02" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="checkbox margin-top-0">
                            <label>
															<input type="hidden" name="_wtb" />
															<input type="checkbox" name="wtb" value="WTB" id="wtb" ${(broadcastType?.contains("WTB"))?"checked":""} />WTB
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
															<input type="hidden" name="_wts" />
															<input type="checkbox" name="wts" value="WTS" id="wts" ${(broadcastType?.contains("WTS"))?"checked":""} />WTS
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!--panel-->
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <a data-toggle="collapse" href="#acc2_collapse_03" class="collapsed">KEYWORD <span class="icon" aria-hidden="true"></span></a>
                      </div>
                      <div id="acc2_collapse_03" class="panel-collapse collapse">
                        <div class="panel-body">
                          <input type="text" name="keyword" class="form-control" value="${filterParams?.keyword}" id="keyword" placeholder="search">
                        </div>
                      </div>
                    </div>
										
										<%

											boolean allcomp = ((companyType?.equals("all")) || (companyType == null))

											boolean myvendors = (companyType?.equals("myVendors"))

											boolean myconnections = (companyType?.equals("myConnections"))

											%>


										<g:if test="${filterParams?.myconnections == false}" >
                    <!--panel-->
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <a data-toggle="collapse" href="#acc2_collapse_04" class="collapsed">
                            Companies <span class="icon" aria-hidden="true"></span>
                        </a>
                      </div>
                      <div id="acc2_collapse_04" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="radio">
                            <label>
															<g:radio name="companyType" value="myVendors" checked="${myvendors}" /> My Vendors
                            </label>
                          </div>
                          <div class="radio">
                            <label>
															<g:radio name="companyType" value="all" checked="${allcomp}" /> All/ Selected Companies
                            </label>
                          </div>
													<input type="text" class="form-control" id="company-filterinput-multi" placeholder="Search by Company">
                          <ul class="checkbox-list">
														<g:each in="${companyMap}">
                            <li>
                              <label for="company-${it.value.id}"><input type="checkbox"  name="company" value="${it.value.id}"    id="company-${it.value.id}">${it.value.name} (${it.value.hit})</label>
                            </li>
														</g:each>
                          </ul>
                        </div>
                      </div>
                    </div>
										</g:if>
										
										<%

										boolean allcntry = ((countryType?.equals("all")) || (countryType == null))

										boolean mycntry = (countryType?.equals("myCountries")) 
										
										%>
										<g:if test="${filterParams?.myconnections == false}" >
                    <!--panel-->
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <a data-toggle="collapse" href="#acc2_collapse_05" class="collapsed">Countries <span class="icon" aria-hidden="true"></span></a>
                      </div>
                      <div id="acc2_collapse_05" class="panel-collapse collapse">
                        <div class="panel-body" style="overflow: auto; height: 180px;">
                          <input type="text" class="form-control" id="country-filterinput-multi" placeholder="Search countries">
                          <ul class="checkbox-list">
														<g:each in="${countryMap}">
                            <li>
                              <label for="country-${it.value.id}"><input type="checkbox"  name="country" value="${it.value.id}"    id="country-${it.value.id}">${it.value.name} (${it.value.hit})</label>
                            </li>
														</g:each>
                            
                          </ul>
                        </div>
                      </div>
                    </div>
                    <!--panel-->
										</g:if>
                  </div>
                  <!--panel-group-->
								</form>
							</div>
							
								<div class="modal fade broadcasts-popup" id="modal_multiple_part_filter" tabindex="-1" role="dialog" aria-labelledby="modal_single_part_broadcastsLabel" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<h4 class="modal-title" id="modal_single_part_broadcastsLabel">Create New Reminder</h4>
											</div>
											<!--modal-header-->
											
											<div class="modal-body">
												<div class="form-group">
													<label for="part" class="col-sm-2 control-label">Filter Name:</label>
													<div class="col-sm-8">
														<input type="text" class="form-control" name="name" id="filter_mname" >
													</div>
												</div>
											</div>
											
											<div class="modal-footer">
												<p class="ajax-resp" align="left"></p>
												<div class="form-group">
													<div class="col-sm-offset-2 col-sm-8">
														 <button type="button" id="save-multiple-filter" class="btn btn-primary">Save Filter</button>
														 <button type="button" class="btn btn-primary modal-dismiss" data-dismiss="modal">Cancel</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<script>
									
									
									$('#save-multiple-filter').click(function(){
										var form = $('#filterForm');
										$.ajax({
											type: 'POST',
											url: '../broadcastFltr/save',
											data: form.serialize() + "&name=" + $('#filter_mname').val(),
											success: function(response){
												
												alert(response.msg);
												$('.modal-dismiss').click();
											}
										})
									});
								</script>
			
<form id="filterFormM" name="signUpForm2"  action="../broadcast/filter" method="post" style="display:none">
		<input type="hidden" id="filterName" name="name" value="" />
		<input type="hidden" id="parttype" name="parttype" value="Multiple" />
		<input type="hidden" id="myconnections" name="myconnections" value="<%=filterParams.myconnections%>" />
		
			<div id="inner_for_multiple" class="inner-contleftpan">


				<!--inner-contleftToppanel start -->

				<div class="inner-contleftToppanel">

				

					<ul>

					<li>
						<div class="button h">
							<!--<a href="#" id="filterResultsM"><span>Filter</span></a>-->
							<button type="button" id="filterResultsM" class="btn btn-broadcast btn-broadcast-filter">Filter</button>
						</div>
					</li>

					<li>
						<div class="button">
							<!--<a href="#"  id="saveFilterM"   ><span>Save Filter</span></a>-->
							<button type="button" id="saveFilterM" class="btn btn-broadcast btn-broadcast-filter">Save Filter</button>
						</div>
					</li>

					</ul>

					

					<div class="spacer"></div>

					

				<div id="multipl_part" class="filter-penal">
                                <!-- greenarea starts-->
                                        <div class="greenarea">
                                                <h3 class="page_collapsiblem collapse-open" id="body-section1m">Filter Name:<span></span></h3>
                        <div class="container" style="display: none;">
                               <div class="content">
                                                     <div class="greenbox">
                                                                 <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                          <tbody><tr>
                                                                                <td colspan="2" align="left" valign="top">
                                                                                <div class="ddOutOfVision" style="height:0px;overflow:hidden;position:absolute;" id="filterddM_msddHolder">
<div id="filterselectM"><g:select id="filterddM" style="width:96%;"    optionKey="id" from="${BroadcastFilter.findAllByPersonAndFilterType(session.person,'M')}" optionValue="name" name="filter" onChange="getFilterM(this.value)"  value="${filterid}" noSelection="[null:'Select Filter']"  ></g:select></div>


                                                                                </div>
<div id="filterddM_msdd" class="dd2" style="width: 193px;"><div id="filterddM_title" class="ddTitle"><span id="filterddM_arrow" class="arrow" style="background-position: 0px 0px;"></span><span class="ddTitleText" id="filterddM_titletext"><span class="ddTitleText">WTB_My Inventory</span></span></div><div id="filterddM_child" class="ddChild" style="width: 191px;"><a href="javascript:void(0);" class="selected enabled" style="[object CSSStyleDeclaration]" id="filterddM_msa_0"><span class="ddTitleText">WTB_My Inventory</span></a><a href="javascript:void(0);" class="enabled" style="[object CSSStyleDeclaration]" id="filterddM_msa_1"><span class="ddTitleText">Demo 1</span></a></div></div>

                                                                        </td>
                                                                          </tr>
                                                                          <tr>
                                                                                <td colspan="2" align="left" valign="top" height="10"></td>
                                                                          </tr>
                                                                          <tr>
                                                                                <td align="left" valign="top" width="30%">
<input type="hidden" name="_wtb" /><p> <input type="checkbox" safari="1" name="wtb" value="WTB" ${(broadcastType?.contains("WTB"))?"checked":""} id="wtb"   /><label for="wtb">WTB</label></p>
                                                                                </td>
                                                                                <td align="left" valign="top" width="50%">
<input type="hidden" name="_wts" /> <p><input type="checkbox" safari="1"  name="wts" value="WTS" ${(broadcastType?.contains("WTS"))?"checked":""} id="wts"   /><label for="wtb">WTS</label></p>
                                                                                </td>

                                                                         </tr>
                                                                        </tbody></table>
                                                          </div>
                                                        </div>
                                                </div>
                                                </div>
                                    <!-- greenarea ends-->


                          <!-- greenarea starts-->
                                        <div class="greenarea">
                                                  <h3 class="page_collapsiblem collapse-open" id="body-section2m">Keyword:<span></span></h3>
                                                  <div class="container" style="display: none;">
                               			<div class="content">
                                                    <div class="greenbox">
                                                         <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                  <tbody><tr>
                                                                        <td colspan="2" align="left" valign="top">
                                                                        <g:textArea class="inptbox"  name="keyword" value="${filterParams?.keyword}" />
                                                                    </td>
                                                                  </tr>
                                                                </tbody></table>
                                                  </div>
                                                   </div>
                                                  </div>
                                                </div>
                           <!-- greenarea ends-->


<g:if test="${filterParams?.myconnections == false}" >
					


                               <!-- greenarea starts-->
                                        <div class="greenarea">
                                                  <h3 class="page_collapsiblem collapse-open" id="body-section3m">Companies<span></span></h3>
                                                  <div class="container" style="display: none;">
                                <div class="content">
                                                    <div class="greenbox">
                                                         <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                             <tbody><tr>
                                                                        <td colspan="2" align="left" valign="top">
                                                                        <div class="defaultP">
                                                                                <div class="ez-radio">
                                                                                <g:radio name="companyType" value="myVendors"  checked="${myvendors}" class="ez-hide"  /></div><label for="vendors">My Vendors</label>
                                                                        </div>
                                                                        </td>

                                                                  </tr>

                                                                  <tr>
                                                                        <td colspan="2" align="left" valign="top">
                                                                        <div class="defaultP">
                                                                                <div class="ez-radio">
                                                                                <g:radio name="companyType" value="all" checked="${allcomp}" class="ez-hide" /></div><label for="scompanies">All/ Selected Companies</label>
                                                                        </div>
                                                                        </td>

                                                                  </tr>

                                                              <tr>
                                                                       <td colspan="2" align="left" valign="top" height="10"></td>
                                                                  </tr>
                                                                  <tr>
                                                                        <td colspan="2" align="left" valign="top">
																																				<div class="filterinput-wrap">
																																					<i class="fa fa-search fa-lg"></i>
																																					<span style="display:none"><i class="fa fa-times-circle fa-lg"></i></span>
																																					<input type="text" class="filterlist" id="company-filterinput-multi" placeholder="Search By Company">
																																					<ul class="unstyled bcast-filter company">
																																						<g:each in="${companyMap}">
																																						<li><input type="checkbox" id="company-${it.value.id}" /><label for="company-${it.value.id}">${it.value.name} (${it.value.hit})</label></li>
																																						</g:each>
																																					</ul>
																																				</div>
																																				<!--
                                                                        <select name="company" id="company" style="width:96%;"  multiple="multiple"    tabindex="1">
<g:each in="${companyMap}">
                                                                          <option name="one" value="${it.value.id}" ${companySel?.contains(it.value.name)?"selected='selected'":""} >${it.value.name} (${it.value.hit})</option>
</g:each>
                                                                        </select>-->
                                                                </td>
                                                                  </tr>
                                                                </tbody></table>
                                                  </div>
                                                    </div>
                                                  </div>
                                                </div>
                                    <!-- greenarea ends-->



</g:if>

					

		


<g:if test="${filterParams?.myconnections == false}" >

                                <!-- greenarea starts-->
                                        <div class="greenarea">
                                                  <h3 class="page_collapsiblem collapse-open" id="body-section4m">Countries<span></span></h3>
                                                   <div class="container" style="display: none;">
                                <div class="content">
                                                     <div class="greenbox">
                                                         <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                  <tbody>
	<!-- 
								<tr>
                                                                        <td colspan="2" align="left" valign="top">
                                                                        <div class="defaultP">
                                                                                <div class="ez-radio">
                                                                                <g:radio name="countryType" value="myCountries" checked="${mycntry}" id="mcountries" class="ez-hide"  />
                                                                                </div>
                                                                                <label for="mcountries">My Countries</label>
                                                                        </div>
                                                                        </td>

                                                                  </tr>
                                                                  <tr>
                                                                        <td colspan="2" align="left" valign="top">
                                                                        <div class="defaultP">
                                                                                <div class="ez-radio">
                                                                                <g:radio name="countryType" value="all" checked="${allcntry}" id="ascompanies" class="ez-hide" />
                                                                                </div>
                                                                                <label for="ascompanies">All/ Selected Countries</label>
                                                                        </div>
                                                                        </td>

                                                                  </tr>
			-->
                                                              <tr>
                                                                        <td colspan="2" align="left" valign="top" height="10"></td>
                                                                  </tr>
                                                                  <tr>
                                                                        <td colspan="2" align="left" valign="top">
																																				
																																				<div class="filterinput-wrap">
																																					<i class="fa fa-search fa-lg"></i>
																																					<span style="display:none"><i class="fa fa-times-circle fa-lg"></i></span>
																																					<input type="text" class="filterlist" id="country-filterinput-multi" placeholder="Search By Country">
																																					<ul class="unstyled bcast-filter country">
																																						<g:each in="${countryMap}">
																																						<li><input type="checkbox" id="country-${it.value.id}" /><label for="country-${it.value.id}">${it.value.name} (${it.value.hit})</label></li>
																																						</g:each>
																																					</ul>
																																				</div>
																																				
																																				<!--
                                                                        <select name="country" id="country" multiple="multiple"  style="width:96%;" tabindex="1">
<g:each in="${countryMap}">
        <option  value="${it.value.id}" ${countrySel?.contains(it.value.name)?"selected='selected'":""} >${it.value.name} (${it.value.hit})</option>
</g:each>
                                                                        </select>-->


                                                                   </td>
                                                                  </tr>
                                                                </tbody></table>
                                                  </div>
                                                    </div>
                                                  </div>
                                                </div>

					</div>
                                    <!-- greenarea ends-->

</g:if>

				</div>
				<!--inner-contleftToppanel end -->
				
			</div>
			<!--inner-contleftpan end -->

</form>

<g:render template="broadcastTableMultiplePart" model="${[broadcastList:broadcastList,countryMap:countryMap,companyMap:companyMap,filterParams:flash.filterParams]}" />

<script>

function hide_and_show1()
	{
		 
		if($('#hide_and_show_filter_active_multiple').hasClass('hide_and_show_filter_active'))
		{
			  
			   $('#inner_for_multiple').animate({width:"-=17%"},1000);
			   $('#tableGridM div.inner-contrightpan').animate({width:"+=17%"},1000);
			   $('#hide_and_show_filter_active_multiple').removeClass('hide_and_show_filter_active');
			   $('#hide_and_show_filter_active_multiple').addClass('hide_and_show_filter_inactive');
			   $('#multipl_part').css('display','none');
			 
			
		}
		else
		{
				$('#inner_for_multiple').animate({width:"+=17%"},1000);
				$('#tableGridM div.inner-contrightpan').animate({width:"-=17%"},1000);
				$('#hide_and_show_filter_active_multiple').removeClass('hide_and_show_filter_inactive');
				$('#hide_and_show_filter_active_multiple').addClass('hide_and_show_filter_active');
				$('#multipl_part').css('display','block');
		 
		}
		  
	}

</script>
<script>
	$(document).ready(function(){
		setTimeout(function(){
			$('#multipl_part h3').each(function(){
				
				if($(this).next().is(':visible')){
					$(this).find('span').html('<i class="fa fa-minus fa-lg"></i>');
				} else {
					$(this).find('span').html('<i class="fa fa-plus fa-lg"></i>');
				}
				// $(this).find('span').html('<i class="fa fa-plus fa-lg"></i>');
			});
		}, 500);
		
		$('ul.bcast-filter').each(function(){
			if($(this).find('li').length > 5) {
				$(this).css({
					'height' : '130px',
					'overflow-y' : 'scroll'
				});
			}
		});
		
		
		// $('#signal_parts h3.collapse-open').each(function(){
			// $(this).find('span').html('<i class="fa fa-minus fa-lg"></i>');
		// });
	});
	
	$('#company-filterinput-multi').filterList();
	$('#country-filterinput-multi').filterList();
	
	
	$('.filterinput-wrap input').bind('keyup', function(){
		
		if($(this).val() == ''){
			
			$(this).parent().find('i.fa-search').show();
			$(this).parent().find('span').hide();
			
		} else {
			
			$(this).parent().find('i.fa-search').hide();
			$(this).parent().find('span').show();
			
		}
		
	});
	
	$('.filterinput-wrap .fa-times-circle').click(function(){
		
		$(this).parent().parent().find('i.fa-search').show();
		$(this).parent().parent().find('input').val('');
		$(this).parent().parent().find('ul.bcast-filter li').show();
		$(this).parent().hide();
		
	});
	
	$('#multipl_part h3').click(function(){
			
			if($(this).find('span i').hasClass('fa-plus')){
				
				$(this).find('span i').removeClass('fa-plus').addClass('fa-minus');
			} else {
				$(this).find('span i').removeClass('fa-minus').addClass('fa-plus');
			}			
	});
</script>
