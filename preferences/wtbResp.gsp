<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.Continent" %>
<%@page import="test2.Country" %>

<html> 
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="pz" />
        <title>WTB Preferences</title> 
        <g:javascript src="preferences.js" />				



    </head>
    <body> 
				
				<!--[if !IE]>start main info<![endif]-->
				<div id="main_info">
					<div id="main_info_bottom">
						
				<!--[if !IE]>start sec info<![endif]-->
						<div class="title_wrapper">
							<h3>Alert Settings</h3>
						</div>
						
						
						<!--[if !IE]>start product page<![endif]-->
						<div id="product_page">
							<!--[if !IE]>start product content<![endif]-->
<g:formRemote name="myform" url="[controller:'preferences',action:'saveWTBResp']" onSuccess="fadeit(e)" onFailure="fadeit(e)" >

								<div id="product_content">
									<h4>WTB Broadcasts</h4>
								<!--[if !IE]>start forms<![endif]-->


															

								<div class="modules">
								<div class="row">
									<div >
										<g:radioGroup name="wtbResp_from" value="${comm?.wtbResp_from}" labels="['All Companies','Only Companies from MyCountries','Only from MyVendors']" values="['a','c','v']" >
										<p>${it.label} ${it.radio}</p>
										</g:radioGroup>


									</div>
								</div>
								<div class="row">
									<br/>
								</div>




										<!--[if !IE]>start module<![endif]-->
										<div class="module">
											<div class="module_top">
												<h5>Alerts for WTB Broadcasts from My Vendors</h5>
												<a href="#" class="edit_module help_module">Help</a>
											</div>
											<div class="module_bottom">
												<div class="module_options">
													<div class="module_options_inner">
													<div class="module_option">
														<dl>
															<dt>General</dt>
															<dd><g:checkBox name="basic_respwtb.sms" value="${comm?.basic_respwtb?.sms}" />SMS</dd>
															<dd><g:checkBox name="basic_respwtb.email"  value="${comm?.basic_respwtb?.email}" />Email</dd>
														</dl>
													</div>
													<div class="module_option">
														<dl>
															<dt>Instant Messenger</dt>
															<dd><g:checkBox name="basic_respwtb.gtalk"  value="${comm?.basic_respwtb?.gtalk}" />Gtalk</dd>
															<dd><g:checkBox name="basic_respwtb.yahoo"  value="${comm?.basic_respwtb?.yahoo}" />Yahoo</dd>
															<dd><g:checkBox name="basic_respwtb.msn"  value="${comm?.basic_respwtb?.msn}" />MSN</dd>
															<dd><g:checkBox name="basic_respwtb.aol"  value="${comm?.basic_respwtb?.aol}" />AIM</dd>
														</dl>
													</div>
													</div>
												</div>
											</div>
										</div>

								<div class="row">
									<br/>
									<br/>
									<br/>
								</div>






	
								

								
							</div> <!-- end modules -->						
						
				</div> <!-- product_content -->

                                                    </div>  <!-- product gallery -->

								<div class="row">
									<div class="inputs">
										<span class="button blue_button search_button"><span><span><em>Save</em></span></span><input name="" type="submit" /></span>
									</div>
								</div>
						 
				</div> <!-- product_content -->





</g:formRemote >


			</div> <!-- product_page -->
		</div> <!-- main_info_bottom -->
	</div> <!-- main_info -->
</body>
</html>

