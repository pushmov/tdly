<%@ page import="test2.BroadcastFilter" %>
<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.Continent" %>
<%@page import="test2.Manufacturer" %>
<%@page import="test2.Company" %>
<%@page import="test2.Country" %>
<%@page import="test2.Cond" %>
<%
boolean allcomp = ((broadcastFilter?.companyType?.equals("all")) || (broadcastFilter?.companyType == null))

boolean myvendors = broadcastFilter?.myVendors
boolean myconnections = broadcastFilter?.myConnections
%>

							<g:form name="broadcastFilterForm" class="form-horizontal" >
								<g:hiddenField name="id" value="${broadcastFilter?.id}" />
	
								<div class="modal-body">
									<!--form-group-->
                  <div class="form-group">
                    <label for="s_p_part" class="col-sm-2 control-label">Filter Name:</label>
                    <div class="col-sm-8">
											<g:textField name="name" value="${broadcastFilter?.name}" class="form-control" />
                    </div>
                  </div>
									<div class="form-group">
                    <label for="s_p_type" class="col-sm-2 control-label">Broadcast Type:</label>
                    <div class="col-sm-8">
                      <label><g:checkBox name="wtb" value="${broadcastFilter?.broadcasttype?.contains("WTB")}" />WTB</label>
											<label><g:checkBox name="wts" value="${broadcastFilter?.broadcasttype?.contains("WTS")}" />WTS</label>
                    </div>
                  </div>
									
									<div class="form-group">
                    <label for="s_p_part" class="col-sm-2 control-label">Keyword:</label>
                    <div class="col-sm-8">
											<g:textField name="keyword" value="${broadcastFilter?.keyword?.equals("all")?"":broadcastFilter?.keyword}" class="form-control" />
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="s_p_part" class="col-sm-2 control-label">Condition:</label>
                    <div class="col-sm-8">
                      <input type="text" placeholder="Search By Condition" id="conditionS-filterinput" class="form-control">
                      <ul class="unstyled bcast-filter filter-list conditionS">
                          <li><input type="checkbox" id="conditionS-1"><label for="conditionS-1">New (85)</label></li>
                          <li><input type="checkbox" id="conditionS-3"><label for="conditionS-3">Used (1)</label></li>
                          <li><input type="checkbox" id="conditionS-2"><label for="conditionS-2">Refurbished (20)</label></li>
                          <li><input type="checkbox" id="conditionS-4"><label for="conditionS-4">Original packaging (3)</label></li>
                          <li><input type="checkbox" id="conditionS-"><label for="conditionS-"> (16)</label></li>
                      </ul>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="s_p_part" class="col-sm-2 control-label">Partlist:</label>
                    <div class="col-sm-8">
											<g:select from="${['MyInventory','MyWatchlist']}" noSelection="${['All':'All']}" name="partlist" value="${broadcastFilter?.partlist}" class="form-control"></g:select>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="s_p_part" class="col-sm-2 control-label">Manufacturer:</label>
                    <div class="col-sm-8">
                        <input type="text" placeholder="Search By Manufacturer" id="manufacturer-filterinput" class="form-control">
                        <ul class="unstyled bcast-filter filter-list manufacturer">
                          <li><input type="checkbox" id="manufacturer-5"><label for="manufacturer-5">ibm</label></li>
                          <li><input type="checkbox" id="manufacturer-4"><label for="manufacturer-4">Fujitsu</label></li>
                          <li><input type="checkbox" id="manufacturer-1"><label for="manufacturer-1">Dell</label></li>
                          <li><input type="checkbox" id="manufacturer-6"><label for="manufacturer-6">Cisco</label></li>
                          <li><input type="checkbox" id="manufacturer-2"><label for="manufacturer-2">Texas Instruments</label></li>
                          <li><input type="checkbox" id="manufacturer-3"><label for="manufacturer-3">Samsung</label></li>
                          <li><input type="checkbox" id="manufacturer-8"><label for="manufacturer-8">Apple</label></li>
                          <li><input type="checkbox" id="manufacturer-9"><label for="manufacturer-9">HP</label></li>
                          <li><input type="checkbox" id="manufacturer-7"><label for="manufacturer-7">Juniper</label></li>
                          <li><input type="checkbox" id="manufacturer-12"><label for="manufacturer-12">ibmww</label></li>
                        </ul>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="s_p_part" class="col-sm-2 control-label">Companies:</label>
                    <div class="col-sm-8">
                      <div>
                        <g:radio name="companyType" checked="${myvendors}" value="myVendors" id="vendors" /><label for="vendors">My Vendors</label>	
                        <g:radio name="companyType" checked="${!myvendors}" value="all"  id="scompanies" /><label for="scompanies">All/ Selected Companies</label>
                      </div>
                      <div>
                        <input type="text" placeholder="Search By Company" id="company-filterinput" class="form-control">
                        <ul class="unstyled bcast-filter filter-list company">
                          <li><input type="checkbox" id="company-2"><label for="company-2">ICC Northern UK Ltd</label></li>
                          <li><input type="checkbox" id="company-3"><label for="company-3">e</label></li>
                          <li><input type="checkbox" id="company-4"><label for="company-4">Boston Inc</label></li>
                          <li><input type="checkbox" id="company-5"><label for="company-5">pioneer</label></li>
                          <li><input type="checkbox" id="company-6"><label for="company-6">ee</label></li>
                          <li><input type="checkbox" id="company-7"><label for="company-7">Ash inc</label></li>
                          <li><input type="checkbox" id="company-8"><label for="company-8">Aamir inc</label></li>
                          <li><input type="checkbox" id="company-9"><label for="company-9">Hrithik inc</label></li>
                          <li><input type="checkbox" id="company-10"><label for="company-10">Deol inc</label></li>
                          <li><input type="checkbox" id="company-11"><label for="company-11">Saif inc</label></li>
                          <li><input type="checkbox" id="company-12"><label for="company-12">Kareena inc</label></li>
                          <li><input type="checkbox" id="company-13"><label for="company-13">Karishma inc</label></li>
                          <li><input type="checkbox" id="company-14"><label for="company-14">Anil inc</label></li>
                          <li><input type="checkbox" id="company-15"><label for="company-15">Vidya inc</label></li>
                          <li><input type="checkbox" id="company-16"><label for="company-16">Geeta inc</label></li>
                          <li><input type="checkbox" id="company-17"><label for="company-17">Seema inc</label></li>
                          <li><input type="checkbox" id="company-18"><label for="company-18">Ranjini inc</label></li>
                          <li><input type="checkbox" id="company-19"><label for="company-19">Priya inc</label></li>
                          <li><input type="checkbox" id="company-20"><label for="company-20">sunil inc</label></li>
                          <li><input type="checkbox" id="company-21"><label for="company-21">kapil inc</label></li>
                          <li><input type="checkbox" id="company-24"><label for="company-24">huhu</label></li>
                          <li><input type="checkbox" id="company-26"><label for="company-26">ede</label></li>
                          <li><input type="checkbox" id="company-28"><label for="company-28">frfr</label></li>
                          <li><input type="checkbox" id="company-30"><label for="company-30">ded</label></li>
                          <li><input type="checkbox" id="company-31"><label for="company-31">dede</label></li>
                          <li><input type="checkbox" id="company-32"><label for="company-32">dede12121</label></li>
                          <li><input type="checkbox" id="company-33"><label for="company-33">a</label></li>
                          <li><input type="checkbox" id="company-34"><label for="company-34">test1</label></li>
                          <li><input type="checkbox" id="company-35"><label for="company-35">test1</label></li>
                          <li><input type="checkbox" id="company-36"><label for="company-36">uhu</label></li>
                          <li><input type="checkbox" id="company-37"><label for="company-37">uhuh</label></li>
                          <li><input type="checkbox" id="company-38"><label for="company-38">huh</label></li>
                          <li><input type="checkbox" id="company-39"><label for="company-39">dede</label></li>
                          <li><input type="checkbox" id="company-99"><label for="company-99">AFLAC</label></li>
                          <li><input type="checkbox" id="company-100"><label for="company-100">Applied Industrial Technologies</label></li>
                          <li><input type="checkbox" id="company-101"><label for="company-101">Charter Communications</label></li>
                          <li><input type="checkbox" id="company-102"><label for="company-102">Coca-Cola</label></li>
                          <li><input type="checkbox" id="company-103"><label for="company-103">AFLAC</label></li>
                          <li><input type="checkbox" id="company-104"><label for="company-104">qeindia</label></li>
                          <li><input type="checkbox" id="company-105"><label for="company-105">qeindia</label></li>
                          <li><input type="checkbox" id="company-106"><label for="company-106">qeindia</label></li>
                          <li><input type="checkbox" id="company-107"><label for="company-107">qeindia</label></li>
                          <li><input type="checkbox" id="company-108"><label for="company-108">qeindia</label></li>
                          <li><input type="checkbox" id="company-109"><label for="company-109">qeindia</label></li>
                          <li><input type="checkbox" id="company-110"><label for="company-110">qeindia</label></li>
                          <li><input type="checkbox" id="company-111"><label for="company-111">qeindia</label></li>
                          <li><input type="checkbox" id="company-112"><label for="company-112">qeindia</label></li>
                          <li><input type="checkbox" id="company-113"><label for="company-113">de</label></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="s_p_part" class="col-sm-2 control-label">Countries:</label>
                    <div class="col-sm-8">
											<input type="text" placeholder="Search By Country" id="country-filterinput" class="form-control">
                      <ul class="unstyled bcast-filter filter-list country">
                          <li><input type="checkbox" id="country-194"><label for="country-194">Australia</label></li>
                          <li><input type="checkbox" id="country-192"><label for="country-192">United States</label></li>
                      </ul>
                    </div>
                  </div>
                  
                <div class="modal-footer">
                    <p align="left" class="ajax-resp"></p>
                    <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-8">
                         <button class="btn btn-primary btn-filter-save" type="button">Save</button>
                         <button data-dismiss="modal" class="btn btn-primary btn-filter-cancel" type="button">Cancel</button>
                      </div>
                    </div>
                </div>
									
								</div>
	
            </g:form>

<!-- filter dialog end -->


<script>
	$('#conditionS-filterinput').filterList();
	$('#manufacturer-filterinput').filterList();
	$('#company-filterinput').filterList();
	$('#country-filterinput').filterList();
	
	$('.filterinput-wrap input').bind('keyup', function(){
		
		if($(this).val() == ''){
			
			$(this).parent().find('i.fa-search').show();
			$(this).parent().find('span').hide();
			
		} else {
			
			$(this).parent().find('i.fa-search').hide();
			$(this).parent().find('span').show();
			
		}
		
	});
	
	$('.filterinput-wrap .fa-times-circle').click(function(){
		
		$(this).parent().parent().find('i.fa-search').show();
		$(this).parent().parent().find('input').val('');
		$(this).parent().parent().find('ul.bcast-filter li').show();
		$(this).parent().hide();
		
	});
	
	$('.btn-filter-save').click(function(){
		var form = $(this).closest('form');
		$.ajax({
            type: "POST",
            url: "../broadcastFltr/save",
            data: form.serialize(),
            error: function(data, textStatus, jqXHR) {
				alert("Error: " + textStatus);
            },
            success: function(data, textStatus, jqXHR) {
				location.reload( true);
            }
        });
        return false;
	});
	
</script>