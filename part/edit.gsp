

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Edit Part</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Part List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New Part</g:link></span>
        </div>
        <div class="body">
            <h1>Edit Part</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${partInstance}">
            <div class="errors">
                <g:renderErrors bean="${partInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <input type="hidden" name="id" value="${partInstance?.id}" />
                <input type="hidden" name="version" value="${partInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="partno">Partno:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:partInstance,field:'partno','errors')}">
                                    <input type="text" id="partno" name="partno" value="${fieldValue(bean:partInstance,field:'partno')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="manufacturer">Manufacturer:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:partInstance,field:'manufacturer','errors')}">
                                    <g:select optionKey="id" from="${Manufacturer.list()}" name="manufacturer.id" value="${partInstance?.manufacturer?.id}" ></g:select>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="watchlist">Watchlist:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:partInstance,field:'watchlist','errors')}">
                                    
<ul>
<g:each var="w" in="${partInstance?.watchlist?}">
    <li><g:link controller="watchlist" action="show" id="${w.id}">${w?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="watchlist" params="['part.id':partInstance?.id]" action="create">Add Watchlist</g:link>

                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" value="Update" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
