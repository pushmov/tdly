

<g:form name="editInventory" class="form-horizontal">
	<div class="modal-body">
		<g:hasErrors bean="${inventory}">
			<div class="errors">
			<g:renderErrors bean="${inventory}" as="list" />
			</div>
		</g:hasErrors>
		<g:hasErrors bean="${inventory?.item}">
			<div class="errors">
				<g:renderErrors bean="${inventory?.item}" as="list" />
			</div>
		</g:hasErrors>
		
		<g:hiddenField name="id" value="${inventory?.id}" />
		<g:hiddenField name="mfgid" value="" />
		<g:render template="/tradeunitinvedit" var="item" bean="${inventory?.item}"  />
		
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-8">
				<button type="button" id="save" class="btn btn-primary">Save</button>
				<button type="button" data-dismiss="modal" class="btn btn-primary">Cancel</button>
			</div>
		</div>
	</div>
	
	
</g:form>

<script>
	$('#save').click(function(){
		var id = "";
		var formData = $('#editInventory').serializeArray();
			$.each(formData, function(i, field) {
				if(field.name == "id" ){
					id = field.value;
				}
			});
			
			$.ajax({
				type: "POST",
				url: "../inventory/update",
				cache: false,
				data: $('#editInventory').serialize(),
				error: function(data, textStatus, jqXHR) {
					alert("Error: " + textStatus);
				},
				
				success: function(data, textStatus, jqXHR) {
					if(data.indexOf("errors") != -1) {
						$('#editInventory').replaceWith(data);
					} else {
						$("tr[id='" + id + "']").replaceWith(data);
						$('button[data-dismiss="modal"]').click();
						$('#modal_watchlist_success #response').html('Part Changes saved.');
						$('button[data-target="#modal_watchlist_success"]').click();
					}
				}
			});
	});
</script>