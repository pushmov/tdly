

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Show Inventory</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Inventory List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New Inventory</g:link></span>
        </div>
        <div class="body">
            <h1>Show Inventory</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>

                    
                        <tr class="prop">
                            <td valign="top" class="name">Id:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:inventory, field:'id')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Visible:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:inventory, field:'visible')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Myvendorsonly:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:inventory, field:'myvendorsonly')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Searches:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:inventory, field:'searches')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Hits:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:inventory, field:'hits')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Wtb:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:inventory, field:'wtb')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Wts:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:inventory, field:'wts')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Company:</td>
                            
                            <td valign="top" class="value"><g:link controller="company" action="show" id="${inventory?.company?.id}">${inventory?.company?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Created:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:inventory, field:'dateCreated')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Createdby:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:inventory, field:'createdby')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Item:</td>
                            
                            <td valign="top" class="value"><g:link controller="tradeUnit" action="show" id="${inventory?.item?.id}">${inventory?.item?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Lastmod:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:inventory, field:'lastUpdated')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Lastmodby:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:inventory, field:'lastmodby')}</td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <input type="hidden" name="id" value="${inventory?.id}" />
                    <span class="button"><g:actionSubmit class="edit" value="Edit" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
