<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta name="layout" content="pz" />

<title>Admin Panel</title>
<link media="screen" rel="stylesheet" type="text/css" href="css/admin.css"  />
<!--[if lte IE 6]><link media="screen" rel="stylesheet" type="text/css" href="css/ie.css" /><![endif]-->

</head>

				<!--[if !IE]>start main info<![endif]-->
				<div id="main_info">
					<div id="main_info_bottom">
						<div class="title_wrapper">
							
							<ul class="search_tabs">
								<li><g:link url="[action:'list']"   ><span><span>List</span></span></g:link></li>
								<li><g:link  id="selected_search_tab"   url="[action:'create']"   ><span><span>Add</span></span></g:link></li>
								<li><g:link   url="[action:'searchinit']"   ><span><span>Search</span></span></g:link></li>
								<li><g:link   url="[action:'initupload']"   ><span><span>Upload</span></span></g:link></li>
								<li><g:link   url="[action:'watchlist']"   ><span><span>Watchlist</span></span></g:link></li>
							</ul>
						</div>

						
				            <g:form action="save" method="post" class="search_form" >



							<!--[if !IE]>start fieldset<![endif]-->
							<fieldset>
								<!--[if !IE]>start forms<![endif]-->
								<div class="forms">
								<h4>Add Part</h4>

								<!--[if !IE]>start row<![endif]-->
								<div class="row">
								</div>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${inventory}">
            <div class="errors">
                <g:renderErrors bean="${inventory}" as="list" />
            </div>
            </g:hasErrors>


					    <g:render template="/tradeunit" var="item" bean="${inventory.item}" />


								<div class="row">
									<label>Visible:</label>
									<div class="inputs">
										<span><g:checkBox name="visible" value="${inventory?.visible}" ></g:checkBox>
</span>
									</div>
								</div>
								<div class="row">
									<label>Myvendorsonly:</label>
									<div class="inputs">
										<span><g:checkBox name="myvendorsonly" value="${inventory?.myvendorsonly}" ></g:checkBox>
</span>
									</div>
								</div>
								<div class="row">
									<div class="inputs">
										<span class="button blue_button search_button"><span><span><em>Save</em></span></span><input name="" type="submit" /></span>
									</div>
								</div>

								<!--[if !IE]>end row<![endif]-->
								
								</div>
								<!--[if !IE]>end forms<![endif]-->
									
								
								<!--[if !IE]>start tooltip<![endif]-->
								<br><br><br><br><br>
								<div class="tooltip">
									<div class="tooltip_top">
										<div class="tooltip_bottom">
											<span class="pointer"></span>
											<p class="first">
												Actual search results wouldn't have tabs, but this is a template after all. Enter Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Pellentesque lobortis lacus euismod urna. Praesent sed tortor. Suspendisse in lacus facilisis tellus tempus venenatis. Vivamus dolor arcu, ultrices in, 
											</p>
											<p>
												<strong>*Note :</strong> Clicking on the "More Advanced Geographic Filters"  button will open an ajax frame for further filtering so information is not lost and there are no popups used, this way. 
											</p>
										</div>
									</div>
								</div>
								<!--[if !IE]>end tooltip<![endif]-->
								
								
								
							</fieldset>
							
							
						</g:form>
						
					
					
					</div>
				</div>
				<!--[if !IE]>end main info<![endif]-->
				
				
				
				
			</div>
			<!--[if !IE]>end page<![endif]-->
			
			
			
		</div>
	</div>
<!--[if !IE]>end wrapper<![endif]-->

</body>
</html>
