<script language="Javascript" >
$(document).ready(function() {


    $('#dialog-responseSave').dialog({
        autoOpen: false,
        height: 250,
        width: 350,
        modal: true,
        buttons: {
            Ok: function() {
                $(this).dialog('close');
            }

        }
    });
});

</script>

<div id="dialog-responseSave" title="Autoupload "> 
	<p> 
		<span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span> 
		<div id="msg"></div> 
	</p> 
</div>


								<g:form name="autoupload" controller="inventory" method="post" action="testupload" class="form-horizontal">
									<div class="form-group">
                    <label for="url" class="col-sm-2 control-label">FTP or HTTP URL</label>
                    <div class="col-sm-8">
											<input type="text" class="form-control" id="url" name="url" value="${fieldValue(bean:autoUpload,field:'url')}" />
                    </div>
                  </div>
									
									<div class="form-group">
                    <label for="action" class="col-sm-2 control-label">Disable Auto upload</label>
                    <div class="col-sm-8">
											<input type="checkbox" name="form-control" id="action" ${(autoUpload.disableauto?'checked':"")}  />
											<input type="hidden" name="_disableauto" />
                    </div>
                  </div>
									
									<div class="form-group">
                    <label for="part" class="col-sm-2 control-label">Part Number</label>
                    <div class="col-sm-8">
											<g:select class="form-control" from="${['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O']}" noSelection="${['NA':'Not Applicable']}" name="part" value="${autoUpload?.part}" />
                    </div>
                  </div>
									
									<div class="form-group">
                    <label for="manufacturer" class="col-sm-2 control-label">Manufacturer</label>
                    <div class="col-sm-8">
											<g:select from="${['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O']}" noSelection="${['NA':'Not Applicable']}" name="manufacturer" value="${autoUpload?.manufacturer}" />
                    </div>
                  </div>
									
									<div class="form-group">
                    <label for="s_p_part" class="col-sm-2 control-label">Quantity</label>
                    <div class="col-sm-8">
											<g:select from="${['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O']}" noSelection="${['NA':'Not Applicable']}" name="qty" value="${autoUpload?.qty}" />
                    </div>
                  </div>
									
									<div class="form-group">
                    <label for="s_p_part" class="col-sm-2 control-label">Condition</label>
                    <div class="col-sm-8">
											<g:select from="${['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O']}" noSelection="${['NA':'Not Applicable']}" name="condition" value="${autoUpload?.condition}" />
                    </div>
                  </div>
									
									<div class="form-group">
                    <label for="s_p_part" class="col-sm-2 control-label">Price</label>
                    <div class="col-sm-8">
											<g:select from="${['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O']}" noSelection="${['NA':'Not Applicable']}" name="price" value="${autoUpload?.price}" />
                    </div>
                  </div>
									
									<div class="form-group">
                    <label for="s_p_part" class="col-sm-2 control-label">Retail Price</label>
                    <div class="col-sm-8">
											<g:select from="${['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O']}" noSelection="${['NA':'Not Applicable']}" name="retailprice" value="${autoUpload?.retailprice}" />
                    </div>
                  </div>
									
									<div class="form-group">
                    <label for="s_p_part" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-8">
											<g:select from="${['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O']}" noSelection="${['NA':'Not Applicable']}" name="description" value="${autoUpload?.description}" />
                    </div>
                  </div>
									
									<div class="form-group">
                    <label for="s_p_part" class="col-sm-2 control-label">Image Url</label>
                    <div class="col-sm-8">
											<g:select from="${['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O']}" noSelection="${['NA':'Not Applicable']}" name="imageurl" value="${autoUpload?.imageurl}" />
                    </div>
                  </div>
									
									<div class="form-group">
                    <div class="col-sm-offset-2 col-sm-8">
                       <button type="button" id="saveupload" class="btn btn-primary">Save Settings</button>
                       <button type="button" id="testupload" class="btn btn-primary">Test Autoupload</button>
                    </div>
                  </div>
								</g:form>
