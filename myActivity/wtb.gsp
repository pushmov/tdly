 

<html>
	<g:javascript library="jquery" plugin="jquery" />
	<jqui:resources/>
        <jqDT:resources/>               	                

	<jqval:resources />
        <jqvalui:resources />

	<script>

	$(document).ready(function() {


	    $('#dialog-bmessage').dialog({
        	autoOpen: false,
	        height: 150,
        	width: 250,
	        modal: true,
        	buttons: {
		            Ok: function() {
        		        $(this).dialog('close');
            		    }

	        }        
             });

	    $('#dialog-message').dialog({
        	autoOpen: false,
	        height: 150,
        	width: 250,
	        modal: true,
        	buttons: {
		            Ok: function() {
        		        $(this).dialog('close');
            		    }

	        }        
             });


	    $('#dialog-broadcast').dialog({
        	autoOpen: false,
	        height: 500,
        	width: 700,
	        modal: true,
        	buttons: {
		            Save: function() {
        		        $('#fsingle').submit(); 
                		$(this).dialog('close');
            		    },
		            Cancel: function() {
                		$(this).dialog('close');
            		    }

	        }        
             });

      $('#fsingle').submit( function() {

			    alert('submitting');
			    $.ajax({
    			    type: "POST",
    			    url: "../broadcast/update",
			    cache: false,
    			    data: $(this).serialize(),
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
				   $('#dialog-bmessage').dialog("open");
 				   //setTimeout(function(){$('#dialog-message').dialog("close")},2000);
        			
    			    }
			    
                            });

		    
        	            return false;

              });


	});


function addToWatchlist(id) {


			    $.ajax({
    			    type: "POST",
    			    url: "../inventory/addwatchlist",
			    cache: false,
    			    data: { bid : id },
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
				   $('#dialog-message').dialog("open");
 				   //setTimeout(function(){$('#dialog-message').dialog("close")},2000);
        			
    			    }
			    
                            });



}


function edit(bid,part,mfgid,condition,price,quantity,description,broadcasttype) {

	alert(description);
	$('#fsingle #id').val(bid);
	$('#fsingle #part').val(part);
	$('#fsingle #qty').val(quantity);
	$('#fsingle #price').val(price);
	$('#fsingle #condition').val(condition);
	$('#fsingle #manufacturerid').val(mfgid);
	$('#fsingle #description').val(description);
	$('#fsingle #broadcasttype').val(broadcasttype);
	$('#dialog-broadcast').dialog('open');

}



	</script>


<div id="dialog-bmessage" title="Broadcast"> 
	<p> 
		<span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span> 
		 Broadcast Updated.
	</p> 
</div> 

<div id="dialog-message" title="Watchlist"> 
	<p> 
		<span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span> 
		 Part Added to Watchlist.
	</p> 
</div> 

<div id="dialog-broadcast" title="Update Broadcast"> 
	<p>
            <g:form name="fsingle" >
							<!--[if !IE]>start fieldset<![endif]-->
							<fieldset>

		 					    <g:hiddenField name="id" value="" />


								<div class="row">
									<label>BroadcastType:</label>
									<div class="inputs">
										<span class="input_wrapper select_wrapper">
					                                    <g:select from="${['WTB','WTS']}" name="broadcasttype" value="" ></g:select>
										</span>
									</div>
								</div>

                  					    <g:render template="/tradeunitajax" var="tradeunit"  />


								<div class="row">
									<label>Description:</label>
									<div class="inputs">
										<span class="input_wrapper"><g:textField  name="description" value="" /></span>
									</div>
								</div>

 
								<!--[if !IE]>end row<![endif]-->
								
							</fieldset>
							
							
	</g:form>
	</p>
</div>

<div class="tablewrap">
<br/>
<br/>
<div class="eachtab" id="tab-1">

<table width="100%" border="0" cellspacing="1" cellpadding="0"  id="rounded-corner"  >
<thead>

								<tr>

                        
                   	        <g:sortableColumn property="created" title="Created" />
                   	        <g:sortableColumn property="unit.part" title="Part" />
                   	        <g:sortableColumn property="unit.manufacturer" title="Manufacturer" />
                   	        <g:sortableColumn property="unit.condition" title="Condition" />
                   	        <g:sortableColumn property="unit.price" title="Price" />
                   	        <g:sortableColumn property="unit.qty" title="quantity" />
                   	        <g:sortableColumn property="description" title="Description" />
				<td>Actions</td>
                        
                        
                        </tr>
</thead>
 <tfoot>
    </tfoot>
<tbody>
	
                    <g:each in="${broadcastList}" status="i" var="broadcast">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                        
                            <td>${fieldValue(bean:broadcast, field:'dateCreated')}</td>
                            <td>${fieldValue(bean:broadcast.unit, field:'part')}</td>
                            <td>${fieldValue(bean:broadcast.unit, field:'manufacturer.mfgname')}</td>
                            <td>${fieldValue(bean:broadcast.unit, field:'condition')}</td>
                            <td>${fieldValue(bean:broadcast.unit, field:'price')}</td>
                            <td>${fieldValue(bean:broadcast.unit, field:'qty')}</td>
                            <td>${fieldValue(bean:broadcast, field:'description')}</td>
				<td>
						<div class="actions_menu">
							<ul>
				   			<li><a href="javascript:void(0)" class="edit" id="respond" onClick="javascript:edit('${broadcast?.id}','${broadcast?.unit?.part}','${broadcast?.unit?.manufacturer?.id}','${broadcast?.unit?.condition}','${broadcast?.unit?.price}','${broadcast?.unit?.qty}','${broadcast?.description}','${broadcast?.broadcasttype}');" >Edit</a></li>
												<li><g:remoteLink class="delete" controller="broadcast" action="delete" id="${broadcast?.id}" before="return confirm('Are you sure?');" params="[targetUri: (request.forwardURI - request.contextPath)]" >Delete</g:remoteLink></li>
												<li><a href="javascript:void(0)" class="add" id="respond" onClick="javascript:addToWatchlist('${broadcast?.id}');" >Add Part To Watchlist</a></li>
											</ul>
										</div>
									</td>

                        </tr>
                    </g:each>
      </tbody>
							
								
							</table>
	<div class="pagination">
								
                                
 <g:paginate controller="myActivity" action="${flash.action}" total="${Total}"  />

		</div>

		</div>
			<!--inner-contrightpan end -->
</div>
			

								
								
						

