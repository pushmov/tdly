 

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Broadcast List</title>
	<script>
	function assignBid(bid) {
		dialog3Form.bid.value = bid
	}
	</script>


    </head>
    <body> 
        <div class="body">
	<div id="bresp"></div>
            <div class="list">
                <table>
                    <thead>
                        <tr> 
                        
                   	        <g:sortableColumn property="created" title="Created" />
                   	        <g:sortableColumn property="unit.part" title="Part" />
                   	        <g:sortableColumn property="unit.manufacturer" title="Manufacturer" />
                   	        <g:sortableColumn property="unit.condition" title="Condition" />
                   	        <g:sortableColumn property="unit.price" title="Price" />
                   	        <g:sortableColumn property="unit.qty" title="quantity" />
                   	        <g:sortableColumn property="description" title="Description" />
                        
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${broadcastList}" status="i" var="broadcast">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                        
                            <td>${fieldValue(bean:broadcast, field:'dateCreated')}</td>
                            <td>${fieldValue(bean:broadcast.unit, field:'part')}</td>
                            <td>${fieldValue(bean:broadcast.unit, field:'manufacturer')}</td>
                            <td>${fieldValue(bean:broadcast.unit, field:'condition')}</td>
                            <td>${fieldValue(bean:broadcast.unit, field:'price')}</td>
                            <td>${fieldValue(bean:broadcast.unit, field:'qty')}</td>
                            <td>${fieldValue(bean:broadcast, field:'description')}</td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>

            <div class="paginateButtons">
            </div>
        </div>
    </body>
</html>
