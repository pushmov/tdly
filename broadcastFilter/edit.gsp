

<%@ page import="test2.BroadcastFilter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'broadcastFilter.label', default: 'BroadcastFilter')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${broadcastFilterInstance}">
            <div class="errors">
                <g:renderErrors bean="${broadcastFilterInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${broadcastFilterInstance?.id}" />
                <g:hiddenField name="version" value="${broadcastFilterInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="categories"><g:message code="broadcastFilter.categories.label" default="Categories" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: broadcastFilterInstance, field: 'categories', 'errors')}">
                                    <g:select name="categories" from="${test2.Category.list()}" multiple="yes" optionKey="id" size="5" value="${broadcastFilterInstance?.categories*.id}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="countries"><g:message code="broadcastFilter.countries.label" default="Countries" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: broadcastFilterInstance, field: 'countries', 'errors')}">
                                    <g:select name="countries" from="${test2.Country.list()}" multiple="yes" optionKey="id" size="5" value="${broadcastFilterInstance?.countries*.id}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="keyword"><g:message code="broadcastFilter.keyword.label" default="Keywords" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: broadcastFilterInstance, field: 'keyword', 'errors')}">
                                    <g:textField name="keyword" value="${broadcastFilterInstance?.keyword}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="manufacturers"><g:message code="broadcastFilter.manufacturers.label" default="Manufacturers" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: broadcastFilterInstance, field: 'manufacturers', 'errors')}">
                                    <g:select name="manufacturers" from="${test2.Manufacturer.list()}" multiple="yes" optionKey="id" size="5" value="${broadcastFilterInstance?.manufacturers*.id}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="myCountries"><g:message code="broadcastFilter.myCountries.label" default="My Countries" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: broadcastFilterInstance, field: 'myCountries', 'errors')}">
                                    <g:checkBox name="myCountries" value="${broadcastFilterInstance?.myCountries}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="myVendors"><g:message code="broadcastFilter.myVendors.label" default="My Vendors" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: broadcastFilterInstance, field: 'myVendors', 'errors')}">
                                    <g:checkBox name="myVendors" value="${broadcastFilterInstance?.myVendors}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="name"><g:message code="broadcastFilter.name.label" default="Name" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: broadcastFilterInstance, field: 'name', 'errors')}">
                                    <g:textField name="name" value="${broadcastFilterInstance?.name}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="wtb"><g:message code="broadcastFilter.wtb.label" default="Wtb" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: broadcastFilterInstance, field: 'wtb', 'errors')}">
                                    <g:checkBox name="wtb" value="${broadcastFilterInstance?.wtb}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="wts"><g:message code="broadcastFilter.wts.label" default="Wts" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: broadcastFilterInstance, field: 'wts', 'errors')}">
                                    <g:checkBox name="wts" value="${broadcastFilterInstance?.wts}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
