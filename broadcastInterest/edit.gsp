

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Edit BroadcastInterest</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">BroadcastInterest List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New BroadcastInterest</g:link></span>
        </div>
        <div class="body">
            <h1>Edit BroadcastInterest</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${broadcastInterestInstance}">
            <div class="errors">
                <g:renderErrors bean="${broadcastInterestInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <input type="hidden" name="id" value="${broadcastInterestInstance?.id}" />
                <input type="hidden" name="version" value="${broadcastInterestInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="broadcast">Broadcast:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:broadcastInterestInstance,field:'broadcast','errors')}">
                                    <g:select optionKey="id" from="${Broadcast.list()}" name="broadcast.id" value="${broadcastInterestInstance?.broadcast?.id}" ></g:select>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="company">Company:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:broadcastInterestInstance,field:'company','errors')}">
                                    <g:select optionKey="id" from="${Company.list()}" name="company.id" value="${broadcastInterestInstance?.company?.id}" ></g:select>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated">Date Created:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:broadcastInterestInstance,field:'dateCreated','errors')}">
                                    <g:datePicker name="dateCreated" value="${broadcastInterestInstance?.dateCreated}" ></g:datePicker>
                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" value="Update" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
