

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Privatebroadcast List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New Privatebroadcast</g:link></span>
        </div>
        <div class="body">
            <h1>Privatebroadcast List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>

                   	        <g:sortableColumn property="broadcasttype" title="Broadcasttype" />
                   	        <g:sortableColumn property="company" title="Company" />
                   	        <g:sortableColumn property="created" title="Created" />
                   	        <g:sortableColumn property="unit.part" title="Part" />
                   	        <g:sortableColumn property="unit.manufacturer" title="Manufacturer" />
                   	        <g:sortableColumn property="unit.condition" title="Condition" />
                   	        <g:sortableColumn property="unit.price" title="Price" />
                   	        <g:sortableColumn property="unit.qty" title="quantity" />
                   	        <g:sortableColumn property="description" title="Description" />

                        
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${privatebroadcastInstanceList}" status="i" var="privatebroadcastInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${privatebroadcastInstance.id}">${fieldValue(bean:privatebroadcastInstance, field:'id')}</g:link></td>
                        
                            <td>${fieldValue(bean:privatebroadcastInstance, field:'company')}</td>
                        
                            <td>${fieldValue(bean:privatebroadcastInstance, field:'dateCreated')}</td>
                        
                            <td>${fieldValue(bean:privatebroadcastInstance, field:'createdby')}</td>
                        
                            <td>${fieldValue(bean:privatebroadcastInstance, field:'noofresponses')}</td>
                        
                            <td>${fieldValue(bean:privatebroadcastInstance, field:'broadcasttype')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate controller="myActivity" action="list" total="${pbTotal}"  params="[activetab: 2]"  />
            </div>
        </div>
    </body>
</html>
