<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.Company" %>
<%@page import="com.ucbl.util.PZUtil" %>
<%
	
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		if (session.person?.address?.timezone)
			sdf.setTimeZone(TimeZone.getTimeZone(session.person.address.timezone));
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
		if (session.person?.address?.timezone)
			sdf2.setTimeZone(TimeZone.getTimeZone(session.person.address.timezone));
%>
<script language="javascript" >
$(document).ready(function() {
	//override
  $(function(){

			// The height of the content block when it's not expanded
			var adjustheight = 50;
			// The "more" link text
			var moreText = "+  More";
			// The "less" link text
			var lessText = "- Less";
			var animateDuration = 800;
					
			// The section added to the bottom of the "more-less" div

			$(".showmore .moreblock").each(function(){
				if ($(this).height() > adjustheight){
					//$(".showmore").append('<p class=continued>[&hellip;]</p><a href=# class=adjust></a>');
					$(this).css('height', adjustheight).css('overflow', 'hidden');
					$(this).parent(".showmore").append('<p class=continued>[&hellip;]</p><a href=# class=adjust></a>');
					$(this).parent(".showmore").find("a.adjust").text(moreText);
					$(this).parent(".showmore").find(".adjust").toggle(function() {
						var h = $(this).parents("div:first").find(".moreblock a").height();
						
						$(this).parents("div:first").find(".moreblock").css('overflow', 'visible').animate({
							height: "+=" + h
						}, animateDuration);
						
						$(this).parents("div:first").find("p.continued").css('display', 'none');
						$(this).text(lessText);
					}, function() {
						
						// console.log('hora');
						$(this).parents("div:first").find(".moreblock").css('overflow', 'hidden').animate({
							height: adjustheight
						}, animateDuration);
						$(this).parents("div:first").find("p.continued").css('display', 'block');
						$(this).text(moreText);
					});
				}
			});

	});
	
	$('.inbox-control input[type="checkbox"]').click(function(){
		
		if(this.checked){
			$('.right_message').find('input[type="checkbox"]').prop('checked', true);
		} else {
			$('.right_message').find('input[type="checkbox"]').prop('checked', false);
		}
		
	});
	
});

</script>
<button type="button" class="navbar-toggle left-menu" data-toggle="collapse" data-target="#main-menu" aria-expanded="true" aria-controls="main-menu">
	<span class="sr-only">Toggle navigation</span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
</button>
<div class="clearfix"></div>

	<nav id="sidebar-inner" class="left-page-nav">
		<div id="main-menu" class="navbar-collapse collapse" aria-expanded="true"> 
      <div class="sidebar-inner-block">
          <ul>
            <li class="current"><a href="#">Compose Message <img src="/tdly/assets/img/profil_page/compose.png" alt=""></a></li>
            <li><a href="../broadcastResponse/list?messageType=inbox"><img src="/tdly/assets/img/profil_page/inbox.png" alt=""> Inbox</a></li>
            <li><a href="../connectionRequest/list?messageType=invite"><img src="/tdly/assets/img/profil_page/invitations.png" alt=""> Invitations</a></li>
            <li><a href="../broadcastResponse/list?messageType=sent"><img src="/tdly/assets/img/profil_page/send.png" alt=""> Sent</a></li>
            <li><a href="#"><img src="/tdly/assets/img/profil_page/archived.png" alt=""> Archived</a></li>
            <li><a href="#"><img src="/tdly/assets/img/profil_page/trash.png" alt=""> Trash</a></li>
          </ul>
      </div>
      <div class="clearfix"></div>
    </div>
  </nav>
	
	<div id="main-content" class="page-mailbox">
		<div class="row" data-equal-height="true">
			<div class="col-md-4 list-messages">
				<div class="panel panel-default">
					<div class="panel-body messages">
						<div class="input-group input-group-lg border-bottom">
							<span class="input-group-btn">
								<a href="" class="btn"><i class="fa fa-search"></i></a>
							</span>
							<input type="text" class="form-control bd-0 bd-white" placeholder="Search">
						</div>
						<div id="messages-list" class="panel panel-default withScroll" data-height="window" data-padding="90">
							
							<g:each in="${broadcastResponseList}" status="i" var="resp">
							<div class="message-item media">
								<div class="pull-left text-center">
									<div class="pos-rel message-checkbox">
										<input type="checkbox" data-style="flat-red">
									</div>
								</div>
								
								<div class="message-item-right">
									<div class="media">
										<img src="../images/icon_img.jpg" alt="avatar 3" width="50" class="pull-left">
										<div class="media-body">
											<h5 class="c-dark"><strong>${resp?.person?.firstname} ${resp?.person?.lastname}</strong><span><a href="#">(${resp?.receiver?.address?.country?.name})</a></span></h5>
											<small class="date-small"><%=PZUtil.format(resp.dateCreated)%></small>
											<p class="f-14 c-gray c-text">${resp?.broadcast?.title}</p>
											
											<!--
											<div class="attachment-area">
												<strong><i class="fa fa-paperclip"></i> <span class="mail-info">2</span></strong>
											</div>
											-->
										</div>
									</div>
								</div>
								
								<div class="data-content" style="display:none">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<div class="p-20">
												<div class="message-item media">
													<div class="message-item-right">
														<div class="media">
															<img src="../images/icon_img.jpg" alt="avatar 4" width="50" class="pull-left">
															<div class="media-body">
																<h5 class="c-dark"><strong>${resp?.person?.firstname} ${resp?.person?.lastname}</strong></h5>
																<small class="date-small"><%=PZUtil.format(resp.dateCreated)%></small>
																<p class="c-gray">jerry.smith@gmail.com</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											
											<div class="message-body">${resp?.responsetext?.trim()}</div>
										</div>
									</div>
								</div>
								
								<div style="display:none" class="message-nav">
									<div>
										<div class="pull-left">
											<div id="go-back" data-rel="tooltip" title="Go back" class="icon-rounded m-r-10 email-go-back">
												<i class="fa fa-hand-o-left"></i>
											</div>
											<div data-rel="tooltip" title="Reply" class="icon-rounded m-r-10">
												<a href="javascript:void(0)" id="respond" onClick="javascript:showResponse('${fieldValue(bean:resp.broadcast, field:'id')}',${resp?.person?.id});">
													<i class="fa fa-reply"></i>
												</a>
											</div>
											<div data-rel="tooltip" title="Forward" class="icon-rounded m-r-10"><i class="fa fa-long-arrow-right"></i>
											</div>
											<div data-rel="tooltip" title="Remove from favs" class="icon-rounded m-r-10"><i class="fa fa-heart"></i>
											</div>
											<div data-rel="tooltip" title="Print" class="icon-rounded m-r-10"><i class="fa fa-print"></i>
											</div>
											<div data-rel="tooltip" title="Move to trash" class="icon-rounded m-r-10">
												<a href="javascript:void(0)" onClick="deleteInbox('${resp.id}', this ,'${messageType}')" id="showResp">
													<i class="fa fa-trash-o"></i>
												</a>
											</div>
										</div>
										<div class="pull-right">
											<div data-rel="tooltip" title="Prev" class="icon-rounded m-r-10"><i class="fa fa-angle-double-left"></i>
											</div>
											<div data-rel="tooltip" title="Next" class="icon-rounded m-r-10"><i class="fa fa-angle-double-right"></i>
											</div>
											<div data-rel="tooltip" title="Parameters" class="icon-rounded gear m-r-10"><i class="fa fa-gear"></i>
											</div>
										</div>
									</div>
								</div>
								
							</div>
							</g:each>
							
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-lg-8 col-md-8 email-hidden-sm detail-message">
				<div id="message-detail" class="panel panel-default withScroll" data-height="window" data-padding="40">
					<div class="panel-heading messages message-result">
						<div class="message-action-btn clearfix p-t-20">
							
						</div>
						<h2 class="p-t-20 w-500"><span class="title"></span></h2>
						<div class="panel-body messages message-result message-clone-result">
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="clearfix"></div>
	
	<script>
		$('.message-item').click(function(){
			$('.message-item').removeClass('message-active');
			$(this).addClass('message-active');
			$('.message-clone-result').html("");
			$(this).find('.data-content > .row').clone().appendTo('.message-clone-result');
			
			$('#message-detail .message-action-btn').html("");
			$(this).find('.message-nav > div').clone().appendTo('#message-detail .message-action-btn');
			
			if($(this).find('.media-body .f-14').text() != ''){
				$('.w-500 .title').html('Re: ' + $(this).find('.media-body .f-14').text());
			}
			
		});
	</script>

<div class="left_side" style="display:none">
     <div class="left_message">
	 <div class="compose">
		<a href="#">Compose Message</a>
		<i class="fa fa-envelope-o fa-2x"></i>
	</div> 
		  <div class="sidemenu">
                  <ul>
						<li><a href="../broadcastResponse/list?messageType=inbox" class="${((messageType == 'inbox') || (messageType == null) || (messageType == ''))?'active':''}" >Inbox</a> </li>
						<li><a href="../connectionRequest/list?messageType=invite" class="${(messageType == 'invite')?'active':''}" >Invitations </a> </li>
						<li><a href="../broadcastResponse/list?messageType=sent" class="${(messageType == 'sent')?'active':''}"  >Sent</a></li>		
						<li><a href="#" class=""  >Archived</a></li>		
						<li><a href="#" class=""  >Trash</a></li>		
		      </ul>
            </div>
						
						
						
	 </div>
	 
	 <div class="clear"></div>
	 
	 <div class="left_search clearfix">
		<form>
			<input type="text" name="" placeholder="Search" />
			<button type="submit" class="btn btn-msg-search"><i class="fa fa-search fa-lg"></i></button>
		</form>
						</div>
	
<!--
	 <div class="search_maindiv">
	       
	      <div class="search_main"><input class="search_inbox" type='text' name='search' value='Search'  onfocus="this.value=='Search'?this.value='':this.value=this.value;" onblur="this.value==''?this.value='Search':this.value=this.value;">
	 </div>
	 <div class="search_icon"><img src="../images/search_icon.png" /></div> 
	 </div>
 -->	 
	  <div class="clear"></div>
	 </div>
	 
	 
	 <div class="right_message" style="display:none">
         <div class="clear"></div>
				 
				 <div class="inbox-control clearfix">
						<div class="chekbox_m">
							<input type="checkbox" value="" name="check" class="check" id="check1">
							<label for="check1" class="check" href="#">Select All</label>
						</div>
						
						<div class="buttons">
							<a href="#">Archive</a>
						</div>
						<div class="buttons">
							<a href="#">Delete</a>
						</div>
				 </div>
				 
         	<g:each in="${broadcastResponseList}" status="i" var="resp">
        <%
						def company
						if (messageType == null) {
							company = Company.get(resp.person.company.id)
						}
						else {
							company = Company.get(resp.receiver.company.id)
						}
		            %>

         		<div class="inbox_main">
							<div class="chekbox_m">
								<input type="checkbox" value="" name="">
							</div>
                      		<div class="icon_img"><img src="../images/icon_img.jpg" /></div>
                          	<div class="con_m">
                               	<div class="con_m_heading">
																	<div class="date_m"><%=PZUtil.format(resp.dateCreated)%></div>
					<g:if test="${messageType == null}" >
						<g:link controller="person" action="profile" id="${resp?.person?.id}" >${resp?.person?.firstname} ${resp?.person?.lastname}</g:link>
					</g:if>
					<g:if test="${messageType != null}" >
						<g:link controller="person" action="profile" id="${resp?.receiver?.id}" >${resp?.receiver?.firstname} ${resp?.receiver?.lastname}</g:link>
					</g:if>(
					<g:if test="${messageType == null}" >
						${resp?.person?.address?.country?.name} 
					</g:if>
					<g:if test="${messageType != null}" >
						${resp?.receiver?.address?.country?.name}
					</g:if>)</a>
				</div>
                                <div class="con_m_para">
					<g:if test="${resp.broadcast != null}">
						<g:if test="${resp.broadcast?.title != null}">
                                        		<b>Broadcast :</b> <a href="javascript:void(0);"  onClick="broadcastDesc(${resp?.broadcast?.id})" >${resp?.broadcast?.title}</a></strong>
						</g:if>
						<g:if test="${resp?.broadcast?.title == null}">
							<a href="javascript:void(0)" onClick="javascript:getResponses(${resp.id}, ${resp.id} )" ><b>Broadcast :&nbsp;&nbsp;</b>${PZUtil.format(resp.broadcast?.dateCreated)}&nbsp;&nbsp;&nbsp;<b>${fieldValue(bean:resp.broadcast, field:'broadcasttype')}</b>
							&nbsp;&nbsp;<b>Part:&nbsp;</b>${fieldValue(bean:resp.broadcast?.unit, field:'part')}
							    &nbsp;&nbsp;&nbsp;<b>${fieldValue(bean:resp.broadcast?.unit?.manufacturer, field:'mfgname')}</b>
							    &nbsp;&nbsp;</b>${fieldValue(bean:resp.broadcast?.unit, field:'condition.name')}
							    &nbsp;&nbsp;<b>Price:&nbsp;</b>${fieldValue(bean:resp.broadcast?.unit, field:'price')}
							    &nbsp;&nbsp;<b>Quantity:&nbsp;</b>${fieldValue(bean:resp.broadcast?.unit, field:'qty')}
						</g:if>
					</g:if>
				</div>
                                <div class="con_m_para1">
					<g:if test="${resp.broadcast != null}">
  						<g:if test="${resp.broadcast?.title != null}">
							<div class="showmore">
                                        			<div class="moreblock">
                                        				${resp?.responsetext?.trim()}
                                        			</div>
                                        		</div>
                        			</g:if>
                        		</g:if>
                                	<g:if test="${resp.broadcast?.title == null}">
						<g:if test="${resp.broadcast != null}">
							<div class="showmore">
                                        			<div class="moreblock">
                            						${resp?.responsetext?.trim()}</a>
                                        			</div>
                                			</div>
                                		</g:if>
                                	</g:if>
				</div>
<g:if test="${messageType == null}" >
                                <g:if test="${company.id != session.company.id}">
                                           <div class="con_icon"><a href="javascript:void(0)" id="respond" onClick="javascript:showResponse('${fieldValue(bean:resp.broadcast, field:'id')}',${resp?.person?.id});" >
																						<i class="fa fa-mail-reply fa-lg"></i></a>
																					</div>
	<!--
                                           <div class="con_icon"><a href="javascript:void(0)" id="respond" onClick="javascript:showResponse('${fieldValue(bean:resp.broadcast, field:'id')}',${resp?.person?.id});" ><i class="fa fa-mail-forward fa-lg"></i></a></div>
	-->
                                </g:if>
                                 <g:else>
                                                <pre>           </pre>
                                </g:else>

																<!-- samples other icon -->
																<div class="con_icon co"> <a href="javascript:void(0)" title="reply"><i class="fa fa-mail-reply fa-lg"></i></a></div>
																<div class="con_icon co"> <a href="javascript:void(0)" title="forward"><i class="fa fa-mail-forward fa-lg"></i></a></div>
																<div class="con_icon co"> <a href="javascript:void(0)" title="dashboard"><i class="fa fa-home fa-lg"></i></a></div>
																<!-- end samples --> 
																
                               <div class="con_icon co"> <a href="javascript:void(0)" onClick="deleteInbox('${resp.id}', this ,'${messageType}')" id="showResp" title="delete"><i class="fa fa-trash fa-lg"></i></a></div>
                        </g:if>
                        <g:else>
                               <div class="con_icon"> <a href="javascript:void(0)" onClick="deleteInbox('${resp.id}', this ,'${messageType}')" id="showResp" title="delete"><i class="fa fa-trash fa-lg"></i></a></div>
                        </g:else>

		  </div>
                          
                          <div class="clear"></div>
                  </div>
                    </g:each>
		  <div  class="clear"></div>
		 <!-- 
		  <div class="pagination_n">
		       <div class="pagination_pre"><a href="#"></a></div>			    
			     <div class="pagination_num">
                  <ul>
						<li><a href="#" class="active">1 </a> </li>
						<li><a href="#" >2</a></li>						
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>		
						<li><a href="#">5</a></li>
						 
						
		         </ul>             
			     </div>
			   <div class="pagination_next"><a href="#"></a></div>
		  </div>
		   -->
			<div class="pagination_n">
			<util:remotePaginate controller="broadcastResponse" action="bcastresponse" total="${brTotal}"   params="[messageType:messageType]" update="tab-1" />
			</div>
		  <div  class="clear"></div>
	 </div>
	 
	 <script>
		$(document).ready(function(){
			$('body').addClass('mailbox-page');
			$('#messages-list .message-item:eq(1)').click();
		});
	 </script>

