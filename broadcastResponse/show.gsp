

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Show BroadcastResponse</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">BroadcastResponse List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New BroadcastResponse</g:link></span>
        </div>
        <div class="body">
            <h1>Show BroadcastResponse</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>

                    
                        <tr class="prop">
                            <td valign="top" class="name">Id:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:broadcastResponse, field:'id')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Broadcast:</td>
                            
                            <td valign="top" class="value"><g:link controller="broadcast" action="show" id="${broadcastResponse?.broadcast?.id}">${broadcastResponse?.broadcast?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Company:</td>
                            
                            <td valign="top" class="value"><g:link controller="company" action="show" id="${broadcastResponse?.company?.id}">${broadcastResponse?.company?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Fromid:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:broadcastResponse, field:'fromid')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Quotes:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:broadcastResponse, field:'quotes')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Respdate:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:broadcastResponse, field:'respdate')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Responsetext:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:broadcastResponse, field:'responsetext')}</td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <input type="hidden" name="id" value="${broadcastResponse?.id}" />
                    <span class="button"><g:actionSubmit class="edit" value="Edit" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
