<%! import groovy.xml.MarkupBuilder %> 


<html>
    <head>
<script>

var myWin; 
var theform;
var bid;  

Ext.onReady(function(){

/*
    var tabs = new Ext.TabPanel({
        renderTo: 'tabs1',
        width:450,
        activeTab: 0,
        frame:true,
        defaults:{autoHeight: true},
        items:[
            {contentEl:'script', title: 'Short Text'},
            {contentEl:'markup', title: 'Long Text'}
        ]
    });
*/
    // second tabs built from JS
    var tabs2 = new Ext.TabPanel({
        renderTo: document.body,
        activeTab: 0,
        width:800,
        height:550,
        plain:true,
        defaults:{autoScroll: true},
        items:[{
                title: 'Broadcast Responses',
                autoLoad: '/test2/broadcastResponse/list2'
            },{
                title: 'RFQ Responses',
                autoLoad:'/test2/myActivity/wts'
            },{
                title: 'Vendor Requests',
                autoLoad: '/test2/myVendor/listrequests'
            },{
                title: 'Consumer Enquiry',
                autoLoad: '/test2/consumerinquiry/list'
            },{
                title: 'Private Broadcasts',
                autoLoad: '/test2/privatebroadcastinbox/list'
            },{
                title: 'Broadcasts of Interest',
                autoLoad: '/test2/broadcastInterest/list'
            }
        ]
    });

    function handleActivate(tab){
        alert(tab.title + ' was activated.');
    }


/*
var treeLoader = new Ext.tree.TreeLoader({ 
dataUrl:'http://localhost:8080/test2/broadcastResponse/nodes?id=1'
});

var rootNode = new Ext.tree.AsyncTreeNode({ 
text: 'Countries' 
});

var tree = new Ext.tree.TreePanel({ 
renderTo:'childTab1', 
loader: treeLoader, 
root: rootNode 
});

   tree.render();
*/
}); 




</script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>BroadcastResponse List</title>
    </head>
    <body>

					<table valign="top">
                        <tr valign="top">
                   	        <g:sortableColumn property="broadcast.created" title="Broadcast Date" />
                   	        <g:sortableColumn property="broadcast.created" title="Type" />
                   	        <g:sortableColumn property="broadcast.unit.part" title="Part" />
                   	        <g:sortableColumn property="broadcast.unit.manufacturer" title="Manufacturer" />
                   	        <g:sortableColumn property="broadcast.unit.condition" title="Condition" />
                   	        <g:sortableColumn property="broadcast.unit.price" title="Price" />
                   	        <g:sortableColumn property="broadcast.unit.qty" title="Quantity" />
                   	        <g:sortableColumn property="company" title="Company" />
                   	        <g:sortableColumn property="fromid" title="Contact Name" />
                   	        <g:sortableColumn property="respdate" title="Response Date" />
                   	        <g:sortableColumn property="respdate" title="Country" />
					  <th>Response text</th>                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${broadcastResponseList}" status="i" var="broadcastResponse">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                        
                            <td>${fieldValue(bean:broadcastResponse.broadcast, field:'dateCreated')}</td>
                            <td>${fieldValue(bean:broadcastResponse.broadcast, field:'broadcasttype')}</td>
                            <td>${fieldValue(bean:broadcastResponse.broadcast.unit, field:'part')}</td>
                            <td>${fieldValue(bean:broadcastResponse.broadcast.unit, field:'manufacturer')}</td>
                            <td>${fieldValue(bean:broadcastResponse.broadcast.unit, field:'condition')}</td>
                            <td>${fieldValue(bean:broadcastResponse.broadcast.unit, field:'price')}</td>
                            <td>${fieldValue(bean:broadcastResponse.broadcast.unit, field:'qty')}</td>
                            <td>${fieldValue(bean:broadcastResponse.company, field:'name')}</td>
                            <td>${fieldValue(bean:broadcastResponse, field:'fromid')}</td>
                            <td>${fieldValue(bean:broadcastResponse, field:'dateCreated')}</td>
                            <td>${fieldValue(bean:broadcastResponse, field:'country')}</td>
                            <td>${fieldValue(bean:broadcastResponse, field:'responsetext')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            <div class="paginateButtons">
			<util:remotePaginate controller="broadcastResponse" action="bcastresponse" total="${BroadcastResponse.count()}"
                                   update="blist"/>
            </div>


    </body>
</html>




