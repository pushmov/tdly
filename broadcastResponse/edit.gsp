

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Edit BroadcastResponse</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">BroadcastResponse List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New BroadcastResponse</g:link></span>
        </div>
        <div class="body">
            <h1>Edit BroadcastResponse</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${broadcastResponse}">
            <div class="errors">
                <g:renderErrors bean="${broadcastResponse}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <input type="hidden" name="id" value="${broadcastResponse?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="broadcast">Broadcast:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:broadcastResponse,field:'broadcast','errors')}">
                                    <g:select optionKey="id" from="${Broadcast.list()}" name="broadcast.id" value="${broadcastResponse?.broadcast?.id}" ></g:select>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="company">Company:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:broadcastResponse,field:'company','errors')}">
                                    <g:select optionKey="id" from="${Company.list()}" name="company.id" value="${broadcastResponse?.company?.id}" ></g:select>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fromid">Fromid:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:broadcastResponse,field:'fromid','errors')}">
                                    <input type="text" id="fromid" name="fromid" value="${fieldValue(bean:broadcastResponse,field:'fromid')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="quotes">Quotes:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:broadcastResponse,field:'quotes','errors')}">
                                    
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="respdate">Respdate:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:broadcastResponse,field:'respdate','errors')}">
                                    <g:datePicker name="respdate" value="${broadcastResponse?.respdate}" ></g:datePicker>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="responsetext">Responsetext:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:broadcastResponse,field:'responsetext','errors')}">
                                    <input type="text" id="responsetext" name="responsetext" value="${fieldValue(bean:broadcastResponse,field:'responsetext')}"/>
                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" value="Update" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
