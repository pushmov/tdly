

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Create BroadcastResponse</title>         
    </head>
    <body>
        <div class="body">
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${broadcastResponse}">
            <div class="errors">
                <g:renderErrors bean="${broadcastResponse}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        	<input type=hidden name="bid" value="${params.id}" >
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="responsetext">Responsetext:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:broadcastResponse,field:'responsetext','errors')}">
                                    <input type="text" id="responsetext" name="responsetext" value="${fieldValue(bean:broadcastResponse,field:'responsetext')}"/>
                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><input class="save" type="submit" value="Create" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
