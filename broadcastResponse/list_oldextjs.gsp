<%! import groovy.xml.MarkupBuilder %> 


<html>
    <head>
<script>

var myWin; 
var theform;
var bid;  

Ext.onReady(function(){

/*
    var tabs = new Ext.TabPanel({
        renderTo: 'tabs1',
        width:450,
        activeTab: 0,
        frame:true,
        defaults:{autoHeight: true},
        items:[
            {contentEl:'script', title: 'Short Text'},
            {contentEl:'markup', title: 'Long Text'}
        ]
    });
*/
    // second tabs built from JS
    var tabs2 = new Ext.TabPanel({
        renderTo: document.body,
        activeTab: <%=flash.activetab%>,
        width:1000,
        height:550,
        plain:true,
        defaults:{autoScroll: true},
        items:[{
                title: 'Broadcast Responses',
                <g:if test="${flash.selecttab == '0' }" >
	                autoLoad: '/test2/broadcastResponse/bcastresponse?max=' + <%=flash.max%> + '&offset=' + <%=flash.offset%>
		    </g:if>
		    <g:else>
			    autoLoad: '/test2/broadcastResponse/bcastresponse'
		    </g:else>

            },{
                title: 'RFQ Responses',
                <g:if test="${flash.selecttab == '1' }" >
	                autoLoad:'/test2/myActivity/wts?max=' + <%=flash.max%> + '&offset=' + <%=flash.offset%>
		    </g:if>
		    <g:else>
				autoLoad:'/test2/myActivity/wts'
		    </g:else>
            },{
                title: 'Vendor Requests',
                <g:if test="${flash.selecttab == '2' }" >
      	          autoLoad: '/test2/myVendor/listrequests?max=' + <%=flash.max%> + '&offset=' + <%=flash.offset%>
		    </g:if>
		    <g:else>
				autoLoad: '/test2/myVendor/listrequests'
		    </g:else>
            },{
                title: 'Consumer Enquiry',
                <g:if test="${flash.selecttab == '3' }" >
            	    autoLoad: '/test2/consumerinquiry/list?max=' + <%=flash.max%> + '&offset=' + <%=flash.offset%>
		    </g:if>
		    <g:else>
				autoLoad: '/test2/consumerinquiry/list'
		    </g:else>
            },{
                title: 'Private Broadcasts',
                <g:if test="${flash.selecttab == '4' }" >
                	    autoLoad: '/test2/privatebroadcastinbox/list?max=' + <%=flash.max%> + '&offset=' + <%=flash.offset%>
		    </g:if>
		    <g:else>
				autoLoad: '/test2/privatebroadcastinbox/list'
		    </g:else>
            },{
                title: 'Broadcasts of Interest',
                <g:if test="${flash.selecttab == '5' }" >
                	    autoLoad: '/test2/broadcastInterest/list?max=' + <%=flash.max%> + '&offset=' + <%=flash.offset%>
		    </g:if>
		    <g:else>
				autoLoad: '/test2/broadcastInterest/list'
		    </g:else>
            }
        ]
    });

    function handleActivate(tab){
        alert(tab.title + ' was activated.');
    }


/*
var treeLoader = new Ext.tree.TreeLoader({ 
dataUrl:'http://localhost:8080/test2/broadcastResponse/nodes?id=1'
});

var rootNode = new Ext.tree.AsyncTreeNode({ 
text: 'Countries' 
});

var tree = new Ext.tree.TreePanel({ 
renderTo:'childTab1', 
loader: treeLoader, 
root: rootNode 
});

   tree.render();
*/
}); 





function showResponse(prm) {
      bid = prm;
	theform=new Ext.FormPanel({

	labelAlign: 'top',

	frame:true,

	title: 'Multi Column, Nested Layouts and HTML editor',

	bodyStyle:'padding:5px 5px 0',

	width: 600,

	items: [{

	layout:'column',

	items:[{

	columnWidth:.5,

	layout: 'form',

	items: [{

	xtype:'textfield',

	fieldLabel: 'First Name',

	name: 'first',

	anchor:'95%'

	}]

	},{

	columnWidth:.5,

	layout: 'form',

	items: [{

	xtype:'textfield',

	fieldLabel: 'Last Name',

	name: 'last',

	anchor:'95%'

	},{

	xtype:'textfield',

	fieldLabel: 'Email',

	name: 'email',

	vtype:'email',

	anchor:'95%'

	}]

	}]

	},{

	xtype:'htmleditor',

	id:'responsetext',

	name:'responsetext',

	fieldLabel:'Comments',

	height:200,

	anchor:'98%'

	}],


	buttons: [{ 
text: 'Save', 
handler: function(){ 
theform.getForm().submit({ 
url:'/test2/broadcastResponse/save?bid=' + bid, 
			waitMsg:'Saving Data...',

success: function(f,a){ 
Ext.Msg.alert('Success', a.result.msg); 
myWin.close();
}, 
failure: function(f,a){ 
Ext.Msg.alert('Warning', 'Error'); 
} 
}); 
}
}, { 
text: 'Reset', 
handler: function(){ 
theform.getForm().reset(); 
}
}]
	});




myWin = new Ext.Window({ // 2 
id : 'myWin', 
height : 500, 
width : 700, 
items : [ 
theform 
] 
}); 
myWin.show();
}

function getCompany(cid) {

Ext.Ajax.request({
	url : '/test2/company/showajax' , 
	params : { id : cid },
	method: 'POST',
	success: function ( result, request ) { 
		showCompany(result.responseText); 
	},
	failure: function ( result, request) { 
		Ext.MessageBox.alert('Failed', result.responseText); 
	} 
});


}

function doJSON(stringData) {
		try {
			var jsonData = Ext.util.JSON.decode(stringData);
			return jsonData;
		}
		catch (err) {
			Ext.MessageBox.alert('ERROR', 'Could not decode ' + stringData);
		}
	}

 function showCompany(company) {

	var comp = doJSON(company);
	myWin = new Ext.Window({ // 2 
	id : 'myWin', 
	height : 500, 
	width : 700, 
	title: 'Company Details',
      layout : 'fit',
	items : [ 
		{

			xtype:'panel',

			id:'responsetext',

			name:'responsetext',

			fieldLabel:'Comments',

			height:200,

			anchor:'98%',
                 html : '<b>Name : </b>' + comp.company.name + '<br><br>' + 'Membership Level ' + comp.company.membershiplevel


		}],

		buttons: [{ 
			text: 'Close', 
			handler: function(){ 
				myWin.close();
			} 
		}]
       
	}); 	
	myWin.show();


}

</script>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="emerald" />
        <title>BroadcastResponse List</title>
    </head>
    <body>
    </body>
</html>
