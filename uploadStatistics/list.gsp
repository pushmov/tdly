
<%@ page import="test2.UploadStatistics" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'uploadStatistics.label', default: 'UploadStatistics')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <th>id</th>                       
                            <th><g:message code="uploadStatistics.user.label" default="User" /></th>
                        
                        
                        
                            <g:sortableColumn property="type" title="${message(code: 'uploadStatistics.type.label', default: 'Type')}" />
                            <g:sortableColumn property="result" title="${message(code: 'uploadStatistics.result.label', default: 'Upload Status')}" />
                            <g:sortableColumn property="partsAdded" title="${message(code: 'uploadStatistics.partsAdded.label', default: 'Parts Added')}" />
                            <g:sortableColumn property="partsUpdated" title="${message(code: 'uploadStatistics.partsUpdated.label', default: 'Parts Updated')}" />
                            <g:sortableColumn property="overwriteInventory" title="${message(code: 'overwriteInventory.result.label', default: 'Overwrite Inventory')}" />
                            <g:sortableColumn property="dateCreated" title="${message(code: 'uploadStatistics.dateCreated.label', default: 'Date Created')}" />
                        
                            <g:sortableColumn property="comments" title="${message(code: 'uploadStatistics.comments.label', default: 'Comments')}" />
                            <g:sortableColumn property="invErrors" title="${message(code: 'uploadStatistics.invErrors.label', default: 'Inv Errors')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${uploadStatisticsInstanceList}" status="i" var="uploadStatisticsInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${uploadStatisticsInstance.id}">${fieldValue(bean: uploadStatisticsInstance, field: "id")}</g:link></td>
                        
                            <td>${uploadStatisticsInstance.user?.firstname}&nbsp;${uploadStatisticsInstance.user?.lastname}</td>
                            <td>${fieldValue(bean: uploadStatisticsInstance, field: "type")}</td>
                            <td>${fieldValue(bean: uploadStatisticsInstance, field: "result")}</td>
                            <td>${fieldValue(bean: uploadStatisticsInstance, field: "partsAdded")}</td>
                            <td>${fieldValue(bean: uploadStatisticsInstance, field: "partsUpdated")}</td>
                            <td>${fieldValue(bean: uploadStatisticsInstance, field: "overwriteInventory")}</td>
                        
                            <td><g:formatDate date="${uploadStatisticsInstance.dateCreated}" /></td>
                            <td>${fieldValue(bean: uploadStatisticsInstance, field: "comments")}</td>
                        
                            <td>${fieldValue(bean: uploadStatisticsInstance, field: "invErrors")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${uploadStatisticsInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
