

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Employee List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New Employee</g:link></span>
        </div>
        <div class="body">
            <h1>Employee List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	        <g:sortableColumn property="id" title="Id" />
                        
                   	        <g:sortableColumn property="firstname" title="Firstname" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${employeeList}" status="i" var="employee">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${employee.id}">${fieldValue(bean:employee, field:'id')}</g:link></td>
                        
                            <td>${fieldValue(bean:employee, field:'firstname')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${Employee.count()}" />
            </div>
        </div>
    </body>
</html>
